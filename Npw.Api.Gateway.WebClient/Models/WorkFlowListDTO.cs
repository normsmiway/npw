﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Models
{
    public class WorkFlowListDTO
    {
        public List<WorkFlowModel> workflows { get; set; }
    }

    public class WorkFlowModel
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
    }
}
