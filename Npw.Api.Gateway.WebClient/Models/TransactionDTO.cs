﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Models
{
    public class TransactionDTO
    {
        public Guid Id { get;  set; }
        public decimal Amount { get;  set; }

        public string AccountNumber { get;  set; }

        public string SourceAccountNumber { get;  set; }

        public string AccountName { get;  set; }

        public DateTime Date { get; set; }

        public int ApprovalStatus { get;  set; }
    }
}
