﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Models
{
    public class WorkFlowConfigDTO
    {
        public Guid DepartMentWorkFlowId { get; set; }
        public Guid WorkFlowId { get; set; }

        public int UserOrder { get; set; }

        public int DepartmentOrder { get; set; }
    }
}
