﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Models
{
    public class TransactionScheduleDTO
    {
        public List<ScheduleDTO> transactionschedules { get; set; }
    }

    public class ScheduleDTO
    {
        public Guid Id { get; set; }

        public int BatchNumber { get;  set; }

        public decimal TotalAmount { get; set; }
    }
}
