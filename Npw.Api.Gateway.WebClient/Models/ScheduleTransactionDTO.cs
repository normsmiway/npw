﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Models
{
    public class ScheduleTransactionDTO
    {
        public List<TransactionDTO> transactions { get; set; }
    }
}
