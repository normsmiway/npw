﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Permissions
{
    public static class PermissionNames
    {
        public const string CanRegister = "canregister";
        public const string CanView = "canview";
    }
}
