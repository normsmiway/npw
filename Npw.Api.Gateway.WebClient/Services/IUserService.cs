﻿using N.Shared.Types;
using Npw.Api.Gateway.WebClient.Models;
using Npw.Api.Gateway.WebClient.Queries;
using RestEase;
using System;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Services
{
    [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
    public interface IUsersService
    {

        [AllowAnyStatusCode]
        [Get("users")]
        Task<PagedResult<UserDto>> GetAllAsyc([Query] BrowseUsers query);

        [AllowAnyStatusCode]
        [Get("users/{id}")]
        Task<UserDto> GetAsyc([Path]Guid id);
    }
}
