﻿using N.Shared.Types;
using Npw.Api.Gateway.WebClient.Models;
using Npw.Api.Gateway.WebClient.Queries;
using Npw.Api.Gateway.WebClient.Queries.Transaction;
using RestEase;
using System;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Services
{
    [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
    public interface ITransactionService
    {

        [AllowAnyStatusCode]
        [Get("transactions/{id}")]
        Task<TransactionDTO> GetTransactionAsyc([Path]Guid id);

        [AllowAnyStatusCode]
        [Get("transactions/transactionschedule")]
        Task<TransactionScheduleDTO> GetAllTransactionSchedulesAsyc([Query] GetTransactionSchedule query);


        [AllowAnyStatusCode]
        [Get("transactions/transactionsunderschedule/{transactionScheduleId}")]
        Task<ScheduleTransactionDTO> GetAllTransactionsUnderSchedules([Path]Guid transactionScheduleId);

    }
}
