﻿using N.Shared.Types;
using Npw.Api.Gateway.WebClient.Models;
using Npw.Api.Gateway.WebClient.Queries.Beneficiary;
using Npw.Api.Gateway.WebClient.Queries.BeneficiaryAccount;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Services
{
    [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
    public interface IBeneficiaryService
    {
        [AllowAnyStatusCode]
        [Get("Beneficiaries")]
        Task<PagedResult<BeneficiaryDto>> GetAllBeneficiaryAsync([Query] BrowseBeneficiaries query);

        [AllowAnyStatusCode]
        [Get("Beneficiaries/{id}")]
        Task<BeneficiaryDto> GetBeneficiaryAsync([Path]Guid id);

        [AllowAnyStatusCode]
        [Get("Beneficiaries/GetBeneficiaryAccount/{id}")]
        Task<BeneficiaryAccountDto> GetBeneficiaryAccountAsync([Path]Guid id);

        [AllowAnyStatusCode]
        [Get("Beneficiaries/BeneficiaryAccounts")]
        Task<PagedResult<BeneficiaryAccountDto>> GetAllBeneficiaryAccountsAsync([Query] BrowseBeneficiaryAccounts query);

    }
}
