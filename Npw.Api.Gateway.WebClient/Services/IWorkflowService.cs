﻿using Npw.Api.Gateway.WebClient.Models;
using Npw.Api.Gateway.WebClient.Queries.WorkFlow;
using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Services
{
    [SerializationMethods(Query = QuerySerializationMethod.Serialized)]
    public interface IWorkflowService
    {
        
        [AllowAnyStatusCode]
        [Get("workflow/{userId}/{departmentId}")]
        Task<WorkFlowListDTO> GetUserWorkFlowConfigAsyc([Path]Guid userId, Guid departmentId);

        [AllowAnyStatusCode]
        [Get("workflow")]
        Task<WorkFlowConfigDTO> GetWorkFlowsAsyc([Query] GetWorkFlows query);

    }
}
