﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.RabbitMq;
using Npw.Api.Gateway.WebClient.Messages.Commands.Notifications;
using OpenTracing;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Controllers
{

    public class NotificationsController : BaseController
    {
        public NotificationsController(IBusPublisher busPublisher, ITracer tracer) : base(busPublisher, tracer)
        {
        }

        [HttpPost]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Post(EmailDto command)
        {
            var email = new SendEmailNotification(command.SenderEmail, command.ReceiverEmail, command.Subject, command.Body);
            return await SendAsync(email, Guid.NewGuid(), "notifications");
        }
    }
}
