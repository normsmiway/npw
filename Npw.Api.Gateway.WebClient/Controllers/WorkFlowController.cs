﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.Mvc;
using N.Shared.RabbitMq;
using Npw.Api.Gateway.WebClient.Messages.Commands.WorkFlow;
using Npw.Api.Gateway.WebClient.Models;
using Npw.Api.Gateway.WebClient.Queries.WorkFlow;
using Npw.Api.Gateway.WebClient.Services;
using OpenTracing;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;
namespace Npw.Api.Gateway.WebClient.Controllers
{
    
    public class WorkFlowController : BaseController
    {

        private readonly IWorkflowService _service;
        public WorkFlowController(IBusPublisher busPublisher, ITracer tracer, IWorkflowService service) : base(busPublisher, tracer)
        {
            _service = service;
        }

        [HttpPost]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Post(CreateWorkFlowCommand command)
        {
            var createworkflowcommand = new CreateWorkFlowCommand(command.Id, command.Name,command.workflowlist);
            var result = await SendAsync(createworkflowcommand.BindId(u => u.Id), command.Id, "workflow");
            return result;
        }




        [HttpPost("create/department/workflow")]

        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> CreateDepartmentWorkFlow(CreateDepartmentWorkFlowCommand command)
        {
            var createdepartmentworkflow = new CreateDepartmentWorkFlowCommand(command.Id, command.DeptId,command.workflowlist);
            var result = await SendAsync(createdepartmentworkflow.BindId(u => u.Id), createdepartmentworkflow.Id, "workflow");
            return result;
        }




        [HttpGet("{userId:guid}/{departmentId:guid}")]
        [SwaggerResponse(200, Type = typeof(TransactionDTO))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> GetUserWorkFlowConfig(Guid userId, Guid departmentId)
           => Single(await _service.GetUserWorkFlowConfigAsyc(userId,departmentId));
        
       [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("get/config")]
        [SwaggerResponse(200, Type = typeof(WorkFlowListDTO))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Get([FromQuery] GetWorkFlows query)
           => Single(await _service.GetWorkFlowsAsyc(query));
    }
}
