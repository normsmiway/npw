﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.Mvc;
using N.Shared.RabbitMq;
using N.Shared.Types;
using Npw.Api.Gateway.WebClient.Framework;
using Npw.Api.Gateway.WebClient.Messages.Commands.Users;
using Npw.Api.Gateway.WebClient.Models;
using Npw.Api.Gateway.WebClient.Queries;
using Npw.Api.Gateway.WebClient.Services;
using OpenTracing;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IUsersService _service;
        public UsersController(IBusPublisher busPublisher, ITracer tracer,
            IUsersService service) : base(busPublisher, tracer)
        {
            _service = service;
        }

        [HttpPost]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Post(CreateOrUpdateUser command)
        {
            var userCommand = new CreateOrUpdateUser(command.Id, command.Email, command.UserName,
                            command.Role, command.FirstName, command.LastName, command.Gender, command.PhoneNumber);
            var result = await SendAsync(userCommand.Bind(u => u.Id, UserId).Bind(c => c.UpdatedBy, UserId), userCommand.Id, "users");

            return result;
        }


        [HttpGet]
        [AdminAuth]
        [SwaggerResponse(200, Type = typeof(PagedResult<UserDto>))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Get([FromQuery] BrowseUsers query)
            => Collection(await _service.GetAllAsyc(query));

        [HttpGet("{id:guid}")]
        [SwaggerResponse(200, Type = typeof(UserDto))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Get(Guid id)
            => Single(await _service.GetAsyc(id));




    }
}
