﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using N.Shared.Mvc;
using N.Shared.RabbitMq;
using N.Shared.Types;
using Npw.Api.Gateway.WebClient.Framework;
using Npw.Api.Gateway.WebClient.Messages.Commands.Beneficiary;
using Npw.Api.Gateway.WebClient.Messages.Commands.BeneficiaryAccount;
using Npw.Api.Gateway.WebClient.Models;
using Npw.Api.Gateway.WebClient.Queries.Beneficiary;
using Npw.Api.Gateway.WebClient.Queries.BeneficiaryAccount;
using Npw.Api.Gateway.WebClient.Services;
using OpenTracing;
using Swashbuckle.AspNetCore.Annotations;

namespace Npw.Api.Gateway.WebClient.Controllers
{
    public class BeneficiaryController : BaseController
    {
        private readonly IBeneficiaryService _service;
        public BeneficiaryController(IBusPublisher busPublisher, ITracer tracer, IBeneficiaryService service) : base(busPublisher, tracer)
        {
            _service = service;
        }

        [HttpPost("Add")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Post(AddBeneficiary command)
        {
            var addBeneCommand = new AddBeneficiary(command.Id,command.RCNumberOrIdNumber, command.BVN, command.Name, command.Email, command.Phone, command.Address, command.CreatedBy);
            var result = await SendAsync(addBeneCommand.BindId(u => u.Id).Bind(u =>u.CreatedBy, UserId).Bind(u=>u.CreatedAt, DateTime.Now).Bind(u=>u.ApprovedBy, UserId).Bind(u=>u.ApprovedAt, DateTime.Now), addBeneCommand.Id, "beneficiaries");

            return result;
        }

        [HttpPost("Approve")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> ApproveAddBeneficiary(ApproveRejectAddBeneficiary command)
        {
            var approve = new ApproveRejectAddBeneficiary(command.Id,  command.ApprovedBy,command.ApprovalStatus);
            var result = await SendAsync(approve.Bind(u => u.ApprovedBy, UserId), approve.Id, "beneficiaries");

            return result;
        }

        [HttpPost("Update")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Put(UpdateBeneficiary command)
        {
            var updateBeneCommand = new UpdateBeneficiary(command.Id, command.RCNumberOrIdNumber, command.BVN, command.Name, command.Email, command.Phone, command.Address, command.UpdatedBy, command.ApprovedBy, command.IsBlacklisted, command.ApprovalStatus, command.IsActive);
            var result = await SendAsync(updateBeneCommand.Bind(u=>u.UpdatedBy, UserId).Bind(u=>u.UpdatedAt, DateTime.Now).Bind(u=>u.ApprovedBy, UserId).Bind(u=>u.ApprovedAt, DateTime.Now), updateBeneCommand.Id, "beneficiaries");

            return result;
        }



        [HttpPost("Account/Add")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Post(AddBeneficiaryAccount command)
        {
            var addBeneAcctCommand = new AddBeneficiaryAccount(command.Id, command.AccountNumber, command.AccountName, command.BeneficiaryUserId, command.BankName, command.BankCode, command.CreatedBy);
            var result = await SendAsync(addBeneAcctCommand.BindId(u => u.Id).Bind(u=>u.CreatedBy, UserId), addBeneAcctCommand.Id, "beneficiaries");

            return result;
        }

        [HttpPost("Account/Update")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> UpdateBeneficiaryAccount(UpdateBeneficiaryAccount command)
        {
            var updBeneAcctCommand = new UpdateBeneficiaryAccount(command.Id, command.UpdatedBy, command.IsActive, command.ApprovalStatus, command.AccountNumber, command.AccountName, command.BeneficiaryUserId, command.BankName,command.BankCode);
            var result = await SendAsync(updBeneAcctCommand.Bind(u => u.UpdatedBy, UserId), updBeneAcctCommand.Id, "beneficiaries");

            return result;
        }

        [HttpPost("Account/Approve")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> ApproveBeneficiaryAccount(ApproveRejectAddBeneficiaryAccount command)
        {
            var approve = new ApproveRejectAddBeneficiaryAccount(command.Id, command.ApprovedBy, command.ApprovalStatus);
            var result = await SendAsync(approve.Bind(u => u.ApprovedBy, UserId), approve.Id, "beneficiaries");

            return result;
        }

        [HttpGet]
        [AdminAuth]
        [SwaggerResponse(200, Type = typeof(PagedResult<BeneficiaryDto>))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Get([FromQuery] BrowseBeneficiaries query)
            => Collection(await _service.GetAllBeneficiaryAsync(query));

        [HttpGet("{id:guid}")]
        [SwaggerResponse(200, Type = typeof(BeneficiaryDto))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Get(Guid id)
            => Single(await _service.GetBeneficiaryAsync(id));

        [HttpGet("GetBeneficiaryAccount/{id:guid}")]
        [SwaggerResponse(200, Type = typeof(BeneficiaryAccountDto))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> GetBeneficiaryAccount(Guid id)
            => Single(await _service.GetBeneficiaryAccountAsync(id));

        [HttpGet("GetBeneficiaryAccounts")]
        //[AdminAuth]
        [SwaggerResponse(200, Type = typeof(PagedResult<BeneficiaryAccountDto>))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> GetBeneficiaryAccountList([FromQuery] BrowseBeneficiaryAccounts query)
            => Collection(await _service.GetAllBeneficiaryAccountsAsync(query));

    }
}