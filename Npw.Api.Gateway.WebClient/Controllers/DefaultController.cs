﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using N.Shared.Authentication;
using N.Shared.Authorization;
using Npw.Api.Gateway.WebClient.Permissions;

namespace Npw.Api.Gateway.WebClient.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
     
        
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "Web Client Api running...";
        }
        
    }
}
