﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.Mvc;
using N.Shared.RabbitMq;
using Npw.Api.Gateway.WebClient.Messages.Commands.Transaction;
using Npw.Api.Gateway.WebClient.Models;
using Npw.Api.Gateway.WebClient.Queries.Transaction;
using Npw.Api.Gateway.WebClient.Services;
using OpenTracing;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Controllers
{

    //Transaction
    public class TransactionController : BaseController
    {

        private readonly ITransactionService _service;
        public TransactionController(IBusPublisher busPublisher, ITracer tracer, ITransactionService service) : base(busPublisher, tracer)
        {
            _service = service;
        }

        [HttpPost]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Post(CreateTransactionSchedule command)
        {
            var createtransactionscheduletransactionCommand = new CreateTransactionSchedule(command.Id,command.UserId,command.TotalAmount,command.Transactions);
            var result = await SendAsync(createtransactionscheduletransactionCommand.BindId(u => u.Id).Bind(x => x.UserId,UserId), createtransactionscheduletransactionCommand.Id, "transactions");
            return result;
        }



        [HttpPost("createtransaction")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> PostTransaction(CreateTransaction command)
        {
            var createtransactionCommand = new CreateTransaction(command.Id, command.UserId, command.Amount, command.AccountNumber, command.AccountName,command.BeneficiaryId,command.Narration,command.ApprovalCode);
            var result = await SendAsync(createtransactionCommand.BindId(u => u.Id).Bind(x => x.UserId, UserId), createtransactionCommand.Id, "transactions");
            return result;
        }


        [HttpPut]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Put(ApproveRejectTransaction command)
        {
            var approveorrejecttransactionCommand = new ApproveRejectTransaction(command.Id, command.transactions);
            var result = await SendAsync(approveorrejecttransactionCommand, command.Id, "transactions");
            return result;
        }

        [HttpPost("delete")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Delete(DeleteTransaction command)
        {
            var deletetransactionCommand = new DeleteTransaction(command.TransactionId);
            var result = await SendAsync(deletetransactionCommand, command.TransactionId, "transactions");
            return result;
        }

        [HttpPost("archive")]
        [SwaggerResponse(201, Type = typeof(AcceptedResult))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Archive(ArchiveTransaction command)
        {
            var archivetransactionCommand = new ArchiveTransaction(command.Id,command.TransactionScheduleId,command.TransactionIds);
            var result = await SendAsync(archivetransactionCommand, command.TransactionScheduleId, "transactions");
            return result;
        }


        [HttpGet("{id:guid}")]
        [SwaggerResponse(200, Type = typeof(TransactionDTO))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> Get(Guid id)
           => Single(await _service.GetTransactionAsyc(id));

        [HttpGet("transactionschedule")]
        [SwaggerResponse(200, Type = typeof(TransactionDTO))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> GetAllTransctionSchedule([FromQuery] GetTransactionSchedule query)
           => Single(await _service.GetAllTransactionSchedulesAsyc(query));


        [HttpGet("transactionsunderschedule/{transactionScheduleId:guid}")]
        [SwaggerResponse(200, Type = typeof(TransactionDTO))]
        [SwaggerResponse(404, Type = typeof(NotFoundResult))]
        [SwaggerResponse(403, Type = typeof(ForbidResult))]
        [SwaggerResponse(401, Type = typeof(UnauthorizedResult))]
        public async Task<IActionResult> GetAllTransctionsUnderSchedule(Guid transactionScheduleId)
           => Single(await _service.GetAllTransactionsUnderSchedules(transactionScheduleId));


    }
}
