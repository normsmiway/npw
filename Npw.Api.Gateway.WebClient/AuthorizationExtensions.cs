﻿using Microsoft.AspNetCore.Authorization;
using Npw.Api.Gateway.WebClient.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient
{
    public static class AuthorizationExtensions
    {
        public static void AddReadOnlyPolicy(this AuthorizationOptions options)
        {
            options.AddPolicy(PolicyNames.CanView,
                policy => policy.RequireClaim(UserClaimTypes.canview, "true"));
        }

        public static void AddHeadWritePolicy(this AuthorizationOptions options)
        {
            options.AddPolicy(PolicyNames.CanRegister,
                policy => policy.RequireClaim(UserClaimTypes.canregister, "true"));
        }
    }
}
