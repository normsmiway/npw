﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient
{
    public static class PolicyNames
    {
        public const string CanView = nameof(CanView);
        public const string CanRegister = nameof(CanRegister);
    }
}
