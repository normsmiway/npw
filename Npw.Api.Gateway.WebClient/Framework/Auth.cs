﻿using N.Shared.Authentication;

namespace Npw.Api.Gateway.WebClient.Framework
{

    public class AdminAuth : JwtAuthAttribute
    {
        public AdminAuth() : base("admin")
        {
        }

    }

    public class UserAuth : JwtAuthAttribute
    {
        public UserAuth() : base("user")
        {

        }
    }
}
