﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Extensions
{
    public static class UserClaimTypes
    {
        public const string canregister = nameof(canregister);
        public const String canview = nameof(canview);
    }
}
