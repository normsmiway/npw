﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.Notifications
{
    [MessageNamespace("notifications")]
    public class SendEmailNotification : ICommand
    {
        public string FromEmail { get; }
        public string ToEmail { get; }
        public string Title { get;  }
        public string Message { get; }

        [JsonConstructor]
        public SendEmailNotification(string fromEmail, string toEmail, string title, string message)
        {
            FromEmail = fromEmail;
            ToEmail = toEmail;
            Title = title;
            Message = message;
        }
    }
}
