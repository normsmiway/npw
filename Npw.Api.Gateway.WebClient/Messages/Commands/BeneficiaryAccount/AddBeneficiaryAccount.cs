﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.BeneficiaryAccount
{
    [MessageNamespace("beneficiaries")]
    public class AddBeneficiaryAccount:ICommand
    {
        public Guid Id { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public Guid BeneficiaryUserId { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public Guid CreatedBy { get; set; }

        [JsonConstructor]
        public AddBeneficiaryAccount(Guid id, string accountNumber, string accountName, Guid beneficiaryUserId, string bankName, string bankCode, Guid createdBy)
        {
            Id = id;
            AccountNumber = accountNumber;
            AccountName = accountName;
            BeneficiaryUserId = beneficiaryUserId;
            BankName = bankName;
            BankCode = bankCode;
            CreatedBy = createdBy;
        }
    }
}
