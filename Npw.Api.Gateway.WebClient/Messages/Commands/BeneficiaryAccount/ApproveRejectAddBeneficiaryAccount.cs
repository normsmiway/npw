﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.BeneficiaryAccount
{
    [MessageNamespace("beneficiaries")]
    public class ApproveRejectAddBeneficiaryAccount : ICommand
    {
        public Guid Id { get; private set; }
        public Guid ApprovedBy { get; private set; }
        public int ApprovalStatus { get; private set; }

        [JsonConstructor]
        public ApproveRejectAddBeneficiaryAccount(Guid id, Guid approvedBy, int approvalStatus)
        {
            Id = id;
            ApprovedBy = approvedBy;
            ApprovalStatus = approvalStatus;
        }
    }
}
