﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.BeneficiaryAccount
{
    [MessageNamespace("beneficiaries")]
    public class UpdateBeneficiaryAccount : ICommand
    {
        public Guid Id { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public Guid BeneficiaryUserId { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public Guid UpdatedBy { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsActive { get; set; }
        public int ApprovalStatus { get; set; }
        [JsonConstructor]
        public UpdateBeneficiaryAccount(Guid id, Guid updatedBy, bool isActive, int approvalStatus, string accountNumber, string accountName, Guid beneficiaryUserId, string bankName, string bankCode)
        {
            Id = id;
            AccountNumber = accountNumber;
            AccountName = accountName;
            BeneficiaryUserId = beneficiaryUserId;
            BankName = bankName;
            BankCode = bankCode;
            UpdatedBy = updatedBy;
            UpdatedAt = DateTime.Now;
            IsActive = isActive;
            ApprovalStatus = approvalStatus;
        }
    }
}
