﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.Users
{
    [MessageNamespace("users")]
    public class CreateOrUpdateUser : ICommand
    {
        public Guid Id { get; private set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Role { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime CreatedAt { get; private set; }
        public Guid CreatedBy { get;private set; }
        public DateTime UpdatedAt { get; private set; }
        public Guid UpdatedBy { get; private set; }
        public bool IsDeletd { get; private set; }
        public bool IsActive { get;private  set; }


        [JsonConstructor]
        public CreateOrUpdateUser(Guid id, string email, string userName, string role, string firstName, string lastName, string gender,string phoneNumber)
        {
            Id = id;
            Email = email;
            UserName = userName;
            Role = role;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            UpdatedAt = DateTime.UtcNow;
            PhoneNumber = phoneNumber;
        }
    }
}
