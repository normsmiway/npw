﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.WorkFlow
{
    [MessageNamespace("workflow")]
    public class CreateWorkFlowCommand : ICommand
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        //public Guid StateId { get; set; }
        public List<WorkFlowModel> workflowlist { get; set; }

        [JsonConstructor]
        public CreateWorkFlowCommand(Guid id, string name, List<WorkFlowModel> WorkFlowList)
        {
            Id = id;
            Name = name;
            workflowlist = WorkFlowList;
        }
    }

    public class WorkFlowModel
    {
        public Guid DepartmentWorkflowId { get; set; }
        public int Order { get; set; }
    }
}
