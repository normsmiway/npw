﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.Transaction
{
    [MessageNamespace("transactions")]
    public class ApproveRejectTransaction : ICommand
    {
        public Guid Id { get; set; }
        public List<TransactionApprovalModel> transactions { get; set; }

        [JsonConstructor]
        public ApproveRejectTransaction(Guid id, List<TransactionApprovalModel> Transactions)
        {
            Id = id;
            transactions = Transactions;
        }

    }

    public class TransactionApprovalModel
    {
        public Guid TransactionID { get; set; }
        public int ApprovalStatus { get; set; }
    }
}
