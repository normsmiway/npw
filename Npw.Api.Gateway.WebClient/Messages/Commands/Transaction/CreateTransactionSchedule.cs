﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.Transaction
{
    [MessageNamespace("transactions")]
    public class CreateTransactionSchedule : ICommand
    {
        public Guid Id { get;  set; }
        public Guid UserId { get;  set; }
        public decimal TotalAmount { get;  set; }
        public List<TransactionModel> Transactions { get; set; }


        [JsonConstructor]
        public CreateTransactionSchedule(Guid _Id, Guid _UserId, decimal _TotalAmount, List<TransactionModel> _Transactions)
        {
            Id = _Id;
            UserId = _UserId;
            TotalAmount = _TotalAmount;
            Transactions = _Transactions;
        }


    }

    public class TransactionModel
    {
        //public decimal Amount { get;  set; }

        //public string AccountNumber { get;  set; }

        //public string SourceAccountNumber { get;  set; }

        //public string AccountName { get;  set; }

        //public Guid BeneficiaryId { get;  set; }
        //public Guid TransactionScheduleId { get;  set; }
        public Guid TransactionId { get; set; }
    }
}
