﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.Beneficiary
{
    [MessageNamespace("beneficiaries")]
    public class ApproveRejectAddBeneficiary : ICommand
    {
        public Guid Id { get; set; }
        public Guid ApprovedBy { get; set; }
        public int ApprovalStatus { get; set; }
        [JsonConstructor]
        public ApproveRejectAddBeneficiary(Guid id, Guid approvedBy, int approvalStatus)
        {
            Id = id;
            ApprovedBy = approvedBy;
            ApprovalStatus = approvalStatus;
        }
    }

}
