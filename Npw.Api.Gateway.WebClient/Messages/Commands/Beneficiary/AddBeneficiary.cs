﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.Beneficiary
{
    [MessageNamespace("beneficiaries")]
    public class AddBeneficiary : ICommand
    {
        public Guid Id { get; set; }
        public string RCNumberOrIdNumber { get; set; }
        public string BVN { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid ApprovedBy { get; set; }
        public DateTime ApprovedAt { get; set; }
        [JsonConstructor]
        public AddBeneficiary(Guid id, string rCNumberOrIdNumber, string bvn, string name, string email, string phone, string address, Guid createdBy)
        {
            Id = id;
            RCNumberOrIdNumber = rCNumberOrIdNumber;
            BVN = bvn;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            CreatedBy = createdBy;
        }
    }
}
