﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Api.Gateway.WebClient.Messages.Commands.Beneficiary
{
    [MessageNamespace("beneficiaries")]
    public class UpdateBeneficiary : ICommand
    {
        public Guid Id { get;  set; }
        public string RCNumberOrIdNumber { get;  set; }
        public string BVN { get;  set; }
        public string Name { get;  set; }
        public string Email { get;  set; }
        public string Phone { get;  set; }
        public string Address { get;  set; }
        public DateTime UpdatedAt { get;  set; }
        public Guid UpdatedBy { get;  set; }
        public Guid ApprovedBy { get;  set; }
        public DateTime ApprovedAt { get;  set; }

        public bool IsActive { get;  set; }
        public bool IsBlacklisted { get;  set; }
        public int ApprovalStatus { get;  set; }
        [JsonConstructor]
        public UpdateBeneficiary(Guid id, string rcNumberOrIdNumber, string bvn, string name, string email, string phone, string address, Guid updatedBy, Guid approvedBy, bool isBlacklisted, int approvalStat, bool isActive)
        {
            Id = id;
            RCNumberOrIdNumber = rcNumberOrIdNumber;
            BVN = bvn;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            UpdatedBy = updatedBy;
            UpdatedAt = DateTime.Now;
            ApprovedBy = approvedBy;
            IsBlacklisted = isBlacklisted;
            ApprovalStatus = approvalStat;
            IsActive = isActive;
        }
    }
}
