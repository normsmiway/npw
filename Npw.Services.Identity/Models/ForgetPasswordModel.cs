﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Models
{
    public class ForgetPasswordModel
    {
        public string UserNameOrEmail { get; set; }
    }
}
