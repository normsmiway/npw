﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Models
{
    public class QuestionAndAnswerModel
    {
        public string UserNameOrEmail { get; set; }

        [Required(ErrorMessage ="Please provide at least one question and answer before submitting")]
        public IDictionary<string, string> Sqas { get; set; }
    }
}
