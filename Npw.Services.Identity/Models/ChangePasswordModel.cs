﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Models
{
 

    public class ChangePasswordModel
    {
        public Guid UserId { get; private set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string RefreshToken { get; set; }
    }
}
