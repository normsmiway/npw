﻿using System;

namespace Npw.Services.Identity.Models
{
    public class SignUpModel
    {
        public Guid Id { get; private set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
    }
}
