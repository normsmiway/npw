﻿using Npw.Services.Identity.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Providers
{
    public class PasswordProvider
    {

        public string Pin { get; private set; }

        public string Raw { get; private set; }
        public int MaxLength { get; private set; }

        


        public PasswordProvider(int maxLength)
        {
            MaxLength = maxLength;

        }

        #region Pin generation Strategies
        private void Generate()
        {
            (Raw, Pin) = NumberGenerator.Generate(MaxLength).ToNpwPassword(MaxLength);
        }


        #endregion

        public (string, string) Provide()
        {
            Generate();
            return (Pin, Raw);
        }


    }
}
