﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Messages.Events
{
    public class DeviceChanged : IEvent
    {

        public Guid UserId { get; }

        [JsonConstructor]
        public DeviceChanged(Guid userId)
        {
            UserId = userId;
        }

    }
}
