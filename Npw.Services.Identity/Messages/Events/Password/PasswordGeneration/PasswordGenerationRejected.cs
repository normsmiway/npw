﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;

namespace Npw.Services.Identity.Messages.Events.PasswordGeneration
{

    public class PasswordGenerationRejected : IRejectedEvent
    {
        public Guid UserId { get; }
        public string Reason { get; }
        public string Code { get; }

        [JsonConstructor]
        public PasswordGenerationRejected(Guid userId, string reason, string code)
        {
            UserId = userId;
            Reason = reason;
            Code = code;
        }
    }
}
