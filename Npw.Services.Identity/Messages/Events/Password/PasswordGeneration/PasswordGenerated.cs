﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;

namespace Npw.Services.Identity.Messages.Events.PasswordGeneration
{
    public class PasswordGenerated : IEvent
    {
        public Guid UserId { get; private set; }
        public string Password { get; private set; }
        public string Email { get; private set; }
        public string Role { get; private set; }
        public string UserName { get; private set; }

        [JsonConstructor]
        public PasswordGenerated(Guid userId,string email,string role,string password,string userName)
        {
            UserId = userId;
            Email = email;
            Role = role;
            Password = password;
        }
    }

}
