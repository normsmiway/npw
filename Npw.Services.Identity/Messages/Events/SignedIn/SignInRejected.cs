﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Messages.Events
{
    public class SignInRejected : IRejectedEvent
    {
        public string Pin { get; }
        public string Reason { get; }
        public string Code { get; }

        [JsonConstructor]
        public SignInRejected(string pin, string reason, string code)
        {
            Pin = pin;
            Reason = reason;
            Code = code;
        }
    }
}
