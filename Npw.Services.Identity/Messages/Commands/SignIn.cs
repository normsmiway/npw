﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Messages.Commands
{
    public class SignIn : ICommand
    {
        public string UserNameOrEmail { get; private set; }
        public string Password { get; private set; }

        [JsonConstructor]
        public SignIn(string userNameOrEmail, string password)
        {
            UserNameOrEmail = userNameOrEmail;
            Password = password;

        }
    }


  
}
