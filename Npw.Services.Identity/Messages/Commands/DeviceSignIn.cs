﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Messages.Commands
{


    public class DeviceSignIn : ICommand
    {
        public string Pin { get; }
        public string Password { get; }
        public string DeviceId { get; }

        [JsonConstructor]
        public DeviceSignIn(string deviceId)
        {
            DeviceId = deviceId;
        }
    }
}
