﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Messages.Commands
{
    public class SignUp : ICommand
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        

        [JsonConstructor]
        public SignUp(Guid id, string email, string userName, string role)
        {
            Id = id;
            Role = role;
            Email = email;
            UserName = userName;
           
        }
    }

    
}
