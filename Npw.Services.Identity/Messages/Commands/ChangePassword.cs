﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Messages.Commands
{
    public class ChangePassword : ICommand
    {
        public Guid UserId { get; private set; }
        public string CurrentPassword { get; private set; }
        public string NewPassword { get; private set; }
        public string RefreshToken { get; private set; }

        [JsonConstructor]
        public ChangePassword(Guid userId, string currentPassword, string newPassword, string refreshToken)
        {
            UserId = userId;
            CurrentPassword = currentPassword;
            NewPassword = newPassword;
            RefreshToken = refreshToken;
        }
    }
}
