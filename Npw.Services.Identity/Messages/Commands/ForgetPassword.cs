﻿using N.Shared.Messages;
using Newtonsoft.Json;

namespace Npw.Services.Identity.Messages.Commands
{
    public class ForgetPassword : ICommand
    {
        public string UserNameOrEmail { get; private set; }

        [JsonConstructor]
        public ForgetPassword(string userNameOrEmail)
        {
            UserNameOrEmail = userNameOrEmail;
        }
    }
}
