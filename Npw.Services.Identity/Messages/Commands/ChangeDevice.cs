﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Messages.Commands
{
    public class ChangeDevice:ICommand
    {
        public Guid UserId { get; }
        public string Pin { get; }
        public string NewDeviceId { get; }
       public IDictionary<string,string> SecurityQnA { get; }

        [JsonConstructor]
        public ChangeDevice(Guid userId, 
            string pin,
            string newDeviceId)
        {
            UserId = userId;
            NewDeviceId = newDeviceId;
            Pin = pin;
        }
    }
}
