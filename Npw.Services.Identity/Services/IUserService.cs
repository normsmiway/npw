﻿using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Services
{
    public interface IUserService
    {
        [AllowAnyStatusCode]
        [Get("users/{id}")]
        Task<UserDto> GetAsync([Path] Guid id);
    }

    public class UserDto
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool Completed { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? CompletedAt { get; set; } //Aimed to be used to track whether or not user has completed security questions
        public string Gender { get; set; }
        public string FullName { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public DateTime? LastLoggedIn { get; set; }
        public bool NewUser => LastLoggedIn.HasValue;


    }
}
