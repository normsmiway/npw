﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using N.Shared.Authentication;
using N.Shared.Authorization;
using N.Shared.Authorization.Extensions;
using N.Shared.RabbitMq;
using N.Shared.Types;
using Npw.Services.Identity.Domain;
using Npw.Services.Identity.Messages.Events;
using Npw.Services.Identity.Repositories;

namespace Npw.Services.Identity.Services
{
    public class RefreshTokenService : IRefreshTokenService
    {
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUserRepository _userRepository;
        private readonly IJwtHandler _jwtHandler;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IClaimsProvider _claimsProvider;
        private readonly IBusPublisher _busPublisher;



        public RefreshTokenService(IRefreshTokenRepository refreshTokenRepository,
            IUserRepository userRepository,
            IJwtHandler jwtHandler,
            IPasswordHasher<User> passwordHasher,
            IClaimsProvider claimsProvider,
            IBusPublisher busPublisher)
        {
            _refreshTokenRepository = refreshTokenRepository;
            _userRepository = userRepository;
            _jwtHandler = jwtHandler;
            _claimsProvider = claimsProvider;
            _passwordHasher = passwordHasher;
            _busPublisher = busPublisher;
        }
        public async Task AddAsync(Guid userId)
        {
            var user = await _userRepository.GetAsync(userId);
            if (user is null)
            {
                throw new NException(ErrorCodes.UserNotFound,
                    $"User: '{userId} was not found'");
            }
            await _refreshTokenRepository.AddAsync(new RefreshToken(user, _passwordHasher));
        }

        public async Task<JsonWebToken> CreateAccessTokenAsync(string token)
        {
            var refreshToken = await _refreshTokenRepository.GetAsync(token);

            if (refreshToken is null)
            {
                throw new NException(ErrorCodes.RefreshTokenNotFound,
                    "Refresh token was not found");
            }

            if (refreshToken.Revoked)
            {
                throw new NException(ErrorCodes.RefreshTokenAlreadyRevoked,
                    $"Refresh token: '{refreshToken.Id}' was revoked.");
            }

            var user = await _userRepository.GetAsync(refreshToken.UserId);
            if (user is null)
            {
                throw new NException(ErrorCodes.UserNotFound,
                    $"User: '{refreshToken.UserId}' was not found.");
            }

            var claims = await _claimsProvider.GetAsync(user.Id);
            var permissions = $"{PermissionConstant.PolicyPrefix}{user.Permissions.PackToString(PermissionConstant.PolicyNameSplitSymbol)}";
            //var jwt = _jwtHandler.CreateToken(user.Id.ToString("N"), user.Role, permissions, claims);
            var jwt = _jwtHandler.CreateToken(user.Id.ToString("N"), user.Role,permissions, claims);
            jwt.RefreshToken = refreshToken.Token;
            await _busPublisher.PublishAsync(new AccessTokenRefreshed(user.Id), CorrelationContext.Empty);

            return jwt;
        }

        public async Task RevokeAsync(string token, Guid userId)
        {
            var refreshToken = await _refreshTokenRepository.GetAsync(token);
            if (refreshToken is null || refreshToken.UserId != userId)
            {
                throw new NException(ErrorCodes.RefreshTokenNotFound,
                    "Refresh token was not found.");
            }

            refreshToken.Revoke();
            await _refreshTokenRepository.UpdateAsync(refreshToken);
            await _busPublisher.PublishAsync(new RefreshTokenRevoked(refreshToken.UserId), CorrelationContext.Empty);
        }
    }
}
