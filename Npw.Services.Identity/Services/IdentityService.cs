﻿using Microsoft.AspNetCore.Identity;
using N.Shared.Authentication;
using N.Shared.Authorization;
using N.Shared.Authorization.Extensions;
using N.Shared.RabbitMq;
using N.Shared.Types;
using Npw.Services.Identity.Domain;
using Npw.Services.Identity.Extensions;
using Npw.Services.Identity.Messages.Commands;
using Npw.Services.Identity.Messages.Events;
using Npw.Services.Identity.Messages.Events.PasswordGeneration;
using Npw.Services.Identity.Policies;
using Npw.Services.Identity.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Services
{

    //Tasks:TODO Forget password, logout, security questtion and answers
    public class IdentityService : IIdentityService
    {


        private readonly IUserRepository _userRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IJwtHandler _jwtHandler;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IClaimsProvider _claimsProvider;
        private readonly IUSerSqaRepository _userSqaRepository;
        private readonly IBusPublisher _busPublisher;
        private readonly IPasswordHandler _passwordHandler;
        private readonly IAccessTokenService _accessTokenService;
        private readonly IRefreshTokenService _refreshTokenService;


        public IdentityService(IUserRepository userRepository,
          IPasswordHasher<User> passwordHasher,
          IJwtHandler jwtHandler,
          IRefreshTokenRepository refreshTokenRepository,
          IClaimsProvider claimsProvider,
          IUSerSqaRepository userSqaRepository,
          IAccessTokenService accessTokenService,
          IRefreshTokenService refreshTokenService,
          IBusPublisher busPublisher, IPasswordHandler passwordHandler)
        {
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
            _jwtHandler = jwtHandler;
            _refreshTokenRepository = refreshTokenRepository;
            _claimsProvider = claimsProvider;
            _userSqaRepository = userSqaRepository;
            _busPublisher = busPublisher;
            _passwordHandler = passwordHandler;
            _accessTokenService = accessTokenService;
            _refreshTokenService = refreshTokenService;

        }


        public async Task ChangePasswordAsync(Guid userId, string currentPassword, string newPassword, string refreshToken)
        {
            var user = await _userRepository.GetAsync(userId);
            if (user is null)
            {
                throw new NException(ErrorCodes.UserNotFound,
                    $"User with id: '{userId}' was not found.");
            }
            if (!user.IsActive || !user.IsDeletd)
            {
                throw new NException(ErrorCodes.User.InactiveUser,
                  $"This user is inactive, please see administrator");
            }
            if (!user.ValidatePassword(currentPassword, _passwordHasher))
            {
                throw new NException(ErrorCodes.InvalidCurrentPassword,
                    "Invalid current password.");
            }

            if (!_passwordHandler.IsValid(newPassword))
            {
                throw new NException(ErrorCodes.InvalidPassword,
                   $"Password does not match the required password rule");
            }

            #region Password Validation & QA
            //if (securityQnA != null)
            //{
            //    var tag = user.Email.Substring(0, user.Email.IndexOf('@'));

            //    user.SetUserSecurityTag(securityQnA, user.UserName ?? tag);

            //    foreach (var item in securityQnA)
            //    {
            //        await _userSqaRepository.AddAsync(new UserSqa(user.Id, item.Key, item.Value));
            //    }
            //}
            #endregion 

            user.SetPassword(newPassword, _passwordHasher);

            if (!user.PasswordUpdated)
                user.SetPasswordUpdateStatus(true);

            await _userRepository.UpdateAsync(user);


            await _busPublisher.PublishAsync(new PasswordChanged(userId), CorrelationContext.Empty);
        }

        public async Task CreateQuestionAndAnswerAsync(Guid userId, IDictionary<string, string> securityTags = null)
        {

            var user = await _userRepository.GetAsync(userId);

            if (user is null)
                throw new NException(ErrorCodes.UserNotFound, $"User with id: '{userId}' was not found.");

            #region Password Validation & QA
            if (securityTags != null)
            {
                var tag = user.Email.Substring(0, user.Email.IndexOf('@'));

                user.SetUserSecurityTag(securityTags, user.UserName ?? tag);
                await _userRepository.UpdateAsync(user);

                foreach (var item in securityTags)
                {
                    await _userSqaRepository.AddAsync(new UserSqa(user.Id, item.Key, item.Value));
                }
            }
            #endregion 
        }

        public async Task<IEnumerable<UserSqa>> GetUserSQA(Guid userId)
        {
            return await _userSqaRepository.FindAsync(userId);

        }

        public async Task<IEnumerable<UserSqa>> GetUserSSA(string userNameOrEmail)
        {
            return await _userSqaRepository.FindAsync(userNameOrEmail);
        }

        public async Task<JsonWebToken> SignInAsync(string userNameOrEmail, string password)
        {
            var user = await _userRepository.GetAsync(userNameOrEmail);

            if (user == null)
            {
                throw new NException(ErrorCodes.InvalidCredentials,
                  "Invalid User");
            }

            if (!user.ValidatePassword(password, _passwordHasher))
            {
                throw new NException(ErrorCodes.InvalidCredentials,
                    "Invalid Password.");
            }

            var refreshToken = new RefreshToken(user, _passwordHasher);

            var claims = await _claimsProvider.GetAsync(user.Id);

            var permissions = $"{user.Permissions?.PackToString(PermissionConstant.PolicyNameSplitSymbol)}" ?? null;

            //add login state to the claim
            claims.Bind(nameof(user.LastLoggedIn), user.LastLoggedIn)
                .Bind(nameof(user.NewUser), user.NewUser)
                .Bind(nameof(user.PasswordUpdated), user.PasswordUpdated);

            var jwt = _jwtHandler.CreateToken(user.Id.ToString("N"), user.Role, permissions, claims);

            jwt.RefreshToken = refreshToken.Token;

            await _refreshTokenRepository.AddAsync(refreshToken);

            #region Update User Logged In
            user.SetLastLoggedIn();
            await _userRepository.UpdateAsync(user);
            #endregion
            return jwt;

        }

        public async Task<string> SignUpAsync(Guid id, string email, string userName, string role = "user", string permissions = "canview:canregister")
        {
            if (await _userRepository.EmailExistsAsync(email))
            {
                throw new NException(ErrorCodes.EmailInUse,
                   $"Email: an account with {email} already registred with this system");
            }
            if (await _userRepository.UserNameExistsAsync(userName))
            {
                throw new NException(ErrorCodes.UserNameInUse,
                   $"User Name: an account with {userName} already registred with this system");
            }

            var user = new User(id, email, userName, role);

            if (string.IsNullOrWhiteSpace(role))
            {
                role = Role.User;
            }

            if (string.IsNullOrWhiteSpace(permissions))
            {
                permissions = $"{Permission.CanView},{Permission.CanRegister}";
            }

            user.SetPermissions(permissions);
            //Generate password
            var password = await _passwordHandler.GeneratePasswordAsync(user.Id);
            user.SetPassword(password, _passwordHasher);

            await _userRepository.AddAsync(user);

            //Pubish Password Generated to subscribers
            // await _busPublisher.PublishAsync(new PasswordGenerated(id, email, password), CorrelationContext.Empty);

            await _busPublisher.PublishAsync(new SignedUp(id, email, userName, role, password), CorrelationContext.Empty);

            //return userName;

            return password;

        }

        public async Task<bool> ValidateUserSqa(string userNameOrEmail, IDictionary<string, string> securityTags = null)
        {
            if (string.IsNullOrEmpty(userNameOrEmail))
            {
                throw new NException(ErrorCodes.UserNotFound,
                    $"Username or Email cannot be empty");
            }
            var user = await _userRepository.GetAsync(userNameOrEmail);
            if (user is null)
            {
                throw new NException(ErrorCodes.UserNotFound,
                  $"User with email or username {userNameOrEmail} does not exist");
            }

            var valid = await _userSqaRepository.IsValid(user.Id, securityTags);

            if (valid)
            {
                //Generate password
                var password = await _passwordHandler.GeneratePasswordAsync(user.Id);

                //Update User
                user.SetPassword(password, _passwordHasher);
                user.SetPasswordUpdateStatus();
                _ = _userRepository.UpdateAsync(user);

                _ = _busPublisher.PublishAsync(new PasswordGenerated(user.Id, user.Email, user.Role, password,user.UserName), CorrelationContext.Empty);
            }


            return valid;
        }


        #region Helper Method
        private bool ValidateSecurityQuestion(User user, IEnumerable<UserSqa> userSqa, IDictionary<string, string> questionAndAnswerPair)
        {

            int savedQuestionCout = userSqa.ToList().Count();
            int userProvidedQuestionCount = questionAndAnswerPair.ToList().Count();

            if (savedQuestionCout != userProvidedQuestionCount)
            {
                throw new NException(ErrorCodes.UmatchedSecurityQuestion, $"The security questions " +
                    $"and answers does not match for user {user.Id}");
            }

            foreach (var item in userSqa)
            {
                if (questionAndAnswerPair.ContainsKey(item.Question))
                {
                    //use key in questions to select matching answer
                    if (item.Answer == questionAndAnswerPair[item.Question])
                    {
                        throw new NException(ErrorCodes.InvalidAnserProvided, $"The answer provided to question {item.Question} is invalid");
                    }
                }

            }

            return true;
        }


        #endregion
    }
}
