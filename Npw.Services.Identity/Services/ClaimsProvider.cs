﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Services
{
    public class ClaimsProvider : IClaimsProvider
    {
        private readonly IUserService _userService;

        public ClaimsProvider(IUserService userService)
        {
            _userService = userService;
        }
        public async Task<IDictionary<string, string>> GetAsync(Guid Id)
        {
            var user = await _userService.GetAsync(Id);
            if (user != null)
            {
                #region
                //foreach (var property in user.GetType().GetProperties())
                //{
                //    var d = user.GetType().GetProperty(property.Name).GetValue(this, null).ToString() ?? string.Empty;
                //}
                #endregion
                // Provide your own claims collection if needed.
                return await Task.FromResult(new Dictionary<string, string>
                {
                    [nameof(user.Id)] = user.Id.ToString() ?? string.Empty,
                    [nameof(user.FirstName)] = user.FirstName ?? string.Empty,
                    [nameof(user.LastName)] = user.LastName ?? string.Empty,
                    [nameof(user.LastName)] = user.Email ?? string.Empty,
                    [nameof(user.UserName)] = user.UserName ?? string.Empty,
                    [nameof(user.Completed)] = user.Completed.ToString() ?? string.Empty,
                    [nameof(user.Gender)] = user.Gender ?? string.Empty,
                    [nameof(user.IsDeleted)] = user.IsDeleted.ToString() ?? string.Empty,
                    [nameof(user.IsActive)] = user.IsActive.ToString() ?? string.Empty,
                    [nameof(user.FullName)] = user.FullName ?? string.Empty,
                    [nameof(user.PhoneNumber)] = user.PhoneNumber ?? string.Empty
                });
            }

            return null;

            //  return await Task.FromResult(new Dictionary<string, string>());

        }
    }
}
