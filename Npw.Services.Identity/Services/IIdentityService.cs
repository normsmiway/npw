﻿using N.Shared.Authentication;
using Newtonsoft.Json.Serialization;
using Npw.Services.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Services
{
    public interface IIdentityService
    {
        Task<string> SignUpAsync(Guid id, string email, string userName, string role = Role.User, string permissions = "canview:canregister");
        Task<JsonWebToken> SignInAsync(string userNameOrEmail, string password);
        Task ChangePasswordAsync(Guid userId, string currentPassword, string newPassword, string refreshToken);
        Task CreateQuestionAndAnswerAsync(Guid userId, IDictionary<string, string> securityTags = null);
        Task<IEnumerable<UserSqa>> GetUserSQA(Guid userId);
        Task<IEnumerable<UserSqa>> GetUserSSA(string userNameOrEmail);
        Task<bool> ValidateUserSqa(string userNameOrEmail, IDictionary<string, string> securityTags = null);
    }

}

