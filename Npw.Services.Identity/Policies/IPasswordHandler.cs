﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Policies
{
    public interface IPasswordHandler
    {
        bool IsValid(string password);
        Task<string> GeneratePasswordAsync(Guid userId);
    }
}
