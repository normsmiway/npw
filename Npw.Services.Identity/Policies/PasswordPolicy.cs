﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Policies
{

    public class PasswordPolicy
    {
        public static int Minimum_Length = 7;
        public static int Upper_Case_length = 1;
        public static int Lower_Case_length = 1;
        public static int NonAlpha_length = 1;
        public static int Numeric_length = 1;




        public static bool IsValid(string Password)
        {
            if (Password.Length < Minimum_Length)
                return false;
            if (UpperCaseCount(Password) < Upper_Case_length)
                return false;
            if (LowerCaseCount(Password) < Lower_Case_length)
                return false;
            if (NumericCount(Password) < Numeric_length)
                return false;
            if (NonAlphaCount(Password) < NonAlpha_length)
                return false;

            return true;
        }

        private static int UpperCaseCount(string Password)
        {
            return Regex.Matches(Password, "[A-Z]").Count;
        }

        private static int LowerCaseCount(string Password)
        {
            return Regex.Matches(Password, "[a-z]").Count;
        }
        private static int NumericCount(string Password)
        {
            return Regex.Matches(Password, "[0-9]").Count;
        }
        private static int NonAlphaCount(string Password)
        {
            return Regex.Matches(Password, @"[^0-9a-zA-Z\._]").Count;
        }
    }

    public class PasswordOptions
    {
        public int MinimumLenght { get; set; }
        public int UpperCaseLenght { get; set; }
        public int LowerCaseLenght { get; set; }
        public int NumericCount { get; set; }
        public int SpcialCharacterLength { get; set; }

        //Password
        public int Length { get; set; }
        public bool DigitOnly { get; set; }

    }

 
}
