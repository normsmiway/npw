﻿using Npw.Services.Identity.Providers;
using Npw.Services.Identity.Repositories;
using System;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Policies
{
    public class PasswordHandler : IPasswordHandler
    {
        private readonly PasswordOptions _options;
        private PasswordProvider _provider;
        private readonly IUserPasswordRepository _repository;
        public PasswordHandler(IUserPasswordRepository repository, PasswordOptions options)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));

            PasswordPolicy.Lower_Case_length = _options.LowerCaseLenght;
            PasswordPolicy.Upper_Case_length = _options.UpperCaseLenght;
            PasswordPolicy.Minimum_Length = _options.MinimumLenght;
            PasswordPolicy.NonAlpha_length = _options.SpcialCharacterLength;
            PasswordPolicy.Numeric_length = _options.NumericCount;

            _repository = repository;
        }

        public async Task<string> GeneratePasswordAsync(Guid userId)
        {
            _provider = new PasswordProvider(_options.Length);
            _provider.Provide();

            string password = _options.DigitOnly ? _provider.Pin : _provider.Raw;

            await _repository.AddAsync(new Domain.UserPassword(password, userId));

            return password;
        }

        public bool IsValid(string password)
        {
            return PasswordPolicy.IsValid(password);
        }
    }
}
