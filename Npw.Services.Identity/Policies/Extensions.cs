﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using N.Shared;
using Npw.Services.Identity.Providers;

namespace Npw.Services.Identity.Policies
{
    public static class Extensions
    {

        private static readonly string SectionName = "password-settings";

        public static void AddPasswordPSettings(this IServiceCollection services)
        {
            IConfiguration configuration;
            using (var serviceProvider = services.BuildServiceProvider())
            {
                configuration = serviceProvider.GetService<IConfiguration>();
            }
            var section = configuration.GetSection(SectionName);
            var options = configuration.GetOptions<PasswordOptions>(SectionName);
            services.Configure<PasswordOptions>(section);
            services.AddSingleton(options);
            services.AddSingleton(typeof(PasswordProvider));
            services.AddSingleton<IPasswordHandler, PasswordHandler>();
        }
    }
}
