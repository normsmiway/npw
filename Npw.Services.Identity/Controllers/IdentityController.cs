﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.Authentication;
using N.Shared.Messages;
using N.Shared.Mvc;
using Npw.Services.Identity.Messages.Commands;
using Npw.Services.Identity.Models;
using Npw.Services.Identity.Services;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Controllers
{
    [Route("")]
    [ApiController]
    [Produces("application/json")]
    public class IdentityController : BaseController
    {
        private readonly IIdentityService _identityService;
        private readonly IRefreshTokenService _refreshTokenService;
        private readonly IAccessTokenService _accessTokenService;
        public IdentityController(IIdentityService identityService,
            IAccessTokenService accessTokenService,
            IRefreshTokenService refreshTokenService)
        {
            _identityService = identityService;
            _refreshTokenService = refreshTokenService;
            _accessTokenService = accessTokenService;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet("me")]
        public IActionResult Get() => Content($"Your id: '{UserId:N}'.");



        [HttpPost("sign-up")]
        [SwaggerResponse(200, Type = typeof(string))]
        public async Task<IActionResult> SignUp(SignUpModel model)
        {

            if (model is null || !ModelState.IsValid)
                return BadRequest($"Invalid request");



            SignUp command = new SignUp(model.Id, model.Email, model.UserName, model.Role);
            command.BindId(c => c.Id);
            var result = await _identityService.SignUpAsync(command.Id,
                command.Email, command.UserName, command.Role);

            return Ok(result);
        }

        [HttpPost("sign-in")]
        [SwaggerResponse(200, Type = typeof(JsonWebToken))]
        public async Task<IActionResult> SignIn(SignInModel model)
        {
            if (model is null || !ModelState.IsValid)
                return BadRequest("Invalid Request");

            SignIn command = new SignIn(model.UserNameOrEmail, model.Password);
            return Ok(await _identityService.SignInAsync(command.UserNameOrEmail, command.Password));
        }

        [HttpPost("me/forget")]
        public async Task<ActionResult> RetriveAccount(ForgetPasswordModel model)
        {
            if (model is null || !ModelState.IsValid)
                return BadRequest("Invalid Request");

            return Ok(await _identityService.GetUserSSA(model.UserNameOrEmail));

        }

        [JwtAuth]
        [HttpPut("me/password")]
        public async Task<ActionResult> ChangePassword(ChangePasswordModel model)
        {
            // await SignOut(model.RefreshToken);

            ChangePassword command = new ChangePassword(model.UserId, model.CurrentPassword, model.NewPassword, model.RefreshToken);
            await _identityService.ChangePasswordAsync(command.Bind(c => c.UserId, UserId).UserId,
               command.CurrentPassword, command.NewPassword, command.RefreshToken);




            return NoContent();
        }

        [JwtAuth]
        [HttpPost("me/sqa")]
        public async Task<ActionResult> CreateSecurityQuestionAndAnswer(QuestionAndAnswerModel model)
        {
            //if (ModelState.IsValid)
            //{
            //    return BadRequest("Invlaid Request, Please provide at least one question and answer before submitting");
            //}
            await _identityService.CreateQuestionAndAnswerAsync(UserId, model.Sqas);

            return NoContent();
        }
        // [JwtAuth]
        [HttpPost("me/validate/sqa")]
        public async Task<ActionResult> GetSecurityQuestionAndAnswer(QuestionAndAnswerModel model)
        {
            if (string.IsNullOrWhiteSpace(model.UserNameOrEmail))
                return BadRequest("Invalid request, please suppy username or email");

            var result = await _identityService.ValidateUserSqa(model.UserNameOrEmail, model.Sqas);
            if (!result)
                return BadRequest("Invalid Security Question and Answer");

            return Ok(result);
        }

        [JwtAuth]
        [HttpGet("me/get/sqa")]
        [SwaggerResponse(200, Type = typeof(IEnumerable<SqaDTO>))]
        public async Task<ActionResult> GetSecurityQuestionAndAnswer()
        {
            var result = await _identityService.GetUserSQA(UserId);
            if (result is null)
                return NotFound();

            return Ok(result.Select(c => new SqaDTO() { Id = c.Id, Answer = c.Answer, Question = c.Question }));
        }

        [HttpPost("sign-out")]
        public async Task<ActionResult> SignOut(SignOutModel model)
        {
            if (model is null || string.IsNullOrWhiteSpace(model.RefreshToken) || !ModelState.IsValid)
                return BadRequest("Invalide Request");

            await SignOut(model.RefreshToken);

            return NoContent();
        }


        private async Task SignOut(string refreshToken)
        {
            //Revoke Refresh Token
            await _refreshTokenService.RevokeAsync(refreshToken, UserId);
            //Deactivate Access Token
            await _accessTokenService.DeactivateCurrentAsync(new RevokeRefreshToken(UserId, string.Empty).UserId.ToString("N"));

        }

    }

    public class SignOut : IEvent
    {
        public SignOut(string refreshToken)
        {
            RefreshToken = refreshToken;
        }
        public string RefreshToken { get; private set; }
    }
    public class SignOutModel
    {
        public string RefreshToken { get; set; }
    }

    public class SqaDTO
    {
        public Guid Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        // public Guid UserId { get; set; }
        //public DateTime CreatedAt { get; set; }

    }
}
