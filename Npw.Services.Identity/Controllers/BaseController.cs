﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.Authorization.Extensions;
using System;
using System.Linq;

namespace Npw.Services.Identity.Controllers
{
    [Produces("application/json")]
    public class BaseController : ControllerBase
    {
        protected bool IsAdmin
            => User.IsInRole("admin");
        protected Guid UserId
            => string.IsNullOrWhiteSpace(User?.Identity.Name) ?
           Guid.Empty : Guid.Parse(User.Identity.Name);

        protected void Permissions()
        {
            var permissions = !User.HasPermission("");
        }
    }
}
