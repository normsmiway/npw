﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Npw.Services.Identity.Controllers
{

    [Route("")]
    public class HomeController : BaseController
    {
        // GET api/values
        [HttpGet("ping")]
        public IActionResult Get() => new RedirectResult("/docs");
        [HttpGet("sample")]
        public IActionResult GetSample() {
            var sample = new Dictionary<string, string>();
            sample.Add("A", "Altitude");
            sample.Add("B", "Beauty");
            sample.Add("C", "Altitude");
            sample.Add("D", "Durable");
            sample.Add("E", "Education");

            return Ok(sample);
        } 

    }
}