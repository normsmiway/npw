﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Extensions
{
    public static class DictionaryExtension
    {
        public static IDictionary<string, string> Bind(this IDictionary<string, string> pairs, object key, object value)
        {
            value = value != null ? value : string.Empty;
            pairs.Add(key.ToString(), value.ToString());
            return pairs;
        }
    }
}
