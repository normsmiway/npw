﻿using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.Messages;
using N.Shared.RabbitMq;
using N.Shared.Types;
using Npw.Services.Identity.Domain;
using Npw.Services.Identity.Repositories;
using System;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Handlers
{
    public class UserOperationsHandler : IEventHandler<UserActiveted>, IEventHandler<UserDeActiveted>, IEventHandler<UserDeleted>
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger<UserOperationsHandler> _logger;
        public UserOperationsHandler(IUserRepository userRepository, ILogger<UserOperationsHandler> logger)
        {
            _userRepository = userRepository;
            _logger = logger;
        }




        public async Task HandleAsync(UserActiveted @event, ICorrelationContext context)
        {
            var user = await GetUser(@event.UserId,"Activating");
            user.Active();

            await _userRepository.UpdateAsync(user);
        }


        public async Task HandleAsync(UserDeActiveted @event, ICorrelationContext context)
        {

            var user = await GetUser(@event.UserId, "Deactivating");
            user.Deactivate();

            await _userRepository.UpdateAsync(user);
        }


        public async Task HandleAsync(UserDeleted @event, ICorrelationContext context)
        {
            var user = await GetUser(@event.UserId, "Deleting");
            if (@event.SoftDelete) { user.Delete(); await _userRepository.UpdateAsync(user); }
            else { await _userRepository.DeleteAsync(@event.UserId); }
        }



        private async Task<User> GetUser(Guid userId, string operation)
        {
            var user = await _userRepository.GetAsync(userId);
            if (user is null)

            {
                _logger.LogError($"User with Id {userId} does not exits");
                throw new NException(ErrorCodes.UserNotFound, $"User with Id {userId} does not exits");
            }
            _logger.LogInformation($"{operation} user with Id:{userId}", user);

            return user;
        }
    }





    [MessageNamespace("users")]
    public class UserDeleted : IEvent
    {
        public Guid UserId { get; }
        public bool SoftDelete { get; }
        public UserDeleted(Guid userId, bool softDelete)
        {
            UserId = userId;
            SoftDelete = softDelete;
        }

    }

    [MessageNamespace("users")]
    public class UserActiveted : IEvent
    {
        public Guid UserId { get; private set; }

        public UserActiveted(Guid userId)
        {
            UserId = userId;
        }
    }

    [MessageNamespace("users")]
    public class UserDeActiveted : IEvent
    {
        public Guid UserId { get; }
        public UserDeActiveted(Guid userId)
        {
            UserId = userId;
        }
    }
}
