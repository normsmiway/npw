﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Domain
{
    public static class Permission
    {
        public const string CanView = "canview";
        public const string CanRegister = "canregister";

        public static bool IsValid(string permission)
        {
            if (!string.IsNullOrWhiteSpace(permission))
            {
                return false;
            }

            permission = permission.ToLowerInvariant();

            return permission == CanView || permission == CanRegister;
        }

    }
}
