﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Domain
{
    public class UserLocation
    {
        public double Longitude { get; private set; }
        public double Latitude { get; private set; }
        public string Address { get; private set; }
        public string Country { get; private set; }
        public string State { get; private set; }

        public UserLocation(double log,double lat,string add,string cnt,string state)
        {
            Longitude = log;
            Latitude = lat;
            Address = add;
            Country = cnt;
            State = state;
        }
    }
}
