﻿using N.Shared.Types;
using System;

namespace Npw.Services.Identity.Domain
{
    public class UserPassword : IIdentifiable
    {
      
        public string Password { get; private set; }
        public Guid UserId { get; private set; }
       
        public Guid Id { get; set; }

        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }

        public UserPassword()
        {

        }
        public UserPassword(string password, Guid userId)
        {
            Password = password;
            UserId = userId;
            Id = Guid.NewGuid();
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }


    }
}
