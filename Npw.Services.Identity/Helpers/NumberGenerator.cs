﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Helpers
{
    public static class NumberGenerator
    {

        /// <summary>
        /// </summary>
        /// <param name="reference"></param>
        /// <param name="maxLength"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        public static (string, string) ToNpwPassword(this string reference, int maxLength = 10, int range = 100)
        {
            if (string.IsNullOrEmpty(reference))
            {
                reference = Generate(maxLength);
            }
            var digitsOnly = string.Concat(reference.Where(c => !char.IsLetter(c))).ToString();
            digitsOnly = digitsOnly.ExtractAndNormalize(maxLength, range);
            return (reference, digitsOnly);
        }

        private static string ExtractAndNormalize(this string numbersOnly, int maxLength = 20, int range = 100)
        {
            if (numbersOnly.Length < maxLength)
            {
                for (int i = 0; i < maxLength - numbersOnly.Length; i++)
                {
                    var num = new Random(range).Next().ToString();
                    numbersOnly += num;
                }
                numbersOnly = numbersOnly.Length >= maxLength ? numbersOnly.Substring(0, maxLength) : numbersOnly;
            }

            return numbersOnly;
        }

        public static string Generate(int length = 20)
        {
            return Guid.NewGuid().ToString().Replace("-", "").Substring(0, length).ToUpper();
        }


    }
}
