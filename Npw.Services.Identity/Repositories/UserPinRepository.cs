﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using N.Shared.Mongo;
using Npw.Services.Identity.Domain;

namespace Npw.Services.Identity.Repositories
{
    public class UserPasswordRepository : IUserPasswordRepository
    {
        private readonly IMongoRepository<UserPassword> _repository;
        public UserPasswordRepository(IMongoRepository<UserPassword> repository)
        {
            _repository = repository;
        }
        public async Task AddAsync(UserPassword userPin)
            => await _repository.AddAsync(userPin);
        
        public async Task<UserPassword> GetAsync(Guid id)
            => await _repository.GetAsync(id);

        public Task UpdateAsync(UserPassword userPin)
            => _repository.UpdateAsync(userPin);
    }
}
