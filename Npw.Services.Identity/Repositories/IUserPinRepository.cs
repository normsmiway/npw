﻿using Npw.Services.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Repositories
{
    public interface IUserPasswordRepository
    {

        Task<UserPassword> GetAsync(Guid id);
        Task AddAsync(UserPassword user);
        Task UpdateAsync(UserPassword userPin);

    }
}
