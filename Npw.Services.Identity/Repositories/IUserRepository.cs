﻿using Npw.Services.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Repositories
{
    public interface IUserRepository
    {
        Task<User> GetAsync(Guid id);
        Task<bool> EmailExistsAsync(string email);
        Task<User> GetAsync(string userNameOrEmail);
        Task<bool> UserNameExistsAsync(string userName);
        Task AddAsync(User user);
        Task DeleteAsync(Guid userId);
        Task AddUserQsaAsync(IEnumerable<IDictionary<string, string>> userSqa, User user);
        Task UpdateAsync(User user);
    }
}
