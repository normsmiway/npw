﻿using Npw.Services.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Repositories
{
    public interface IUSerSqaRepository
    {
        Task<UserSqa> GetAsync(Guid id);
        Task<IEnumerable<UserSqa>> FindAsync(Guid userId);
        Task<IEnumerable<UserSqa>> FindAsync(string userNameOrEmail);
        Task AddAsync(UserSqa userSqa);
        Task UpdateAsync(UserSqa user);
        Task<IEnumerable<UserSqa>> GetAll();
        Task<bool> IsValid(Guid userId, IDictionary<string, string> sqa);
    }
}
