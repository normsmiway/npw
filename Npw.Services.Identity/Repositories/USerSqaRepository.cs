﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using N.Shared.Mongo;
using Npw.Services.Identity.Domain;

namespace Npw.Services.Identity.Repositories
{
    public class UserSqaRepository : IUSerSqaRepository
    {
        private readonly IMongoRepository<UserSqa> _repository;
        private readonly IUserRepository _userRepository;

        public UserSqaRepository(IMongoRepository<UserSqa> repository, IUserRepository userRepository)
        {
            _repository = repository;
            _userRepository = userRepository;
        }
        public async Task AddAsync(UserSqa userSqa)
            => await _repository.AddAsync(userSqa);

        public async Task<IEnumerable<UserSqa>> FindAsync(Guid userId)
            => await _repository.FindAsync(x => x.UserId == userId);

        public async Task<UserSqa> GetAsync(Guid id)
            => await _repository.GetAsync(id);

        public async Task UpdateAsync(UserSqa userSqa)
            => await _repository.UpdateAsync(userSqa);

        public async Task<bool> IsValid(Guid userId, IDictionary<string, string> sqas)
        {
            //var user = await _userRepository.GetAsync(userNameOrEmail);
            var SqaList = await FindAsync(userId);
            var isValid = false;



            foreach (UserSqa sqa in SqaList)
            {
                isValid = sqas.ContainsKey(sqa.Question) ?
                    sqa.Answer == sqas[sqas.Keys.Where(q => q == sqa.Question).FirstOrDefault()] : false;

                if (!isValid)
                    break;
            };



            #region
            //foreach (var sqa in SqaList)
            //{
            //    if (sqas.ContainsKey(sqa.Question))
            //    {
            //        var question = sqas.Keys.Where(q => q == sqa.Question).FirstOrDefault();
            //        if (!(sqa.Answer == sqas[question]))
            //            return false;
            //        return true;
            //    }
            //    else
            //        return false;
            //}
            //return true;
            #endregion
            return isValid;
        }

        public async Task<IEnumerable<UserSqa>> GetAll()
        {
            return await _repository.FindAsync(_ => true);
        }

        public async Task<IEnumerable<UserSqa>> FindAsync(string userNameOrEmail)
        {
            var user = await _userRepository.GetAsync(userNameOrEmail);

            if (user != null)
            {
                return await FindAsync(user.Id);
            }

            return null;
        }
    }
}
