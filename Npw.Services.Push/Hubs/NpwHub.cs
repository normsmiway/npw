﻿
using Microsoft.AspNetCore.SignalR;
using N.Shared.Authentication;
using System;
using System.Threading.Tasks;
using Npw.Services.Push.Framework;

namespace Npw.Services.Push.Hubs
{
    public class NpwHub : Hub
    {
        private readonly IJwtHandler _jwtHandler;


        public NpwHub(IJwtHandler jwtHandler)
        {
            _jwtHandler = jwtHandler;
        }

        public async Task InitializeAsync(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                await DisconnectAsync();
            }

            try
            {
                var payload = _jwtHandler.GetTokenPayload(token);
                if (payload==null)
                {
                    await DisconnectAsync();
                    return;
                }
                var group = Guid.Parse(payload.Subject).ToUserGroup();
                await Groups.AddToGroupAsync(Context.ConnectionId, group);
                await ConnectAsync();
            }
            catch (Exception ex)
            {

                throw;
            }

           
        }

        private async Task ConnectAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("connected");
        }

        private async Task DisconnectAsync()
        {
            await Clients.Client(Context.ConnectionId).SendAsync("disconnected");
        }

    }
}
