﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using N.Shared.Logging;
using N.Shared.Metrics;
using N.Shared.Mvc;
using N.Shared.Vault;

namespace Npw.Services.Push
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseLogging()
                .UseVault()
                .UseLockbox()
                .UseAppMetrics();

    }
}
