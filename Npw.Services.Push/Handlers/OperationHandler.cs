﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Push.Messages.Events;
using Npw.Services.Push.Services;
using System.Threading.Tasks;

namespace Npw.Services.Push.Handlers
{
    public class OperationHandler : IEventHandler<OperationPending>,
        IEventHandler<OperationCompleted>, IEventHandler<OperationRejected>
    {
        private readonly IHubService _hubService;
        public OperationHandler(IHubService hubService)
        {
            _hubService = hubService;
        }
        public async Task HandleAsync(OperationCompleted @event, ICorrelationContext context)
            => await _hubService.PublishOperationCompletedAsync(@event);

        public async Task HandleAsync(OperationRejected @event, ICorrelationContext context)
            => await _hubService.PublishOperationRejectedAsync(@event);
        public async Task HandleAsync(OperationPending @event, ICorrelationContext context)
            => await _hubService.PublishOperationPendingAsync(@event);
    }
}
