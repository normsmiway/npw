﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Push.Framework
{
    public class ClientOptions
    {
        public string Origins { get; set; }
    }
}
