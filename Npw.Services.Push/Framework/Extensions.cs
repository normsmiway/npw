﻿using System;

namespace Npw.Services.Push.Framework
{
    public static class Extensions
    {
        public static string ToUserGroup(this Guid userId)
            => $"users:{userId}";
    }
}
