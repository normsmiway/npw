﻿using Microsoft.AspNetCore.SignalR;
using N.Shared;
using Npw.Services.Push.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Push.Services
{
    public class HubWrapper : IHubWrapper
    {
        private readonly IHubContext<NpwHub> _hubContext;
        public HubWrapper(IHubContext<NpwHub> hubContext)
        {
            _hubContext = hubContext;
        }
        public async Task PublishToAllAsync(string message, object data)
            => await _hubContext.Clients.All.SendAsync(message, data);

        public async Task PublishToUserAsync(Guid userId, string message, object data)
            => await _hubContext.Clients.Group(userId.ToUserGroup()).SendAsync(message, data);
    }
}
