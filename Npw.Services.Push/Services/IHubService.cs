﻿using Npw.Services.Push.Messages.Events;
using System.Threading.Tasks;

namespace Npw.Services.Push.Services
{
    public interface IHubService
    {
        Task PublishOperationPendingAsync(OperationPending @event);
        Task PublishOperationCompletedAsync(OperationCompleted @event);
        Task PublishOperationRejectedAsync(OperationRejected @event);
    }
}
