﻿using N.Shared.Types;
using Npw.Services.Users.Domain;
using Npw.Services.Users.Query.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Npw.Services.Users.Repository
{
    public interface IUserRepository
    {
        Task CreateOrUpdateAsync(User user);
        Task<User> GetAsync(Guid id);
        Task<IEnumerable<User>> GetUsers(Expression<Func<User, bool>> predicate);
        Task<PagedResult<User>> BrowserUsers(BrowseUsers query);
    }
}
