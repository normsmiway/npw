﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using N.Shared.Mongo;
using N.Shared.Types;
using Npw.Services.Users.Domain;
using Npw.Services.Users.Query.Users;

namespace Npw.Services.Users.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoDatabase _database;
        private readonly IMongoRepository<User> _userRepository;
        public UserRepository(IMongoDatabase database, IMongoRepository<User> userRepository)
        {
            _database = database;
            _userRepository = userRepository;

        }

        public async Task<PagedResult<User>> BrowserUsers(BrowseUsers query)
        {
            return await _userRepository.BrowseAsync(_ => true, query);
        }

        public async Task CreateOrUpdateAsync(User user) =>
            await _database.GetCollection<User>
               ($"{nameof(User)}s").ReplaceOneAsync(u =>
               u.Id == user.Id, user, new UpdateOptions { IsUpsert = true });

        public async Task<User> GetAsync(Guid id) => await _userRepository.GetAsync(id);

        public async Task<IEnumerable<User>> GetUsers(Expression<Func<User, bool>> predicate)
            => await _userRepository.FindAsync(predicate);
    }
}
