﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using N.Shared.Dispatchers;
using Npw.Services.Users.Models;
using Npw.Services.Users.Query.Users;

namespace Npw.Services.Users.Controllers
{
    public class UsersController : BaseController
    {
        public UsersController(IDispatcher dispatcher) : base(dispatcher)
        {
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<UserDto>> Get([FromRoute]GetUser query)
        {
            return Single(await QueryAsync(query));
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDto>>> GetAll([FromQuery] BrowseUsers query)
        => GetCollection(await QueryAsync(query));
    }


}
