﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Users.Domain;
using Npw.Services.Users.Messages.Events.Identity;
using Npw.Services.Users.Repository;
using System.Threading.Tasks;

namespace Npw.Services.Users.Handlers.Identity
{
    public class SignedUpHandler : IEventHandler<SignedUp>
    {
        private readonly IUserRepository _userRepository;
        public SignedUpHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task HandleAsync(SignedUp @event, ICorrelationContext context)
        {
            var user = new User(@event.UserId, @event.Email, @event.UserName, @event.Role);

            await _userRepository.CreateOrUpdateAsync(user);
        }
    }
}
