﻿using N.Shared.Handlers;
using Npw.Services.Users.Models;
using Npw.Services.Users.Query.Users;
using Npw.Services.Users.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Users.Handlers.Users.Reads
{
    public class GetUserHandler : IQueryHandler<GetUser, UserDto>
    {
        private readonly IUserRepository _userRepository;
        public GetUserHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<UserDto> HandleAsync(GetUser query)
        {
            var user = await _userRepository.GetAsync(query.Id);

            return new UserDto()
            {
                Id = user.Id,
                Completed = user.Completed,
                CompletedAt = user.CompletedAt,
                CreatedAt = user.CreatedAt,
                CreatedBy = user.CreatedBy,
                Email = user.Email,
                FirstName = user.FirstName,
                FullName = user.FullName,
                Gender = user.Gender,
                IsActive = user.IsActive,
                IsDeletd = user.IsDeletd,
                LastName = user.LastName,
                UpdatedAt = user.UpdatedAt,
                UpdatedBy = user.UpdatedBy,
                UserName = user.UserName,
                PhoneNumber = user.PhoneNumber

            } ?? null;
        }
    }
}
