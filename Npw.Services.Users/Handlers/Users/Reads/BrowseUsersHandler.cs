﻿using N.Shared.Handlers;
using N.Shared.Types;
using Npw.Services.Users.Models;
using Npw.Services.Users.Query.Users;
using Npw.Services.Users.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Users.Handlers.Users.Reads
{
    public class BrowseUsersHandler : IQueryHandler<BrowseUsers, PagedResult<UserDto>>
    {
        private readonly IUserRepository _userRepository;
        public BrowseUsersHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<PagedResult<UserDto>> HandleAsync(BrowseUsers query)
        {
            var pagedResult = await _userRepository.BrowserUsers(query);

            var users = pagedResult.Items.Select(user => new UserDto()
            {
                Id = user.Id,
                Completed = user.Completed,
                Email = user.Email,
                CompletedAt = user.CompletedAt,
                CreatedAt = user.CreatedAt,
                CreatedBy = user.CreatedBy,
                FirstName = user.FirstName,
                Gender = user.Gender,
                IsActive = user.IsActive,
                IsDeletd = user.IsDeletd,
                LastName = user.LastName,
                UpdatedAt = user.UpdatedAt,
                UpdatedBy = user.UpdatedBy,
                UserName = user.UserName,
                PhoneNumber=user.PhoneNumber
            });

            return PagedResult<UserDto>.From(pagedResult, users);
        }
    }
}
