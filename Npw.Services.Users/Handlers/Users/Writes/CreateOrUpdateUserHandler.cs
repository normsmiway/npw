﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using N.Shared.Types;
using Npw.Services.Users.Domain;
using Npw.Services.Users.Messages.Commands;
using Npw.Services.Users.Messages.Events.Users;
using Npw.Services.Users.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Users.Handlers.Users
{
    public class CreateOrUpdateUserHandler : ICommandHandler<CreateOrUpdateUser>
    {
        private readonly IUserRepository _userRepository;
        private readonly IBusPublisher _busPublisher;
        public CreateOrUpdateUserHandler(IUserRepository userRepository, IBusPublisher busPublisher)
        {
            _busPublisher = busPublisher;
            _userRepository = userRepository;
        }

        public async Task HandleAsync(CreateOrUpdateUser command, ICorrelationContext context)
        {
            var user = await _userRepository.GetAsync(command.Id);

            if (user is null)
            {
                throw new NException("User_Creation_Error", "User does not exist");
            }

            user.Complete(command.LastName, command.FirstName, command.Gender,command.PhoneNumber);
            await _userRepository.CreateOrUpdateAsync(user);

            await _busPublisher.PublishAsync(new UserCreatedOrUpdated(user.Id, user.Email, user.UserName, user.Role, command.FirstName, command.LastName, command.Gender,command.PhoneNumber), context);
        }
    }
}
