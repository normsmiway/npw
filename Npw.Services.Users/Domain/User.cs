﻿using N.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Users.Domain
{
    public class User : IIdentifiable
    {
        public Guid Id { get; private set; }
        public string Email { get; private set; }
        public string UserName { get; private set; }
        public string Gender { get; private set; }
        public string PhoneNumber { get; private set; }
        public string LastName { get; private set; }
        public string FirstName { get; private set; }
        public string Role { get; set; }
        public DateTime CreatedAt { get; private set; }
        public Guid CreatedBy { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public Guid UpdatedBy { get; private set; }
        public bool IsDeletd { get; private set; }
        public bool IsActive { get; private set; }
        public bool Completed => CompletedAt.HasValue;
        public DateTime? CompletedAt { get; private set; }
        public string FullName => string.Concat(LastName + " " + FirstName);

        public User(Guid id, string email, string userName, string role)
        {
            Id = id;
            Email = email;
            UserName = userName;
            Role = role;
            CreatedAt = DateTime.UtcNow;
        }


        public void Complete(string lastName, string firstName, string gender,string phoneNumber)
        {
            LastName = lastName;
            FirstName = firstName;
            CompletedAt = DateTime.UtcNow;
            Gender = gender;
            PhoneNumber = phoneNumber;
            UpdatedAt = DateTime.Now;

        }

        public void ActivateUser()
        {
            IsActive = true;
        }

        public void BlockUser()
        {
            IsActive = false;
        }
        public void DeleteUser()
        {
            IsDeletd = true;
        }
        public void UndoDelete()
        {
            IsDeletd = false;
        }
    }
}
