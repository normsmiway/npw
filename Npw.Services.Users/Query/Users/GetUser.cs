﻿using N.Shared.Types;
using Npw.Services.Users.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Users.Query.Users
{
    public class GetUser : IQuery<UserDto>
    {
        public Guid Id { get; set; }
    }
}
