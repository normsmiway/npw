﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Users.Messages.Commands
{
    public class CreateOrUpdateUser : ICommand
    {
        public Guid Id { get; private set; }
        public string Email { get; private set; }
        public string UserName { get; private set; }
        public string Gender { get; set; }
        public string LastName { get; private set; }
        public string FirstName { get; private set; }
        public string Role { get; private set; }
        public string PhoneNumber { get; private set; }

        public DateTime CreatedAt { get; private set; }
        public Guid CreatedBy { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public Guid UpdatedBy { get; private set; }
        public bool IsDeletd { get; private set; }
        public bool IsActive { get; private set; }
        public bool Completed => CompletedAt.HasValue;
        public DateTime? CompletedAt { get; private set; }

        [JsonConstructor]
        public CreateOrUpdateUser(Guid id, string email, string userName, string role,string firstName,string lastName,string gender,string phoneNumber)
        {
            Id = id;
            Email = email;
            UserName = userName;
            Role = role;
            CreatedAt = DateTime.UtcNow;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            PhoneNumber = phoneNumber;
        }
    }
}
