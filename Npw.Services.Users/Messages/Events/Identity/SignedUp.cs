﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;

namespace Npw.Services.Users.Messages.Events.Identity
{
    [MessageNamespace("identity")]
    public class SignedUp : IEvent
    {
        public Guid UserId { get; }
        public string Email { get; }
        public string UserName { get; }
        public string Role { get; }
        public string Password { get; }

        [JsonConstructor]
        public SignedUp(Guid userId, string email, string userName, string role,string password)
        {
            UserId = userId;
            Email = email;
            UserName = userName;
            Role = role;
            Password = password;

        }
    }
}
