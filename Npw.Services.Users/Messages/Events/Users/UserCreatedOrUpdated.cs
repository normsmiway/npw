﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Users.Messages.Events.Users
{
    public class UserCreatedOrUpdated : IEvent
    {
        public Guid Id { get; }
        public string Email { get;  }
        public string UserName { get;  }
        public string Gender { get; }
        public string PhoneNumber { get; }
        public string LastName { get;  }
        public string FirstName { get; }
        public string Role { get;  }

     

        [JsonConstructor]
        public UserCreatedOrUpdated(Guid id, string email, string userName, string role, string firstName, string lastName, string gender,string phoneNumber)
        {
            Id = id;
            Email = email;
            UserName = userName;
            Role = role;
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            PhoneNumber = phoneNumber;
        }
    }
}
