﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.DataAccess.Interface;
using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Transactions.Domain;
using Npw.Services.Transactions.Messages.Commands.Transaction;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.ProceduresImplementation;
using Newtonsoft.Json;

namespace Npw.Services.Transactions.Handlers.Writes
{
    public class CreateTransactionScheduleHandler : ICommandHandler<CreateTransactionSchedule>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<Transaction> transactionrepo;
        private IDapperRepository<TransactionSchedule> transactionschedulerepo;
        private ILogger logger;
        private IConfiguration config;
        public CreateTransactionScheduleHandler(IDapperUnitofWork _unitofwork, ILogger<CreateTransactionScheduleHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            transactionrepo = unitofwork.Repository<Transaction>() as IDapperRepository<Transaction>;
            transactionschedulerepo = unitofwork.Repository<TransactionSchedule>() as IDapperRepository<TransactionSchedule>;
            logger = _logger;
            config = _config;
        }
        public async Task HandleAsync(CreateTransactionSchedule command, ICorrelationContext context)
        {
            logger.LogInformation("Creating Transaction Handler...Building Model");
            try
            {
                logger.LogInformation("Command Model is " + JsonConvert.SerializeObject(command));
                var transactionschedule = new TransactionSchedule();
                transactionschedule.CompleteModel(command.Id, command.UserId, command.TotalAmount);
                logger.LogInformation("Model Built...Getting Connevction String");
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                transactionrepo.ConnectionString = new SqlConnection(ConnectionString);
                logger.LogInformation("Creating Schedule...");
                if (transactionschedulerepo == null)
                {
                    logger.LogInformation("transactionschedulerepo is null...");
                }
                else
                {
                    logger.LogInformation("transactionschedulerepo is not null...");
                }
                object scheduleid = await transactionschedulerepo.InsertAsync(transactionschedule);
                unitofwork.Transaction = transactionschedulerepo.Transaction;
                logger.LogInformation("Scheduke Saved Executing Unit of Work..");
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Schedule Created...Creating Transactions For Schedule");
                Guid ScheduleID = (Guid)scheduleid;
                foreach (var item in command.Transactions)
                {
                    transactionrepo.ConnectionString = new SqlConnection(ConnectionString);
                    var transaction = new Transaction();
                    logger.LogInformation("Build Transaction Model");
                    transaction.CompleteModel(item.Amount, item.AccountNumber, item.SourceAccountNumber, item.AccountName, item.BeneficiaryId, ScheduleID);
                    logger.LogInformation("Model Built...Inserting Transaction");
                    //logger.LogInformation("Fetching Transaction....");
                    //var transaction = transactionrepo.Get(item.TransactionId);
                    //logger.LogInformation("Transaction Gotten...Updating Transaction ScheduleID");
                    //transaction.UpdateScheduleID(ScheduleID);
                    //transactionrepo.Update(transaction);
                    await transactionrepo.InsertAsync(transaction);
                    logger.LogInformation("Transaction Updated");
                }
                logger.LogInformation("Executing Unit of Work...");
                unitofwork.Transaction = transactionrepo.Transaction;
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Unit of Work Executed");
            }
            catch (Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation("Error Is " + ex.Message);
                logger.LogInformation("Stack Trace is " + ex.StackTrace);
            }
        }


    }
}
