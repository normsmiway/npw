﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.DataAccess.Interface;
using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Transactions.Domain;
using Npw.Services.Transactions.Messages.Commands.Transaction;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Handlers.Writes
{
    public class DeleteTransactionHandler : ICommandHandler<DeleteTransaction>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<Transaction> transactionrepo;
        private ILogger logger;
        private IConfiguration config;
        //private IRepository<TransactionSchedule> transactionschedulerepo;
        public DeleteTransactionHandler(IDapperUnitofWork _unitofwork, ILogger<DeleteTransactionHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            transactionrepo = unitofwork.Repository<Transaction>() as IDapperRepository<Transaction>;
            logger = _logger;
            config = _config;
            //transactionschedulerepo = unitofwork.Repository<TransactionSchedule>();
        }
        public async Task HandleAsync(DeleteTransaction command, ICorrelationContext context)
        {
            logger.LogInformation("Deleting Transaction From System.....");
            try
            {
                logger.LogInformation("Command Model is " + JsonConvert.SerializeObject(command));
                logger.LogInformation("Model Built...Getting Connevction String");
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                transactionrepo.ConnectionString = new SqlConnection(ConnectionString);
                logger.LogInformation("Getting Transaction....");
                var transaction = await transactionrepo.GetAsync(command.TransactionId);
                logger.LogInformation("Transaction Gotten.....Deleting Transaction");
                transactionrepo.Delete(transaction);
                unitofwork.Transaction = transactionrepo.Transaction;
                logger.LogInformation("Transaction Deleted......Saving Changes");
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Changes Saved");
            }
            catch (System.Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation("Error Is " + ex.Message);
                logger.LogInformation("Stack Trace is " + ex.StackTrace);
            }
        }


    }
}
