﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.DataAccess.Interface;
using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Transactions.Domain;
using Npw.Services.Transactions.Messages.Commands.Transaction;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.ProceduresImplementation;
using Newtonsoft.Json;

namespace Npw.Services.Transactions.Handlers.Writes
{
    public class CreateTransactionHandler : ICommandHandler<CreateTransaction>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<Transaction> transactionrepo;
        //private IDapperRepository<TransactionSchedule> transactionschedulerepo;
        private ILogger logger;
        private IConfiguration config;
        public CreateTransactionHandler(IDapperUnitofWork _unitofwork, ILogger<CreateTransactionHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            transactionrepo = unitofwork.Repository<Transaction>() as IDapperRepository<Transaction>;
            //transactionschedulerepo = unitofwork.Repository<TransactionSchedule>() as IDapperRepository<TransactionSchedule>;
            logger = _logger;
            config = _config;
        }
        public async Task HandleAsync(CreateTransaction command, ICorrelationContext context)
        {
            logger.LogInformation("Creating Transaction Handler...Building Model");
            try
            {
                logger.LogInformation("Command Model is " + JsonConvert.SerializeObject(command));
                logger.LogInformation("Model Built...Getting Connevction String");
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                transactionrepo.ConnectionString = new SqlConnection(ConnectionString);
                var transaction = new Transaction();
                logger.LogInformation("Build Transaction Model");
                Guid ScheduleId = new Guid();
                transaction.CompleteModel(command.Amount, command.AccountNumber, command.SourceAccountNumber, command.AccountName, command.BeneficiaryId, ScheduleId);
                logger.LogInformation("Model Built...Inserting Transaction");
                await transactionrepo.InsertAsync(transaction);
                logger.LogInformation("Transaction Inserted");
                logger.LogInformation("Executing Unit of Work...");
                unitofwork.Transaction = transactionrepo.Transaction;
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Unit of Work Executed");
            }
            catch (Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation("Error Is " + ex.Message);
                logger.LogInformation("Stack Trace is " + ex.StackTrace);
            }
        }


    }
}
