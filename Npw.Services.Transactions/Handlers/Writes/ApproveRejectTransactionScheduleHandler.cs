﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.DataAccess.Interface;
using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Transactions.Domain;
using Npw.Services.Transactions.Messages.Commands.Transaction;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Handlers.Writes
{
    public class ApproveRejectTransactionScheduleHandler : ICommandHandler<ApproveRejectTransaction>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<Transaction> transactionrepo;
        private ILogger logger;
        private IConfiguration config;
        public ApproveRejectTransactionScheduleHandler(IDapperUnitofWork _unitofwork, ILogger<ApproveRejectTransactionScheduleHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            transactionrepo = unitofwork.Repository<Transaction>() as IDapperRepository<Transaction>;
            logger = _logger;
            config = _config;
        }
        public async Task HandleAsync(ApproveRejectTransaction command, ICorrelationContext context)
        {
            logger.LogInformation("Approving All Transactions.....");
            string ConnectionString = config.GetValue<string>("NpwConnectionString");
            logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
            transactionrepo.ConnectionString = new SqlConnection(ConnectionString);
            try
            {
                logger.LogInformation("Command Model is " + JsonConvert.SerializeObject(command));
                foreach (var item in command.transactions)
                {
                    logger.LogInformation("Getting Transaction.....");

                    var transaction = await transactionrepo.GetAsync(item.TransactionID);
                    logger.LogInformation("Transaction Gotten...Updating Transaction.....");
                    transaction.UpdateStatus(item.ApprovalStatus);
                    transactionrepo.Update(transaction);
                    logger.LogInformation("Record Updated.....");
                }
                logger.LogInformation("Executing Unit of Work.....");
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Unit of Work Executed.....");
            }
            catch (System.Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation("Error Is " + ex.Message);
                logger.LogInformation("Stack Trace is " + ex.StackTrace);
            }
        }


    }
}
