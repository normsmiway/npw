﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Transactions.Domain;
using Npw.Services.Transactions.Messages.Commands.Transaction;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Handlers.Writes
{
    public class ArchiveTransactionHandler : ICommandHandler<ArchiveTransaction>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<Transaction> transactionrepo;
        private ILogger logger;
        private IConfiguration config;
        public ArchiveTransactionHandler(IDapperUnitofWork _unitofwork, ILogger<ArchiveTransactionHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            transactionrepo = unitofwork.Repository<Transaction>() as IDapperRepository<Transaction>;
            logger = _logger;
            config = _config;
        }
        public async Task HandleAsync(ArchiveTransaction command, ICorrelationContext context)
        {
            logger.LogInformation("Archiving Transaction Handler...Building Model");
            try
            {
                logger.LogInformation("Command Model is " + JsonConvert.SerializeObject(command));
                logger.LogInformation("Model Built...Getting Connevction String");
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                transactionrepo.ConnectionString = new SqlConnection(ConnectionString);
                logger.LogInformation("Creating Schedule...");
                foreach (var item in command.TransactionIds)
                {
                    transactionrepo.ConnectionString = new SqlConnection(ConnectionString);
                    logger.LogInformation("Fetching Transaction....");
                    var transaction = transactionrepo.Get(item);
                    logger.LogInformation("Transaction Gotten...Archiving Transaction...");
                    transaction.Archive();
                    transactionrepo.Update(transaction);
                    logger.LogInformation("Transaction Archived");
                }
                logger.LogInformation("Executing Unit of Work...");
                unitofwork.Transaction = transactionrepo.Transaction;
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Unit of Work Executed");
            }
            catch (Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation("Error Is " + ex.Message);
                logger.LogInformation("Stack Trace is " + ex.StackTrace);
            }
        }


    }
}
