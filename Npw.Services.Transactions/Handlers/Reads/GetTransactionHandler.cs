﻿using N.Shared.Handlers;
using Npw.Services.Transactions.Domain;
using Npw.Services.Transactions.Models;
using Npw.Services.Transactions.Query.Transaction;
using System.Threading.Tasks;
using N.Shared.DataAccess.Interface;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using N.Shared.Types;
using Newtonsoft.Json;

namespace Npw.Services.Transactions.Handlers.Reads
{
    public class GetTransactionHandler : IQueryHandler<GetTransaction, TransactionDTO>
    {
        //private IUnitOfWork unitofwork;
        //private IRepository<Transaction> transactionrepo;
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<Transaction> transactionrepo;
        private ILogger logger;
        private IConfiguration config;
        public GetTransactionHandler(IDapperUnitofWork _unitofwork, ILogger<GetTransactionHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            transactionrepo = unitofwork.Repository<Transaction>() as IDapperRepository<Transaction>;
            logger = _logger;
            config = _config;
        }

        public async Task<TransactionDTO> HandleAsync(GetTransaction query)
        {
            logger.LogInformation("Getting Transaction....");
            try
            {
                logger.LogInformation("Model is " + JsonConvert.SerializeObject(query));
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                transactionrepo.ConnectionString = new SqlConnection(ConnectionString);
                logger.LogInformation("Calling Database");
                var trepo = await transactionrepo.GetAsync(query.Id);
                logger.LogInformation("Transaction Gotten....");
                return new TransactionDTO()
                {
                    AccountName = trepo.AccountName,
                    AccountNumber = trepo.AccountNumber,
                    ApprovalStatus = trepo.ApprovalStatus,
                    Amount = trepo.Amount,
                    SourceAccountNumber = trepo.SourceAccountNumber,
                    Date = trepo.CreatedOn,
                    Id = trepo.Id

                } ?? null;
            }
            catch (System.Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation(ex.Message);
                logger.LogInformation(ex.StackTrace);
                throw new NException("96", "An Error Occured While Fetching Transaction Schedules");
            }
        }


    }
}
