﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.DataAccess.Interface;
using N.Shared.Handlers;
using N.Shared.Types;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Transactions.Domain;
using Npw.Services.Transactions.Models;
using Npw.Services.Transactions.Query.Transaction;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Handlers.Reads
{
    public class GetTransactionUnderScheduleHandler : IQueryHandler<GetTransactionsUnderTransactionSchedule, ScheduleTransactionDTO>
    {
        //private IUnitOfWork unitofwork;
        //private IRepository<Transaction> transactionrepo;
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<Transaction> transactionschedulerepo;
        private ILogger logger;
        private IConfiguration config;
        public GetTransactionUnderScheduleHandler(IDapperUnitofWork _unitofwork, ILogger<GetTransactionUnderScheduleHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            transactionschedulerepo = unitofwork.Repository<Transaction>() as IDapperRepository<Transaction>;
            logger = _logger;
            config = _config;
        }

        public async Task<ScheduleTransactionDTO> HandleAsync(GetTransactionsUnderTransactionSchedule query)
        {
            logger.LogInformation("Getting All Transaction Under a Schedule");
            try
            {
                logger.LogInformation("Model is " + JsonConvert.SerializeObject(query));
                logger.LogInformation("Getting Transactions");
                var transactionschedId = query.TransactionScheduleId;
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                transactionschedulerepo.ConnectionString = new SqlConnection(ConnectionString);
                logger.LogInformation("Calling Database");
                var transactions = await transactionschedulerepo.GetAsync(x => x.TransactionScheduleId == transactionschedId);
                logger.LogInformation("Records Gotten.....Building Model....");
                var transactionsdto = new List<TransactionDTO>();
                foreach (var item in transactions)
                {
                    var tranrep = new TransactionDTO()
                    {
                        AccountName = item.AccountName,
                        AccountNumber = item.AccountNumber,
                        ApprovalStatus = item.ApprovalStatus,
                        Amount = item.Amount,
                        SourceAccountNumber = item.SourceAccountNumber,
                        Date = item.CreatedOn,
                        Id = item.Id
                    };
                    transactionsdto.Add(tranrep);
                }
                logger.LogInformation("Model Built");
                return new ScheduleTransactionDTO()
                {
                    transactions = transactionsdto
                } ?? null;
            }
            catch (System.Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation(ex.Message);
                logger.LogInformation(ex.StackTrace);
                throw new NException("96", "An Error Occured While Fetching Transaction Schedules");
            }
        }


    }
}
