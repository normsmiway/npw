﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.DataAccess.Interface;
using N.Shared.Handlers;
using N.Shared.Types;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Transactions.Domain;
using Npw.Services.Transactions.Models;
using Npw.Services.Transactions.Query.Transaction;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Handlers.Reads
{
    public class GetTransactionScheduleHandler : IQueryHandler<GetTransactionSchedule, TransactionScheduleDTO>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<TransactionSchedule> transactionschedulerepo;
        private ILogger logger;
        private IConfiguration config;
        public GetTransactionScheduleHandler(IDapperUnitofWork _unitofwork, ILogger<GetTransactionScheduleHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            transactionschedulerepo = unitofwork.Repository<TransactionSchedule>() as IDapperRepository<TransactionSchedule>;
            logger = _logger;
            config = _config;
        }

        public async Task<TransactionScheduleDTO> HandleAsync(GetTransactionSchedule query)
        {
            logger.LogInformation("Getting Transaction Schedule");
            logger.LogInformation("Model is " + JsonConvert.SerializeObject(query));
            try
            {
                //logger.LogInformation("Model is ." + JsonConvert.SerializeObject(query));
                //var scheduleid = query.Id;
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                transactionschedulerepo.ConnectionString = new SqlConnection(ConnectionString);
                logger.LogInformation("Calling Database");
                var schedules = await transactionschedulerepo.GetAsync(null,null,false);
                logger.LogInformation("All Schedules Gotten...");
                var scheduledto = new List<ScheduleDTO>();
                foreach (var item in schedules)
                {
                    var model = new ScheduleDTO
                    {
                        BatchNumber = item.BatchNumber,
                        Id = item.Id,
                        TotalAmount = item.TotalAmount
                    };
                    scheduledto.Add(model);
                }
                logger.LogInformation("View Model Built");
                return new TransactionScheduleDTO()
                {
                    transactionschedules = scheduledto
                } ?? null;
            }
            catch (System.Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation(ex.Message);
                logger.LogInformation(ex.StackTrace);
                throw new NException("96", "An Error Occured While Fetching Transaction Schedules");
            }
        }


    }
}
