﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Messages.Commands.Transaction
{
    public class ArchiveTransaction : ICommand
    {
        public Guid Id { get; set; }

        public Guid TransactionScheduleId { get; set; }
        public List<Guid> TransactionIds { get; set; }

        [JsonConstructor]
        public ArchiveTransaction(Guid _Id, Guid _TransactionScheduleId, List<Guid> _TransactionIds)
        {
            Id = _Id;
            TransactionScheduleId = _TransactionScheduleId;
            TransactionIds = _TransactionIds;
        }
    }
}
