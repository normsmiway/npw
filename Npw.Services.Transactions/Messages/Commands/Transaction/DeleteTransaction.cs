﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Messages.Commands.Transaction
{
    public class DeleteTransaction : ICommand
    {
        public Guid TransactionId { get;  set; }

        [JsonConstructor]
        public DeleteTransaction(Guid transactionId)
        {
            TransactionId = transactionId;
        }
    }
}
