﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Messages.Commands.Transaction
{
    public class CreateTransaction : ICommand
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public decimal Amount { get; set; }

        public string AccountNumber { get; set; }

        public string SourceAccountNumber { get; set; }

        public string AccountName { get; set; }

        public string Narration { get; set; }

        public string ApprovalCode { get; set; }

        public Guid BeneficiaryId { get; set; }
        //public Guid TransactionScheduleId { get; set; }

        [JsonConstructor]
        public CreateTransaction(Guid _Id, Guid _UserId, decimal _Amount, string _AccountNumber, string _AccountName, Guid _BeneficiaryId, string _Narration, string _ApprovalCode)
        {
            Id = _Id;
            UserId = _UserId;
            Amount = _Amount;
            AccountNumber = _AccountNumber;
            AccountName = _AccountName;
            BeneficiaryId = _BeneficiaryId;
            Narration = _Narration;
            ApprovalCode = _ApprovalCode;
        }


    }

    
}
