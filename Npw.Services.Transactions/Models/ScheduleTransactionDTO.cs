﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Models
{
    public class ScheduleTransactionDTO
    {
        public List<TransactionDTO> transactions { get; set; }
    }
}
