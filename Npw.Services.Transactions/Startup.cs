﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Consul;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using N.Shared;
using N.Shared.Consul;
using N.Shared.DataAccess.Interface;
using N.Shared.Dispatchers;
using N.Shared.Jaeger;
using N.Shared.Mongo;
using N.Shared.Mvc;
using N.Shared.RabbitMq;
using N.Shared.Redis;
using N.Shared.Swagger;
using N.Shared.Types;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Shared.DataAccess.DapperProvider.Implementation;
using Npw.Services.Transactions.Messages.Commands.Transaction;
using System;
using System.Reflection;

namespace Npw.Services.Transactions
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer Container { get; private set; }
        // This method gets called by the runtime. Use this method to add services to the container.

        //services.AddTransient<IUnitOfWork, DapperUnitofWork>();
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCustomMvc();
            services.AddSwaggerDocs();
            services.AddConsul();

            services.AddJaeger();
            services.AddOpenTracing();
            services.AddRedis();
            services.AddTransient<IDapperUnitofWork, DapperUnitofWork>();

            services.AddInitializers(typeof(IMongoDbInitializer));


            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly())
                .AsImplementedInterfaces();
            builder.Populate(services);


            builder.AddDispatchers();

            builder.AddRabbitMq();
            //builder.AddMongo();
            //builder.AddMongoRepository<User>("Users");

            Container = builder.Build();

            return new AutofacServiceProvider(Container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
             IApplicationLifetime applicationLifetime, IConsulClient client,
             IStartupInitializer startupInitializer)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "local")
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAllForwardedHeaders();
            app.UseSwaggerDocs();
            app.UseErrorHandler();
            app.UseServiceId();
            app.UseMvc();
            app.UseRabbitMq()
               .SubscribeCommand<CreateTransactionSchedule>()
               .SubscribeCommand<ApproveRejectTransaction>()
               .SubscribeCommand<DeleteTransaction>()
                .SubscribeCommand<CreateTransaction>()
                 .SubscribeCommand<ArchiveTransaction>()
               ;
            var consulServiceId = app.UseConsul();
            applicationLifetime.ApplicationStopped.Register(() =>
            {
                client.Agent.ServiceDeregister(consulServiceId);
                Container.Dispose();
            });
            startupInitializer.InitializeAsync();
        }
    }
}
