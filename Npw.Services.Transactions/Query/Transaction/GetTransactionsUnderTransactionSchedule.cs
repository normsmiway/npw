﻿using N.Shared.Types;
using Npw.Services.Transactions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Query.Transaction
{
    public class GetTransactionsUnderTransactionSchedule : IQuery<ScheduleTransactionDTO>
    {
        public Guid TransactionScheduleId { get; set; }
    }
}
