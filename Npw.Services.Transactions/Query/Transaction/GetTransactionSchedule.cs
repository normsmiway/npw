﻿using N.Shared.Types;
using Npw.Services.Transactions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Query.Transaction
{
    public class GetTransactionSchedule : IQuery<TransactionScheduleDTO>
    {
        public Guid Id { get; set; }
    }
}
