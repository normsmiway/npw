﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.Dispatchers;
using Npw.Services.Transactions.Models;
using Npw.Services.Transactions.Query.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Controllers
{
    public class TransactionsController : BaseController
    {
        public TransactionsController(IDispatcher dispatcher) : base(dispatcher)
        {
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<TransactionDTO>> GetTransaction([FromRoute]GetTransaction query)
        {
            return Single(await QueryAsync(query));
        }


        [HttpGet("transactionschedule")]
        public async Task<ActionResult<TransactionScheduleDTO>> GetAllTransactionSchedules([FromQuery] GetTransactionSchedule query)
        {
            return Single(await QueryAsync(query));
        }
        


        [HttpGet("transactionsunderschedule/{transactionScheduleId:guid}")]
        public async Task<ActionResult<ScheduleTransactionDTO>> GetAllTransactionsUnderSchedules([FromRoute] GetTransactionsUnderTransactionSchedule query)
        => Single(await QueryAsync(query));
    }
}
