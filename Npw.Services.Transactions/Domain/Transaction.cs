﻿using N.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Domain
{
    public class Transaction : IIdentifiable
    {
        public Guid Id { get; private set; }
        public decimal Amount { get; private set; }

        public string AccountNumber { get; private set; }

        public string SourceAccountNumber { get; private set; }

        public string AccountName { get; private set; }

        public Guid BeneficiaryId { get; private set; }
        public Guid TransactionScheduleId { get; private set; }

        public DateTime CreatedOn { get; private set; }

        public bool IsDeleted { get; private set; }

        public int ApprovalStatus { get; private set; }

        public void CompleteModel(decimal amount, string accountnumber, string sourceaccountnumber, string accountname, Guid beneficiaryId, Guid ScheduleId)
        {
            Id = new Guid();
            Amount = amount;
            AccountNumber = accountnumber;
            SourceAccountNumber = sourceaccountnumber;
            AccountName = accountname;
            BeneficiaryId = beneficiaryId;
            CreatedOn = DateTime.Now;
            IsDeleted = false;
            TransactionScheduleId = ScheduleId;
        }

        public void UpdateStatus(int status)
        {
            ApprovalStatus = status;
        }

        public void UpdateScheduleID(Guid ScheduleId)
        {
            TransactionScheduleId = ScheduleId;
        }

        public void Archive()
        {
            IsDeleted = true;
        }

    }
}
