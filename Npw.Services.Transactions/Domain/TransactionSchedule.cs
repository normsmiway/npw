﻿using N.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Transactions.Domain
{
    public class TransactionSchedule : IIdentifiable
    {
        public Guid Id { get; private set; }

        public Guid UserId { get; private set; }

        public int BatchNumber { get; private set; }

        public decimal TotalAmount { get; private set; }

        public void CompleteModel(Guid id, Guid userId, decimal totalamount)
        {
            Id = id;
            UserId = userId;
            TotalAmount = totalamount;
            BatchNumber = 0;
        }

    }
}
