﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries
{
    public static class ErrorCodes
    {
        public static string InvalidEmail => "invalid_email";
        public static string BvnEmpty => "bvn_empty";
        public static string BvnExist => "bvn_exist";
        public static string BeneficiaryDoesNotExist => "beneficiary_does_not_exist";
        public static string BeneficiaryAcctNotExist => "beneficiary_account_does_not_exist"; 


    }
}
