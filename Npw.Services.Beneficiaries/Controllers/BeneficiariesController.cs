﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using N.Shared.Dispatchers;
using N.Shared.Mvc;
using Npw.Services.Beneficiaries.Messages.Commands;
using Npw.Services.Beneficiaries.Messages.Commands.Beneficiary;
using Npw.Services.Beneficiaries.Models;
using Npw.Services.Beneficiaries.Query.Beneficiary;
using Npw.Services.Beneficiaries.Query.BeneficiaryAccount;

namespace Npw.Services.Beneficiaries.Controllers
{
    public class BeneficiariesController : BaseController
    {

        public BeneficiariesController(IDispatcher dispatcher) : base(dispatcher)
        {
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<BeneficiaryDto>> GetBeneficiary([FromRoute]GetBeneficiary query)
        {
            return Single(await QueryAsync(query));
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BeneficiaryDto>>> GetAllBeneficiary([FromQuery] BrowseBeneficiaries query)
        => GetCollection(await QueryAsync(query));

        [HttpGet("GetBeneficiaryAccount/{id:guid}")]
        public async Task<ActionResult<BeneficiaryAccountDto>> GetBeneficiaryAccount([FromRoute]GetBeneficiaryAccount query)
        {
            return Single(await QueryAsync(query));
        }


        [HttpGet("GetBeneficiaryAccounts")]
        public async Task<ActionResult<IEnumerable<BeneficiaryAccountDto>>> GetAllBeneficiaryAccounts([FromQuery] BrowseBeneficiaryAccounts query)
        => GetCollection(await QueryAsync(query));

    }
}