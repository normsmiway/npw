﻿using N.Shared.Types;
using Npw.Services.Beneficiaries.Domain;
using Npw.Services.Beneficiaries.Query.Beneficiary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Repositories
{
    public interface IBeneficiaryAccountRepository
    {
        //Task<IEnumerable<BeneficiaryBankAccount>> GetAsync(Guid id);
        Task<BeneficiaryBankAccount> GetAsync(Guid Id);
        Task<bool> BeneficiaryAccountExistsAsync(Guid id, string accountNumber);
        Task AddAsync(BeneficiaryBankAccount user);
        Task UpdateAsync(BeneficiaryBankAccount user);
        Task<PagedResult<BeneficiaryBankAccount>> BrowserBeneficiaryAccounts(BrowseBeneficiaryAccounts query);

    }
}
