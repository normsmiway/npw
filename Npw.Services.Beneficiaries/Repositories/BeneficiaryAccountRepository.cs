﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using N.Shared.Mongo;
using N.Shared.Types;
using Npw.Services.Beneficiaries.Domain;
using Npw.Services.Beneficiaries.Query.Beneficiary;

namespace Npw.Services.Beneficiaries.Repositories
{
    public class BeneficiaryAccountRepository : IBeneficiaryAccountRepository
    {
        private readonly IMongoRepository<BeneficiaryBankAccount> _accountRepo;
        private readonly ILogger<BeneficiaryAccountRepository> _logger;

        public BeneficiaryAccountRepository(IMongoRepository<BeneficiaryBankAccount> accountRepo, ILogger<BeneficiaryAccountRepository> logger)
        {
            _accountRepo = accountRepo;
            _logger = logger;
        }
        public async Task AddAsync(BeneficiaryBankAccount user)
        {
            await _accountRepo.AddAsync(user);
        }

        public async Task<bool> BeneficiaryAccountExistsAsync(Guid id, string accountNumber)
        {
            return await _accountRepo.ExistsAsync(x => x.BeneficiaryUserId == id && x.AccountNumber == accountNumber);

        }

        public async Task<BeneficiaryBankAccount> GetAsync(Guid id)
        {
            return await _accountRepo.GetAsync(c => c.Id == id);
        }

        public async Task UpdateAsync(BeneficiaryBankAccount user)
        {
            await _accountRepo.UpdateAsync(user);

        }
        public async Task<PagedResult<BeneficiaryBankAccount>> BrowserBeneficiaryAccounts(BrowseBeneficiaryAccounts query)
        {
            return await _accountRepo.BrowseAsync(_ => true, query);
        }
    }
}
