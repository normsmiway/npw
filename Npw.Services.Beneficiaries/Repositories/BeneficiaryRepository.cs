﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using N.Shared.Mongo;
using N.Shared.Types;
using Npw.Services.Beneficiaries.Domain;
using Npw.Services.Beneficiaries.Query.Beneficiary;

namespace Npw.Services.Beneficiaries.Repositories
{
    public class BeneficiaryRepository : IBeneficiaryRepository
    {
        private readonly IMongoRepository<Beneficiary> _repository;
        //private readonly ILogger<BeneficiaryRepository> _logger;

        public BeneficiaryRepository(IMongoRepository<Beneficiary> repository)
        {
            _repository = repository;

        }
        public async Task AddAsync(Beneficiary user)
        {
            await _repository.AddAsync(user);
        }

        public async Task<bool> BeneficiaryExistsAsync(string bvn, string RcNoOrIdNo)
        {
            return await _repository.ExistsAsync(x => x.BVN == bvn || x.RCNumberOrIdNumber == RcNoOrIdNo);

        }

        public async Task<Beneficiary> GetAsync(Guid id)
        {
            return await _repository.GetAsync(u => (u.Id == id));
        }

        public async Task UpdateAsync(Beneficiary user)
        {
            await _repository.UpdateAsync(user);
        }

        public async Task<PagedResult<Beneficiary>> BrowserBeneficiaries(BrowseBeneficiaries query)
        {
            return await _repository.BrowseAsync(_ => true, query);
        }
    }
}
