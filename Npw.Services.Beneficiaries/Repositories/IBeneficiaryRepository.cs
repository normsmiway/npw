﻿using N.Shared.Types;
using Npw.Services.Beneficiaries.Domain;
using Npw.Services.Beneficiaries.Query.Beneficiary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Repositories
{
    public interface IBeneficiaryRepository
    {
        Task<Beneficiary> GetAsync(Guid id);
        //Task<IEnumerable<Beneficiary>> GetAsync(string bvn, string RcNoOrIdNo);
        Task<bool> BeneficiaryExistsAsync(string bvn, string RcNoOrIdNo);
        Task AddAsync(Beneficiary user);
        Task UpdateAsync(Beneficiary user);
        Task<PagedResult<Beneficiary>> BrowserBeneficiaries(BrowseBeneficiaries query);
    }
}
