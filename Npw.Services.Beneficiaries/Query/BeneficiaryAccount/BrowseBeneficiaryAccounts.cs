﻿using N.Shared.Types;
using Npw.Services.Beneficiaries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Query.Beneficiary
{
    public class BrowseBeneficiaryAccounts : PagedQueryBase, IQuery<PagedResult<BeneficiaryAccountDto>>
    {
    }
}
