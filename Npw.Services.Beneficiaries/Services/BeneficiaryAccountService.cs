﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using N.Shared.Types;
using Npw.Services.Beneficiaries.Domain;
using Npw.Services.Beneficiaries.Repositories;

namespace Npw.Services.Beneficiaries.Services
{
    public class BeneficiaryAccountService : IBeneficiaryAccountService
    {
        private readonly IBeneficiaryAccountRepository _accountRepo;
        public BeneficiaryAccountService(IBeneficiaryAccountRepository accountRepo)
        {
            _accountRepo = accountRepo;
        }
        public async Task AddBeneficiaryAccount(Guid id, string accountNumber, string accountName, Guid beneficiaryId, string bankName, string bankCode, Guid createdBy)
        {
            if (await _accountRepo.BeneficiaryAccountExistsAsync(beneficiaryId, accountNumber))
            {
                throw new NException(ErrorCodes.BvnExist,
                  $"Account Number {accountNumber} already added to the beneficiary");
            }
            var addAccount = new BeneficiaryBankAccount(id, accountNumber, accountName, beneficiaryId, bankName, bankCode, createdBy);
            await _accountRepo.AddAsync(addAccount);
        }

        public async Task ApproveAddBeneficiaryAccount(Guid Id, Guid approvedBy, int approvalStat)
        {
            var beneAccount = await _accountRepo.GetAsync(Id);
            if (beneAccount == null)
            {
                throw new NException(ErrorCodes.BeneficiaryAcctNotExist,
                  $"The beneficiary account does not exist");
            }
            beneAccount.SetApprovedBy(approvedBy, approvalStat);
            await _accountRepo.UpdateAsync(beneAccount);
            throw new NotImplementedException();
        }

        public async Task ChangeActiveStatus(Guid Id, Guid modifiedBy, bool isActive)
        {
            var beneAcct = await _accountRepo.GetAsync(Id);
            if (beneAcct == null)
            {
                throw new NException(ErrorCodes.BeneficiaryAcctNotExist,
                  $"The beneficiary account does not exist");
            }
            beneAcct.SetModifyActiveStatus(modifiedBy, isActive);
            await _accountRepo.UpdateAsync(beneAcct);
        }
        public async Task ApproveChangeActiveStatus(Guid Id, Guid UserId, int approvalStat)
        {
            var beneAcct = await _accountRepo.GetAsync(Id);
            if (beneAcct == null)
            {
                throw new NException(ErrorCodes.BeneficiaryAcctNotExist,
                  $"The beneficiary account does not exist");
            }
            beneAcct.ApproveModifyStatus(UserId, approvalStat);
            await _accountRepo.UpdateAsync(beneAcct);
        }
        public Task<IEnumerable<BeneficiaryBankAccount>> GetBeneficiaryBankAccounts()
        {
            throw new NotImplementedException();
        }

        public Task ModifyBeneficiaryBankAccount(string AccountNumber, Guid UserId)
        {
            throw new NotImplementedException();
        }

        public Task ApproveModifyBeneficaryBankAccount(string AccountNumber, Guid UserId)
        {
            throw new NotImplementedException();
        }

    }
}
