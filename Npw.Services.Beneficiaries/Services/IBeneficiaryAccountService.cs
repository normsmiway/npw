﻿using Npw.Services.Beneficiaries.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Services
{
    public interface IBeneficiaryAccountService
    {
        Task AddBeneficiaryAccount(Guid id, string accountNumber, string accountName, Guid beneficiaryId, string bankName, string bankCode, Guid createdBy);
        Task ApproveAddBeneficiaryAccount(Guid beneId, Guid approvedBy, int approvalStat);
        Task<IEnumerable<BeneficiaryBankAccount>> GetBeneficiaryBankAccounts();
        Task ModifyBeneficiaryBankAccount(string AccountNumber, Guid UserId);
        Task ApproveModifyBeneficaryBankAccount(string AccountNumber, Guid UserId);
        Task ChangeActiveStatus(Guid Id, Guid modifiedBy, bool isActive);
        Task ApproveChangeActiveStatus(Guid Id, Guid UserId, int approvalStat);
    }
}
