﻿using Npw.Services.Beneficiaries.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Services
{
    public interface IBeneficiaryService
    {
        Task AddBeneficiary(Guid userId, string name, string bvn, string identityNo, string address, string phone, string email,  Guid addedBy);
        Task ApproveAddbeneficiary(Guid userId, Guid approvedBy, int approveStat);
        Task<Beneficiary> GetBeneficiary(Guid id);
        Task BlacklistBeneficiary(Guid userId, Guid modifiedBy, bool blacklistStatus);
        Task ApprovedBlacklistBeneficiary(Guid userId, Guid approvedBy , int approvalStat);
    }
}
