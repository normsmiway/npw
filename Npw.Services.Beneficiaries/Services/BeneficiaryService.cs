﻿using N.Shared.Types;
using Npw.Services.Beneficiaries.Domain;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Services
{
    public class BeneficiaryService : IBeneficiaryService
    {
        private readonly IBeneficiaryRepository _beneRepo;
        public BeneficiaryService( IBeneficiaryRepository beneRepo)
        {
            _beneRepo = beneRepo;
        }

        public async Task AddBeneficiary(Guid userId, string name, string bvn, string RcNoOrIdNo, string address, string phone, string email, Guid addedBy)
        {
            if (await _beneRepo.BeneficiaryExistsAsync(bvn, RcNoOrIdNo))
            {
                throw new NException(ErrorCodes.BvnExist,
                   $"Beneficiary with {bvn} already registred with this system");
            }
            
            var beneficiary = new Beneficiary(userId, RcNoOrIdNo, bvn, name, email, phone, address, addedBy);
            await _beneRepo.AddAsync(beneficiary);

        }

        public async Task ApproveAddbeneficiary(Guid userId, Guid approvedBy, int approveStat)
        {
            var bene = await _beneRepo.GetAsync(userId);
            if (bene == null)
            {
                throw new NException(ErrorCodes.BeneficiaryDoesNotExist,
                   $"Beneficiary does not exist");
            }
            bene.SetApprovedBy(approvedBy, approveStat);
            await _beneRepo.UpdateAsync(bene);
        }

        public async Task BlacklistBeneficiary(Guid userId, Guid modifiedBy, bool blacklistStatus)
        {
            var bene = await _beneRepo.GetAsync(userId);
            if (bene == null)
            {
                throw new NException(ErrorCodes.BeneficiaryDoesNotExist,
                   $"Beneficiary does not exist");
            }
            bene.SetBlacklistStatus(modifiedBy, blacklistStatus);
            await _beneRepo.UpdateAsync(bene);
        }
        public async Task ApprovedBlacklistBeneficiary(Guid userId, Guid approvedBy, int approvalStat)
        {
            var bene = await _beneRepo.GetAsync(userId);
            if (bene == null)
            {
                throw new NException(ErrorCodes.BeneficiaryDoesNotExist,
                   $"Beneficiary does not exist");
            }
            bene.ApproveBlacklistStatus(approvedBy, approvalStat);
            await _beneRepo.UpdateAsync(bene);
        }

        public async Task<Beneficiary> GetBeneficiary(Guid id)
        {
            return await _beneRepo.GetAsync(id);
        }
    }
}
