﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Models
{
    public class BeneficiaryDto
    {
        public Guid Id { get;  set; }
        public string RCNumberOrIdNumber { get;  set; }
        public string BVN { get;  set; }
        public string Name { get;  set; }
        public string Email { get;  set; }
        public string Phone { get;  set; }
        public string Address { get;  set; }
        public DateTime CreatedAt { get;  set; }
        public Guid CreatedBy { get;  set; }
        public DateTime UpdatedAt { get;  set; }
        public Guid UpdatedBy { get;  set; }
        public Guid ApprovedBy { get;  set; }
        public DateTime ApprovedAt { get;  set; }
        public int ApprovalStatus { get; set; }
        public bool IsDeletd { get;  set; }
        public bool IsActive { get;  set; }
        public bool IsBlacklisted { get;  set; }
    }
}
