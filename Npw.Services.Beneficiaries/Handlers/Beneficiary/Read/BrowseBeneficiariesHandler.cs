﻿using N.Shared.Handlers;
using N.Shared.Types;
using Npw.Services.Beneficiaries.Models;
using Npw.Services.Beneficiaries.Query.Beneficiary;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.Beneficiary.Read
{
    public class BrowseBeneficiariesHandler : IQueryHandler<BrowseBeneficiaries, PagedResult<BeneficiaryDto>>
    {
        private readonly IBeneficiaryRepository _beneRepository;
        public BrowseBeneficiariesHandler(IBeneficiaryRepository beneRepository)
        {
            _beneRepository = beneRepository;
        }
        public async Task<PagedResult<BeneficiaryDto>> HandleAsync(BrowseBeneficiaries query)
        {
            var pagedResult = await _beneRepository.BrowserBeneficiaries(query);
            var benes = pagedResult.Items.Select(bene => new BeneficiaryDto()
            {
                Id = bene.Id,
                RCNumberOrIdNumber = bene.RCNumberOrIdNumber,
                BVN = bene.BVN,
                Name = bene.Name,
                Email = bene.Email,
                Phone = bene.Phone,
                Address = bene.Address,
                CreatedAt = bene.CreatedAt,
                CreatedBy = bene.CreatedBy,
                UpdatedAt = bene.UpdatedAt,
                UpdatedBy = bene.UpdatedBy,
                ApprovedAt = bene.ApprovedAt,
                ApprovedBy = bene.ApprovedBy,
                IsActive = bene.IsActive,
                IsBlacklisted = bene.IsBlacklisted,
                IsDeletd = bene.IsDeletd
            });
            return PagedResult<BeneficiaryDto>.From(pagedResult, benes);

        }
    }
}
