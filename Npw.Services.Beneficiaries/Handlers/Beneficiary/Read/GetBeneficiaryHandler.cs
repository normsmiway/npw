﻿using N.Shared.Handlers;
using Npw.Services.Beneficiaries.Models;
using Npw.Services.Beneficiaries.Query.Beneficiary;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.Beneficiary.Read
{
    public class GetBeneficiaryHandler : IQueryHandler<GetBeneficiary, BeneficiaryDto>
    {
        private readonly IBeneficiaryRepository beneficiary;
        public GetBeneficiaryHandler(IBeneficiaryRepository _beneficiary)
        {
            beneficiary = _beneficiary;
        }
        public async Task<BeneficiaryDto> HandleAsync(GetBeneficiary query)
        {
            var bene = await beneficiary.GetAsync(query.Id);
            return new BeneficiaryDto()
            {
                Id = bene.Id,
                RCNumberOrIdNumber = bene.RCNumberOrIdNumber,
                BVN = bene.BVN,
                Name = bene.Name,
                Email = bene.Email,
                Phone = bene.Phone,
                Address = bene.Address,
                CreatedAt = bene.CreatedAt,
                CreatedBy = bene.CreatedBy,
                UpdatedAt = bene.UpdatedAt,
                UpdatedBy = bene.UpdatedBy,
                ApprovedAt = bene.ApprovedAt,
                ApprovedBy = bene.ApprovedBy,
                ApprovalStatus = bene.ApprovalStatus,
                IsActive = bene.IsActive,
                IsBlacklisted = bene.IsBlacklisted,
                IsDeletd = bene.IsDeletd
            } ?? null;
        }
    }
}
