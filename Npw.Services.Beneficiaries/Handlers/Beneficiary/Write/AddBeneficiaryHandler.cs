﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Beneficiaries.Messages.Commands.Beneficiary;
using Npw.Services.Beneficiaries.Domain;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npw.Services.Beneficiaries.Messages.Events.Beneficiary;

namespace Npw.Services.Beneficiaries.Handlers.Beneficiary.Write
{
    public class AddBeneficiaryHandler : ICommandHandler<AddBeneficiary>
    {
        private readonly IBeneficiaryRepository beneficiary;
        private readonly IBusPublisher busPublisher;

        public AddBeneficiaryHandler(IBeneficiaryRepository _beneficiary, IBusPublisher _busPublisher)
        {
            beneficiary = _beneficiary;
            busPublisher = _busPublisher;
        }
        public async Task HandleAsync(AddBeneficiary command, ICorrelationContext context)
        {
            var exist = beneficiary.BeneficiaryExistsAsync(command.BVN, command.RCNumberOrIdNumber);

            if (!exist.Result)
            {
                var bene = new Domain.Beneficiary(command.Id, command.RCNumberOrIdNumber, command.BVN, command.Name, command.Email, command.Phone, command.Address, command.CreatedBy);
                await beneficiary.AddAsync(bene);
                //await busPublisher.PublishAsync(new BeneficiaryAdded(bene.Id, bene.RCNumberOrIdNumber, bene.BVN, bene.Name, bene.Email, bene.Phone, bene.Address, bene.CreatedBy), context);
            }
            

        }
    }
  
}
