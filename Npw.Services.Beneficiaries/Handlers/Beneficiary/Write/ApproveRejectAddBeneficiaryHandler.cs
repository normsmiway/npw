﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Beneficiaries.Messages.Commands.Beneficiary;
using Npw.Services.Beneficiaries.Messages.Events.Beneficiary;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.Beneficiary.Write
{
    public class ApproveRejectAddBeneficiaryHandler : ICommandHandler<ApproveRejectAddBeneficiary>
    {
        private readonly IBusPublisher busPublisher;
        private readonly IBeneficiaryRepository beneficiaryrepo;
        public ApproveRejectAddBeneficiaryHandler(IBeneficiaryRepository _beneficiaryrepo, IBusPublisher _busPublisher)
        {
            busPublisher = _busPublisher;
            beneficiaryrepo = _beneficiaryrepo;
        }
        public async Task HandleAsync(ApproveRejectAddBeneficiary command, ICorrelationContext context)
        {

            var bene = await beneficiaryrepo.GetAsync(command.Id);
            if (bene != null)
            {
                bene.SetApprovedBy(command.ApprovedBy, command.ApprovalStatus);
                await beneficiaryrepo.UpdateAsync(bene);
                await busPublisher.PublishAsync(new BeneficiaryApproved(bene.Id, bene.ApprovedBy, bene.ApprovalStatus), context);

            }
        }
    }
}
