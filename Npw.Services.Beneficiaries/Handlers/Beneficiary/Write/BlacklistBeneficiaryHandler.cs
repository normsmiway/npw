﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Beneficiaries.Messages.Commands.Beneficiary;
using Npw.Services.Beneficiaries.Messages.Events.Beneficiary;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.Beneficiary.Write
{
    public class BlacklistBeneficiaryHandler : ICommandHandler<BlacklistBeneficiary>
    {
        private readonly IBusPublisher busPublisher;
        private readonly IBeneficiaryRepository beneficiaryRepo;
        public BlacklistBeneficiaryHandler(IBeneficiaryRepository _beneficiaryRepo, IBusPublisher _busPublisher)
        {
            beneficiaryRepo = _beneficiaryRepo;
            busPublisher = _busPublisher;
        }
        public async Task HandleAsync(BlacklistBeneficiary command, ICorrelationContext context)
        {
            var blackList = await beneficiaryRepo.GetAsync(command.Id);
            blackList.SetBlacklistStatus(command.ModifiedBy, command.BlacklistStatus);
            await beneficiaryRepo.UpdateAsync(blackList);
            await busPublisher.PublishAsync(new BeneficiaryBlacklisted(blackList.Id, command.ModifiedBy, command.BlacklistStatus), context);

        }
    }
}
