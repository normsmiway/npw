﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Beneficiaries.Messages.Commands.Beneficiary;
using Npw.Services.Beneficiaries.Messages.Events.Beneficiary;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.Beneficiary.Write
{
    public class UpdateBeneficiaryHandler : ICommandHandler<UpdateBeneficiary>
    {
        private readonly IBusPublisher busPublisher;
        private readonly IBeneficiaryRepository beneficiaryRepo;
        public UpdateBeneficiaryHandler(IBeneficiaryRepository _beneficiaryRepo, IBusPublisher _busPublisher)
        {
            beneficiaryRepo = _beneficiaryRepo;
            busPublisher = _busPublisher;
        }
        public async Task HandleAsync(UpdateBeneficiary command, ICorrelationContext context)
        {
            var bene = await beneficiaryRepo.GetAsync(command.Id);
            if (bene != null)
            {
                bene.UpdateBeneficiary(command.Id, command.RCNumberOrIdNumber, command.BVN, command.Name, command.Email, command.Phone, command.Address, command.UpdatedBy, command.ApprovedBy, command.IsBlacklisted, command.ApprovalStatus, command.IsActive);
                await beneficiaryRepo.UpdateAsync(bene);
                await busPublisher.PublishAsync(new BeneficiaryUpdated(command.Id, command.RCNumberOrIdNumber, command.BVN, command.Name, command.Email, command.Phone, command.Address, command.UpdatedBy, command.ApprovedBy, command.IsBlacklisted, command.ApprovalStatus, command.IsActive), context);
            }

        }
    }
}