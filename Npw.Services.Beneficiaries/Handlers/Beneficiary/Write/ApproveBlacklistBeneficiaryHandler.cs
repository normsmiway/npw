﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Beneficiaries.Messages.Commands.Beneficiary;
using Npw.Services.Beneficiaries.Messages.Events.Beneficiary;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.Beneficiary.Write
{
    public class ApproveBlacklistBeneficiaryHandler : ICommandHandler<ApproveBlacklistBeneficiary>
    {
        private readonly IBusPublisher busPublisher;
        private readonly IBeneficiaryRepository beneficiaryRepo;
        public ApproveBlacklistBeneficiaryHandler(IBeneficiaryRepository _beneficiaryRepo, IBusPublisher _busPublisher)
        {
            beneficiaryRepo = _beneficiaryRepo;
            busPublisher = _busPublisher;
        }
        public async Task HandleAsync(ApproveBlacklistBeneficiary command, ICorrelationContext context)
        {
            var blackList = await beneficiaryRepo.GetAsync(command.Id);
            blackList.ApproveBlacklistStatus(command.ApprovedBy, command.ApprovalStatus);
            await beneficiaryRepo.UpdateAsync(blackList);
            await busPublisher.PublishAsync(new BlacklistBeneficiaryApproved(blackList.Id, command.ApprovedBy,command.ApprovalStatus), context);

        }
    }
}
