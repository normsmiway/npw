﻿using N.Shared.Handlers;
using N.Shared.Types;
using Npw.Services.Beneficiaries.Models;
using Npw.Services.Beneficiaries.Query.Beneficiary;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.BeneficiaryAccount.Read
{
    public class BrowseBeneficiaryAccountsHandler : IQueryHandler<BrowseBeneficiaryAccounts, PagedResult<BeneficiaryAccountDto>>
    {
        private readonly IBeneficiaryAccountRepository _beneRepository;
        public BrowseBeneficiaryAccountsHandler(IBeneficiaryAccountRepository beneRepository)
        {
            _beneRepository = beneRepository;
        }
        public async Task<PagedResult<BeneficiaryAccountDto>> HandleAsync(BrowseBeneficiaryAccounts query)
        {
            var pagedResult = await _beneRepository.BrowserBeneficiaryAccounts(query);
            var benes = pagedResult.Items.Select(bene => new BeneficiaryAccountDto()
            {
                Id = bene.Id,
                AccountNumber = bene.AccountNumber,
                AccountName = bene.AccountName,
                BankCode = bene.BankCode,
                BankName = bene.BankName,
                BeneficiaryUserId = bene.BeneficiaryUserId,
                CreatedAt = bene.CreatedAt,
                CreatedBy = bene.CreatedBy,
                UpdatedAt = bene.UpdatedAt,
                UpdatedBy = bene.UpdatedBy,
                ApprovedAt = bene.ApprovedAt,
                ApprovedBy = bene.ApprovedBy,
                IsActive = bene.IsActive,
            });
            return PagedResult<BeneficiaryAccountDto>.From(pagedResult, benes);

        }
    }
}
