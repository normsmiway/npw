﻿using N.Shared.Handlers;
using Npw.Services.Beneficiaries.Models;
using Npw.Services.Beneficiaries.Query.BeneficiaryAccount;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.BeneficiaryAccount.Read
{
    public class GetBeneficiaryAccountHandler : IQueryHandler<GetBeneficiaryAccount, BeneficiaryAccountDto>
    {
        private readonly IBeneficiaryAccountRepository beneAcctRepo;

        public GetBeneficiaryAccountHandler(IBeneficiaryAccountRepository _beneAcctRepo)
        {
            beneAcctRepo = _beneAcctRepo;
        }
        public async Task<BeneficiaryAccountDto> HandleAsync(GetBeneficiaryAccount query)
        {
            var beneAcct = await beneAcctRepo.GetAsync(query.Id);
            return new BeneficiaryAccountDto()
            {
                Id = beneAcct.Id,
                AccountNumber = beneAcct.AccountNumber,
                AccountName = beneAcct.AccountName,
                BeneficiaryUserId = beneAcct.BeneficiaryUserId,
                BankName = beneAcct.BankName,
                BankCode = beneAcct.BankCode,
                CreatedAt = beneAcct.CreatedAt,
                CreatedBy = beneAcct.CreatedBy,
                ApprovedBy = beneAcct.ApprovedBy,
                ApprovedAt = beneAcct.ApprovedAt,
                UpdatedAt = beneAcct.UpdatedAt,
                UpdatedBy = beneAcct.UpdatedBy,
                IsActive = beneAcct.IsActive,
                ApprovalStatus = beneAcct.ApprovalStatus
            } ?? null;
        }
    }
}
