﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Beneficiaries.Messages.Commands.BeneficiaryAccount;
using Npw.Services.Beneficiaries.Messages.Events.BeneficiaryAccount;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.BeneficiaryAccount.Write
{
    public class UpdateBeneficiaryHandler : ICommandHandler<UpdateBeneficiaryAccount>
    {
        private readonly IBeneficiaryAccountRepository beneAcctRepo;
        private readonly IBusPublisher busPublisher;
        public UpdateBeneficiaryHandler(IBeneficiaryAccountRepository _beneAcctRepo, IBusPublisher _busPublisher)
        {
            beneAcctRepo = _beneAcctRepo;
            busPublisher = _busPublisher;
        }
        public async Task HandleAsync(UpdateBeneficiaryAccount command, ICorrelationContext context)
        {
            var beneAcct = await beneAcctRepo.GetAsync(command.Id);
            beneAcct.ModifyBeneficiaryAcct(command.Id, command.UpdatedBy, command.IsActive,command.ApprovalStatus,command.AccountNumber, command.AccountName,command.BeneficiaryUserId,command.BankName, command.BankCode);
            await beneAcctRepo.UpdateAsync(beneAcct);
            await busPublisher.PublishAsync(new BeneficiaryAccountUpdated(beneAcct.Id, command.UpdatedBy, command.IsActive, command.ApprovedBy, command.ApprovalStatus), context);
        }
    }
}
