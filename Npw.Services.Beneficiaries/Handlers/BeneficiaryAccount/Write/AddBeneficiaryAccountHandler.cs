﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Beneficiaries.Messages.Commands.BeneficiaryAccount;
using Npw.Services.Beneficiaries.Domain;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npw.Services.Beneficiaries.Messages.Events.BeneficiaryAccount;

namespace Npw.Services.Beneficiaries.Handlers.BeneficiaryAccount.Write
{
    public class AddBeneficiaryAccountHandler : ICommandHandler<AddBeneficiaryAccount>
    {
        private readonly IBeneficiaryAccountRepository beneAcctRepo;
        private readonly IBusPublisher busPublisher;

        public AddBeneficiaryAccountHandler(IBeneficiaryAccountRepository _beneAcctRepo, IBusPublisher _busPublisher)
        {
            beneAcctRepo = _beneAcctRepo;
            busPublisher = _busPublisher;
        }
        public async Task HandleAsync(AddBeneficiaryAccount command, ICorrelationContext context)
        {
            var exist = beneAcctRepo.BeneficiaryAccountExistsAsync(command.BeneficiaryUserId, command.AccountNumber);
            if (!exist.Result)
            {
                var beneAcc = new BeneficiaryBankAccount(command.Id, command.AccountNumber, command.AccountName, command.BeneficiaryUserId, command.BankName, command.BankCode, command.CreatedBy);
                await beneAcctRepo.AddAsync(beneAcc);
                await busPublisher.PublishAsync(new BeneficiaryAccountAdded(beneAcc.Id, beneAcc.AccountNumber, beneAcc.AccountName, beneAcc.BeneficiaryUserId, beneAcc.BankName, beneAcc.BankCode, beneAcc.CreatedBy), context);

            }
        }
    }
}
