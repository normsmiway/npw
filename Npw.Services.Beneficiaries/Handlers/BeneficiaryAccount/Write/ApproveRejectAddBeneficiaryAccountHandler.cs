﻿using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Npw.Services.Beneficiaries.Messages.Commands.BeneficiaryAccount;
using Npw.Services.Beneficiaries.Messages.Events.BeneficiaryAccount;
using Npw.Services.Beneficiaries.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Handlers.BeneficiaryAccount.Write
{
    public class ApproveRejectAddBeneficiaryAccountHandler : ICommandHandler<ApproveRejectAddBeneficiaryAccount>
    {
        private readonly IBusPublisher busPublisher;
        private readonly IBeneficiaryAccountRepository beneAcctRepo;
        public ApproveRejectAddBeneficiaryAccountHandler(IBeneficiaryAccountRepository _beneAcctRepo, IBusPublisher _busPublisher)
        {
            busPublisher = _busPublisher;
            beneAcctRepo = _beneAcctRepo;
        }
        public async Task HandleAsync(ApproveRejectAddBeneficiaryAccount command, ICorrelationContext context)
        {

            var bene = await beneAcctRepo.GetAsync(command.Id);
            if (bene != null)
            {
                bene.SetApprovedBy(command.ApprovedBy, command.ApprovalStatus);
                await beneAcctRepo.UpdateAsync(bene);
                await busPublisher.PublishAsync(new BeneficiaryAccountApprovedRejected(bene.Id, bene.ApprovedBy, bene.ApprovalStatus), context);

            }
        }
    }
}
