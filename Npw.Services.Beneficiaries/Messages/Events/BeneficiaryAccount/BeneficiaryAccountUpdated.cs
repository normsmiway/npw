﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Events.BeneficiaryAccount
{
    public class BeneficiaryAccountUpdated : IEvent
    {
        public Guid Id { get; }
        public Guid UpdatedBy { get; }
        public DateTime UpdatedAt { get; }
        public DateTime ApprovedAt { get; }
        public bool IsActive { get; }
        public Guid ApprovedBy { get; }
        public int ApprovalStatus { get; }


        [JsonConstructor]
        public BeneficiaryAccountUpdated(Guid id, Guid modifiedBy, bool isActive, Guid approvedBy, int approvalStat)
        {
            Id = id;
            UpdatedBy = modifiedBy;
            UpdatedAt = DateTime.Now;
            IsActive = isActive;
            ApprovedBy = approvedBy;
            ApprovedAt = DateTime.Now;
            ApprovalStatus = approvalStat;
        }
    }
}
