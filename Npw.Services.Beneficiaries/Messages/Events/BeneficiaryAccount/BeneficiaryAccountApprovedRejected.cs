﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Events.BeneficiaryAccount
{
    public class BeneficiaryAccountApprovedRejected:IEvent
    {
        public Guid Id { get; }
        public Guid ApprovedBy { get; }
        public int ApprovalStatus { get; }

        [JsonConstructor]
        public BeneficiaryAccountApprovedRejected(Guid id, Guid approvedBy, int approvalStatus)
        {
            Id = id;
            ApprovedBy = approvedBy;
            ApprovalStatus = approvalStatus;
        }
    }
}
