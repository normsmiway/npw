﻿
using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Events.BeneficiaryAccount
{
    public class BeneficiaryAccountAdded:IEvent
    {
        public Guid Id { get; }
        public string AccountNumber { get; }
        public string AccountName { get; }
        public Guid BeneficiaryUserId { get; }
        public string BankName { get; }
        public string BankCode { get; }
        public DateTime CreatedAt { get; }
        public Guid CreatedBy { get; }


        [JsonConstructor]
        public BeneficiaryAccountAdded(Guid id, string accountNo, string accountName, Guid beneUserId, string bankName, string bankCode, Guid createdBy)
        {
            Id = id;
            AccountNumber = accountNo;
            AccountName = accountName;
            BeneficiaryUserId = beneUserId;
            BankName = bankName;
            BankCode = bankCode;
            CreatedBy = createdBy;
            CreatedAt = DateTime.Now;
        }
    }
}
