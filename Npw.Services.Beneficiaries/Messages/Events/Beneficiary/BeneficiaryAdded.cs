﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Events.Beneficiary
{
    public class BeneficiaryAdded:IEvent
    {
        public Guid Id { get; }
        public string RCNumberOrIdNumber { get; }
        public string BVN { get; }
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public string Address { get; }
        public Guid AddedBy { get; }
        public DateTime CreatedAt { get; private set; }
        [JsonConstructor]
        public BeneficiaryAdded(Guid id, string rcNooOrId, string bvn, string name, string email, string phone, string address, Guid addedBy)
        {
            Id = id;
            RCNumberOrIdNumber = rcNooOrId;
            BVN = bvn;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            AddedBy = addedBy;
            CreatedAt = DateTime.Now;
        }
    }
}
