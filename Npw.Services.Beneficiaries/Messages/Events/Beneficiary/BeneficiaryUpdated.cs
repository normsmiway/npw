﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Events.Beneficiary
{
    public class BeneficiaryUpdated : IEvent
    {
        public Guid Id { get; }
        public string RCNumberOrIdNumber { get; }
        public string BVN { get; }
        public string Name { get; }
        public string Email { get; }
        public string Phone { get; }
        public string Address { get; }
        public DateTime UpdatedAt { get; }
        public Guid UpdatedBy { get; }
        public Guid ApprovedBy { get; }
        public DateTime ApprovedAt { get; }

        public bool IsActive { get; }
        public bool IsBlacklisted { get; }
        public int ApprovalStatus { get; }
        [JsonConstructor]
        public BeneficiaryUpdated(Guid id, string rcNooOrId, string bvn, string name, string email, string phone, string address, Guid updatedBy, Guid approvedBy, bool isBlacklisted, int approvalStat, bool isActive)
        {
            Id = id;
            RCNumberOrIdNumber = rcNooOrId;
            BVN = bvn;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            UpdatedBy = updatedBy;
            UpdatedAt = DateTime.Now;
            ApprovedBy = approvedBy;
            IsBlacklisted = isBlacklisted;
            ApprovalStatus = approvalStat;
            IsActive = isActive;
        }
    }
}
