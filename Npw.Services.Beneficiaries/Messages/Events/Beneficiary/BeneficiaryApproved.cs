﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Events.Beneficiary
{
    public class BeneficiaryApproved : IEvent
    {
        public Guid UserId { get; }
        public Guid ApprovedBy { get; }
        public int ApprovalStatus { get; }
        public DateTime ApprovedAt { get; }


        [JsonConstructor]
        public BeneficiaryApproved(Guid userId, Guid approvedBy, int approvalStatus)
        {
            UserId = userId;
            ApprovedBy = approvedBy;
            ApprovalStatus = approvalStatus;
            ApprovedAt = DateTime.Now;
        }
    }
}
