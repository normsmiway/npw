﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Commands.BeneficiaryAccount
{
    public class UpdateBeneficiaryAccount : ICommand
    {
        public Guid Id { get; }
        public string AccountNumber { get; }
        public string AccountName { get; }
        public Guid BeneficiaryUserId { get; }
        public string BankName { get; }
        public string BankCode { get; }
        public Guid UpdatedBy { get; }
        public DateTime UpdatedAt { get; }
        public DateTime ApprovedAt { get; }
        public bool IsActive { get; }
        public Guid ApprovedBy { get; }
        public int ApprovalStatus { get; }
        [JsonConstructor]
        public UpdateBeneficiaryAccount(Guid id, Guid updatedBy, bool isActive, int approvalStatus, string accountNumber, string accountName, Guid beneficiaryUserId, string bankName, string bankCode)
        {
            Id = id;
            AccountNumber = accountNumber;
            AccountName = accountName;
            BeneficiaryUserId = beneficiaryUserId;
            BankName = bankName;
            BankCode = bankCode;
            UpdatedBy = updatedBy;
            UpdatedAt = DateTime.Now;
            IsActive = isActive;
            ApprovalStatus = approvalStatus;
        }
    }
}
