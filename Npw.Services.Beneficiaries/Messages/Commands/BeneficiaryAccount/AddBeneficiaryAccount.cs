﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Commands.BeneficiaryAccount
{
    public class AddBeneficiaryAccount : ICommand
    {
        public Guid Id { get;  }
        public string AccountNumber { get;  }
        public string AccountName { get;  }
        public Guid BeneficiaryUserId { get;  }
        public string BankName { get;  }
        public string BankCode { get;  }
        public DateTime CreatedAt { get;  }
        public Guid CreatedBy { get;  }


        [JsonConstructor]
        public AddBeneficiaryAccount(Guid id, string accountNumber, string accountName, Guid beneficiaryUserId, string bankName, string bankCode, Guid createdBy)
        {
            Id = id;
            AccountNumber = accountNumber;
            AccountName = accountName;
            BeneficiaryUserId = beneficiaryUserId;
            BankName = bankName;
            BankCode = bankCode;
            CreatedBy = createdBy;
            CreatedAt = DateTime.Now;
        }
    }
}
