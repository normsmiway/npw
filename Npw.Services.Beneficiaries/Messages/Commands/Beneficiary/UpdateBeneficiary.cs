﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Commands.Beneficiary
{
    public class UpdateBeneficiary : ICommand
    {
        public Guid Id { get; private set; }
        public string RCNumberOrIdNumber { get; private set; }
        public string BVN { get; private set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
        public string Address { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public Guid UpdatedBy { get; private set; }
        public Guid ApprovedBy { get; private set; }
        public DateTime ApprovedAt { get; private set; }

        public bool IsActive { get; private set; }
        public bool IsBlacklisted { get; private set; }
        public int ApprovalStatus { get; private set; }
        [JsonConstructor]
        public UpdateBeneficiary(Guid id, string rcNumberOrIdNumber, string bvn, string name, string email, string phone, string address, Guid updatedBy, Guid approvedBy, bool isBlacklisted, int approvalStat, bool isActive)
        {
            Id = id;
            RCNumberOrIdNumber = rcNumberOrIdNumber;
            BVN = bvn;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            UpdatedBy = updatedBy;
            UpdatedAt = DateTime.Now;
            ApprovedBy = approvedBy;
            IsBlacklisted = isBlacklisted;
            ApprovalStatus = approvalStat;
            IsActive = isActive;
        }
    }
}
