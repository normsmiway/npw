﻿using N.Shared.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Commands.Beneficiary
{
    public class ApproveRejectAddBeneficiary : ICommand
    {
        public Guid Id { get; private set; }
        public Guid ApprovedBy { get; private set; }
        public int ApprovalStatus { get; private set; }

        public ApproveRejectAddBeneficiary(Guid id, Guid approvedBy, int approvalStatus)
        {
            Id = id;
            ApprovedBy = approvedBy;
            ApprovalStatus = approvalStatus;
        }
    }
}
