﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Messages.Commands.Beneficiary
{
    public class AddBeneficiary:ICommand
    {
        public Guid Id { get; private set; }
        public string RCNumberOrIdNumber { get; private set; }
        public string BVN { get; private set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
        public string Address { get; private set; }
        public Guid CreatedBy { get; private set; }
        public DateTime CreatedAt { get; private set;}
        [JsonConstructor]
        public AddBeneficiary(Guid id, string rCNumberOrIdNumber, string bvn, string name, string email, string phone, string address, Guid createdBy)
        {
            Id = id;
            RCNumberOrIdNumber = rCNumberOrIdNumber;
            BVN = bvn;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            CreatedBy = createdBy;
            CreatedAt = DateTime.Now;
        }
    }
}
