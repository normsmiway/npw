﻿using N.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Domain
{
    public class Beneficiary : IIdentifiable
    {
        private static readonly Regex EmailRegex = new Regex(
            @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
            RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant);


        public Guid Id { get; private set; }
        public string RCNumberOrIdNumber { get; private set; }
        public string BVN { get; private set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Phone { get; private set; }
        public string Address { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public Guid CreatedBy { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public Guid UpdatedBy { get; private set; }
        public Guid ApprovedBy { get; private set; }
        public DateTime ApprovedAt { get; private set; }

        public bool IsDeletd { get; private set; }
        public bool IsActive { get; private set; }
        public bool IsBlacklisted { get; private set; }
        public int ApprovalStatus { get; private set; }


        public Beneficiary(Guid id, string RcOrIdentityNo, string bvn, string name, string email, string phone, string address, Guid createdBy)
        {
            if (!EmailRegex.IsMatch(email))
            {
                throw new NException(ErrorCodes.InvalidEmail,
                    $"Invalid email: '{email}'.");
            }
            if (string.IsNullOrEmpty(bvn))
            {
                throw new NException(ErrorCodes.BvnEmpty, $"BVN cannot be empty.");
            }

            Id = id;
            RCNumberOrIdNumber = RcOrIdentityNo;
            BVN = bvn;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            CreatedAt = DateTime.Now;
            CreatedBy = createdBy;
            IsActive = false;
            ApprovalStatus = 0;

        }


        public void SetApprovedBy(Guid approvedBy, int approvalStat)
        {
            bool isActive = (approvalStat == 0) ? false : true;
            ApprovedBy = approvedBy;
            ApprovedAt = DateTime.Now;
            IsActive = isActive;
            ApprovalStatus = approvalStat;
        }

        public void SetBlacklistStatus(Guid updatedBy, bool blacklistStatus)
        {
            UpdatedBy = updatedBy;
            UpdatedAt = DateTime.Now;
            IsBlacklisted = blacklistStatus;
            ApprovalStatus = 0;

        }

        public void ApproveBlacklistStatus(Guid approvedBy, int approvalStat)
        {
            ApprovedBy = approvedBy;
            ApprovedAt = DateTime.Now;
            ApprovalStatus = approvalStat;
        }

        public void UpdateBeneficiary(Guid id, string rcNooOrId, string bvn, string name, string email, string phone, string address, Guid updatedBy, Guid approvedBy, bool isBlacklisted, int approvalStat, bool isActive)
        {
            Id = id;
            RCNumberOrIdNumber = rcNooOrId;
            BVN = bvn;
            Name = name;
            Email = email;
            Phone = phone;
            Address = address;
            UpdatedAt = DateTime.Now;
            UpdatedBy = updatedBy;
            IsActive = isActive;
            ApprovalStatus = 0;
            IsBlacklisted = isBlacklisted;
        }
    }
}
