﻿using N.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Beneficiaries.Domain
{
    public class BeneficiaryBankAccount:IIdentifiable
    {
        public Guid Id { get; private set; }
        public string AccountNumber { get; private set; }
        public string AccountName { get; private set; }
        public Guid BeneficiaryUserId { get; private set; }
        public string BankName { get; private set; }
        public string BankCode { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public Guid CreatedBy { get; private set; }
        public Guid ApprovedBy { get; private set; }
        public DateTime ApprovedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public Guid UpdatedBy { get; private set; }
        public bool IsActive { get; private set; }
        public int ApprovalStatus { get; private set; }

        
        public BeneficiaryBankAccount(Guid id, string accountNumber, string accountName, Guid beneficiaryUserId, string bankName, string bankCode, Guid createdBy)
        {
            Id = id;
            AccountNumber = accountNumber;
            AccountName = accountName;
            BeneficiaryUserId = beneficiaryUserId;
            BankName = bankName;
            BankCode = bankCode;
            CreatedBy = createdBy;
            CreatedAt = DateTime.Now;
            IsActive = false;
            ApprovalStatus = 0;
        }

        public void ModifyBeneficiaryAcct(Guid id, Guid modifiedBy, bool isActive, int approvalStat, string accountNumber, string accountName, Guid beneficiaryId, string bankName, string bankCode)
        {
            Id = id;
            AccountNumber = accountNumber;
            AccountName = accountName;
            BeneficiaryUserId = beneficiaryId;
            BankName = bankName;
            BankCode = bankCode;
            UpdatedBy = modifiedBy;
            UpdatedAt = DateTime.Now;
            IsActive = isActive;
            ApprovalStatus = approvalStat;
        }

        public void SetApprovedBy(Guid approvedBy, int approvalStat)
        {
            bool isActive = (approvalStat == 0) ? false : true;
            ApprovedBy = approvedBy;
            ApprovedAt = DateTime.Now;
            IsActive = isActive;
            ApprovalStatus = approvalStat;
        }
    }
}
