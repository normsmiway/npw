﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Services
{
    public class ClaimsProvider : IClaimsProvider
    {
        private readonly IUserService _userService;

        public ClaimsProvider(IUserService userService)
        {
            _userService = userService;
        }
        public async Task<IDictionary<string, string>> GetAsync(Guid Id)
        {
            var user = await _userService.GetAsync(Id);
            if (user != null)
            {


                // Provide your own claims collection if needed.
                return await Task.FromResult(new Dictionary<string, string>
                {
                    ["Id"] = user.Id.ToString() ?? string.Empty,
                    ["FistName"] = user.FirstName ?? string.Empty,
                    ["LastName"] = user.LastName ?? string.Empty,
                    ["UserName"] = user.UserName ?? string.Empty,
                    ["ShortDescription"] = user.ShortDescription ?? string.Empty,
                    ["Address"] = user.Address ?? string.Empty,
                    ["Country"] = user.Country ?? string.Empty,
                    ["Completed"] = user.Completed.ToString() ?? string.Empty,
                    ["Gender"] = user.Gender ?? string.Empty,
                    ["IsDeleted"] = user.IsDeletd.ToString() ?? string.Empty,
                    ["IsActive"] = user.IsActive.ToString() ?? string.Empty,
                    ["CreatedAt"] = user.CreatedAt.ToString() ?? string.Empty,
                    ["CreatedBy"] = user.CreatedBy.ToString() ?? string.Empty,
                    ["UpdatedAt"] = user.UpdatedAt.ToString() ?? string.Empty,
                    ["UpdatedBy"] = user.UpdatedBy.ToString() ?? string.Empty,
                    ["IsDeletd"] = user.IsDeletd.ToString() ?? string.Empty
                });
            }
            return null;
        }

        //public async Task<IDictionary<string, string>> DoSomething(Guid Id)
        //{
        //    var user = await _userService.GetAsync(Id);
        //    var result = new Dictionary<string, string>();
        //    foreach (var prop in typeof(UserDto).GetProperties())
        //    {
        //        result.Add(prop.Name, user.GetType().GetField(prop.Name).GetValue(user).ToString() ?? string.Empty);
        //    }

        //    return await Task.FromResult(result);
        //}
    }
}
