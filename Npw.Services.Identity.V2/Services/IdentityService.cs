﻿using Microsoft.AspNetCore.Identity;
using N.Shared.Authentication;
using N.Shared.RabbitMq;
using N.Shared.Types;
using Npw.Services.Identity.Domain;
using Npw.Services.Identity.Messages.Events.PasswordChanged;
using Npw.Services.Identity.Messages.Events.SignedUp;
using Npw.Services.Identity.Policies;
using Npw.Services.Identity.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IJwtHandler _jwtHandler;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUSerSqaRepository _userSqaRepository;
        private readonly IClaimsProvider _claimsProvider;
        private readonly IBusPublisher _busPublisher;
        private readonly IPasswordHandler _passwordHadler;

        public IdentityService(IUserRepository userRepository,
            IPasswordHasher<User> passwordHasher,
            IJwtHandler jwtHandler,
            IRefreshTokenRepository refreshTokenRepository,
            IUSerSqaRepository uSerSqaRepository,
            IClaimsProvider claimsProvider,
            IBusPublisher busPublisher,
            IPasswordHandler passwordHandler)
        {
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
            _jwtHandler = jwtHandler;
            _claimsProvider = claimsProvider;
            _refreshTokenRepository = refreshTokenRepository;
            _userSqaRepository = uSerSqaRepository;
            _busPublisher = busPublisher;
            _passwordHadler = passwordHandler;

        }
        public async Task ChangePasswordAsync(Guid userId, string currentPassword, string newPassword)
        {
            var user = await _userRepository.GetAsync(userId);
            if (user is null)
            {
                throw new NException(ErrorCodes.UserNotFound,
                    $"User with id: '{userId}' was not found.");
            }

            if (!user.ValidatePassword(currentPassword, _passwordHasher))
            {
                throw new NException(ErrorCodes.InvalidCurrentPassword,
                    "Invalid current password.");
            }

            if (!_passwordHadler.IsValid(newPassword))
            {
                throw new NException(ErrorCodes.InvalidPassword,
                   $"Password does not match the required password rule");
            }

            user.SetPassword(newPassword, _passwordHasher);
            await _userRepository.UpdateAsync(user);

            await _busPublisher.PublishAsync(new PasswordChanged(userId), CorrelationContext.Empty);
        }

        public Task<IEnumerable<UserSQA>> GetUserSQA(Guid userId)
        {
            throw new NotImplementedException();
        }

        public async Task<JsonWebToken> SignInAsync(string userNameOrEmail, string password)
        {
            var user = await _userRepository.GetAsync(userNameOrEmail);

            if (user==null)
            {
                throw new NException(ErrorCodes.InvalidCredentials,
                  "Invalid User");
            }

            if (!user.ValidatePassword(password, _passwordHasher))
            {
                throw new NException(ErrorCodes.InvalidCredentials,
                    "Invalid Password.");
            }

            var refreshToken = new RefreshToken(user, _passwordHasher);

            var claims = await _claimsProvider.GetAsync(user.Id);
            var jwt = _jwtHandler.CreateToken(user.Id.ToString("N"), user.Role, claims);

            jwt.RefreshToken = refreshToken.Token;

            await _refreshTokenRepository.AddAsync(refreshToken);

            return jwt;

        }

        public async Task<string> SignUpAsync(Guid id, string email, string userName, string password, IDictionary<string, string> securityQnA = null, string role = "user")
        {
            if (await _userRepository.EmailExistsAsync(email))
            {
                throw new NException(ErrorCodes.EmailInUse,
                   $"Email: an account with {email} already registred with this system");
            }
            if (await _userRepository.UserNameExistsAsync(userName))
            {
                throw new NException(ErrorCodes.UserNameInUse,
                   $"User Name: an account with {userName} already registred with this system");
            }

            var user = new User(id, email, userName, role);

            if (string.IsNullOrWhiteSpace(role))
            {
                role = Role.User;
            }

            if (!_passwordHadler.IsValid(password))
            {
                throw new NException(ErrorCodes.InvalidPassword,
                   $"Password does not match the required password rule");
            }
            user.SetPassword(password, _passwordHasher);

            user.SetUserSecurityTag(securityQnA, userName ?? email.Substring(0, email.IndexOf('@') - 1));


            #region Wrap region in Transaction Scope

            foreach (var item in securityQnA)
            {
                await _userSqaRepository.AddAsync(new UserSQA(user.Id, item.Key, item.Value));
            }

            await _userRepository.AddAsync(user);
            #endregion

            await _busPublisher.PublishAsync(new SignedUp(id, email, userName, role), CorrelationContext.Empty);

            return userName;


        }

     
    }
}
