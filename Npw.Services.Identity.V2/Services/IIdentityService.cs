﻿using N.Shared.Authentication;
using Npw.Services.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Services
{
    public interface IIdentityService
    {
        Task<string> SignUpAsync(Guid id, string email, string userName, string password, IDictionary<string, string> securityTags = null, string role = Role.User);
        Task<JsonWebToken> SignInAsync(string userNameOrEmail, string password);
        Task ChangePasswordAsync(Guid userId, string currentPassword, string newPassword);
        Task<IEnumerable<UserSQA>> GetUserSQA(Guid userId);
    }

}
