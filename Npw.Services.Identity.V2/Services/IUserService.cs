﻿using RestEase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Services
{
    public interface IUserService
    {
        [AllowAnyStatusCode]
        [Get("users/{id}")]
        Task<UserDto> GetAsync([Path] Guid id);

    }
}
