﻿using Microsoft.AspNetCore.Identity;
using N.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Domain
{
    public class User : IIdentifiable
    {
        private static readonly Regex EmailRegex = new Regex(
            @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
            RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant);

        private static readonly Regex PinRegex = new Regex(@"^(\d)(?!\1+$)\d{5}$", RegexOptions.IgnoreCase | RegexOptions.Compiled | RegexOptions.CultureInvariant);


        public Guid Id { get; private set; }
        public string Email { get; private set; }
        public string UserName { get; private set; }
        public string SecurityTag { get; private set; } //user question and ansswer Tag: Concat Questions and User Pin
        public string Role { get; private set; }
        public string PasswordHash { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public Guid CreatedBy { get; private set; }
        public DateTime UpdatedAt { get;private set; }
        public Guid UpdatedBy { get;private set; }
        public bool IsDeletd { get;private set; }
        public bool IsActive { get;private set; }
       

        

        public User()
        {

        }


        public User(Guid id, string email, string userName, string role)
        {
            if (!EmailRegex.IsMatch(email))
            {
                throw new NException(ErrorCodes.InvalidEmail,
                    $"Invalid email: '{email}'.");
            }
            if (!Domain.Role.IsValid(role))
            {
                throw new NException(ErrorCodes.InvalidRole,
                    $"Invalid role: '{role}'.");
            }
            Id = id;
            Email = email.ToLowerInvariant();
            UserName = userName.ToLowerInvariant();
            Role = role.ToLowerInvariant();
            CreatedAt = DateTime.UtcNow;
            UpdatedAt = DateTime.UtcNow;
        }


     


        public void SetPassword(string password, IPasswordHasher<User> passwordHasher)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new NException(ErrorCodes.InvalidPassword, "Password can not be empty");
            }

            PasswordHash = passwordHasher.HashPassword(this, password);
        }
        public void SetUserSecurityTag(IDictionary<string, string> securityTags, string userPin)
        {
            var tag = string.Empty;
            foreach (var item in securityTags.Keys)
            {
                tag += string.Concat(item);
            }

            SecurityTag = $"{tag}:{userPin}";
        }
       


        public bool ValidatePassword(string password, IPasswordHasher<User> passwordHasher)
            => passwordHasher.VerifyHashedPassword(this, PasswordHash, password) != PasswordVerificationResult.Failed;


    }
}
