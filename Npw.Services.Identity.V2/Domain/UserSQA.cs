﻿using N.Shared.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Domain
{
    public class UserSQA : IIdentifiable
    {
        public Guid Id { get; private set; }
        public string Question { get; private set; }
        public string Answer { get; set; }
        public Guid UserId { get; private set; }
        public DateTime CreatedAt { get; private set; }

        public UserSQA()
        {

        }

        public UserSQA(Guid userId, string question, string answer)
        {
            Id = new Guid();
            UserId = userId;
            Question = question;
            Answer = answer;
            CreatedAt = DateTime.UtcNow;
        }
    }
}
