﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity
{
    public static class ErrorCodes
    {
        public static string EmailInUse => "email_in_use";
        public static string UserNameInUse => "pin_in_use";

       
        public static string InvalidUserName => "invalid_pin";
        public static string InvalidCredentials => "invalid_credentials";
        public static string InvalidCurrentPassword => "invalid_current_password";
        public static string InvalidEmail => "invalid_email";
        public static string InvalidPassword => "invalid_password";
        public static string InvalidRole => "invalid_role";
        public static string RefreshTokenNotFound => "refresh_token_not_found";
        public static string RefreshTokenAlreadyRevoked => "refresh_token_already_revoked";
        public static string UserNotFound => "user_not_found";

        public static string InvalidAnserProvided => "invalid_answer_privided";

        public static string UmatchedSecurityQuestion => "unmatched_security_question";

        public static string InvalidDevice => "invalid_deviceid";
    }
}
