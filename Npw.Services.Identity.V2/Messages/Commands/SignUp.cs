﻿using N.Shared.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Messages.Commands
{
    public class SignUp : ICommand
    {
        public Guid Id { get;  }
        public string Email { get;  }
        public string UserName { get;  }
        public string Password { get;  }
        public string Role { get;  }

        public IDictionary<string, string> SecurityQnA { get; }

        public SignUp(Guid id, string password, string email,string userName, string role, IDictionary<string, string> securityQnA)
        {
            Id = id;
            Password = password;
            Role = role;
            Email = email;
            UserName = userName;
            SecurityQnA = securityQnA;
        }
    }
}
