﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Npw.Services.Identity.Controllers
{
    [Route("")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        // GET api/values
      
        // GET api/values/5
        [HttpGet("")]
        public ActionResult<string> Get()
        {
            return Ok("Identity Service is Running...");
        }

       
    }
}
