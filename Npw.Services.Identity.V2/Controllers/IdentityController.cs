﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using N.Shared.Authentication;
using N.Shared.Mvc;
using Npw.Services.Identity.Messages.Commands;
using Npw.Services.Identity.Services;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Controllers
{

    [Route("")]
    [ApiController]
    [Produces("application/json")]
    public class IdentityController : BaseController
    {
        private readonly IIdentityService _identityService;
        //private readonly IRefreshTokenService _refreshTokenService;
        //private ILogger<IdentityController> _logger;

        public IdentityController(IIdentityService identityService,
            IRefreshTokenService refreshTokenService, ILogger<IdentityController> logger)
        {
            _identityService = identityService;
            //_refreshTokenService = refreshTokenService;
            //_logger = logger;
        }

        [HttpPost("sign-up")]
        [SwaggerResponse(200, Type = typeof(string))]
        public async Task<IActionResult> SignUp(SignUp command)
        {
            command.BindId(c => c.Id);
            var result = await _identityService.SignUpAsync(command.Id,
                command.Password,
                command.Email, command.UserName,
                command.SecurityQnA, command.Role);

            return Ok(result);
        }

        [HttpPost("sign-in")]
        [SwaggerResponse(200, Type = typeof(JsonWebToken))]
        public async Task<IActionResult> SignIn(SignIn command)
           => Ok(await _identityService.SignInAsync(command.UserNameOrEmail, command.Password));

        [HttpPut("me/password")]
        [JwtAuth]
        public async Task<ActionResult> ChangePassword(ChangePassword command)
        {
            await _identityService.ChangePasswordAsync(command.Bind(c => c.UserId, UserId).UserId,
               command.CurrentPassword, command.NewPassword);

            return NoContent();
        }
    }
}
