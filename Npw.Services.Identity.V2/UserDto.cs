﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string ShortDescription { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool Completed { get; set; }
        public DateTime? CompletedAt { get; set; } //Aimed to be used to track whether or not user has completed security questions
        public string Gender { get; set; }
        public Guid CreatedBy { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public Guid UpdatedBy { get; private set; }
        public bool IsDeletd { get; private set; }
        public bool IsActive { get; private set; }

    }
}
