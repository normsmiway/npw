﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using N.Shared.Mongo;
using Npw.Services.Identity.Domain;

namespace Npw.Services.Identity.Repositories
{
    public class USerSqaRepository : IUSerSqaRepository
    {
        private readonly IMongoRepository<UserSQA> _repository;

        public USerSqaRepository(IMongoRepository<UserSQA> repository)
        {
            _repository = repository;
        }
        public async Task AddAsync(UserSQA userSqa)
            => await _repository.AddAsync(userSqa);

        public async Task<IEnumerable<UserSQA>> FinAsync(Guid userId)
            => await _repository.FindAsync(x => x.UserId == userId);

        public async Task<UserSQA> GetAsync(Guid id)
            => await _repository.GetAsync(id);

        public async Task UpdateAsync(UserSQA userSqa)
            => await _repository.UpdateAsync(userSqa);
    }
}
