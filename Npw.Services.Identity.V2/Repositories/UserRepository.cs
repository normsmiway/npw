﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using N.Shared.Mongo;
using Npw.Services.Identity.Domain;

namespace Npw.Services.Identity.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoRepository<User> _repository;
        private readonly IPasswordHasher<User> _passwordHarsher;
        private readonly ILogger<UserRepository> _logger;


        public UserRepository(IMongoRepository<User> repository,
            IPasswordHasher<User> passwordHasher, ILogger<UserRepository> logger)
        {
            _repository = repository;
            _passwordHarsher = passwordHasher;
            _logger = logger;
        }
        public async Task AddAsync(User user)
        {
            await _repository.AddAsync(user);
        }

        public Task AddUserQsaAsync(IEnumerable<IDictionary<string, string>> userSqa, User user)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> EmailExistsAsync(string email)
        {
            return await _repository.ExistsAsync(e => e.Email == email);
        }

        public async Task<User> GetAsync(Guid id)
        {
            return await _repository.GetAsync(id);
        }

        public async Task<User> GetAsync(string userNameOrEmail)
        {
            return await _repository.GetAsync(u =>
                    (u.UserName == userNameOrEmail.ToLowerInvariant()) ||
                    (u.Email == userNameOrEmail.ToLowerInvariant()));
        }

        public async Task UpdateAsync(User user)
        {
            await _repository.UpdateAsync(user);
        }

        public async Task<bool> UserNameExistsAsync(string userName)
        {
            return await _repository.ExistsAsync(x => x.UserName == userName);
        }
    }
}
