﻿using Npw.Services.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Repositories
{
    public interface IUSerSqaRepository
    {
        Task<UserSQA> GetAsync(Guid id);
        Task<IEnumerable<UserSQA>> FinAsync(Guid userId);
        Task AddAsync(UserSQA userSqa);
        Task UpdateAsync(UserSQA user);
    }
}
