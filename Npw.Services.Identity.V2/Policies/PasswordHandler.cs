﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Identity.Policies
{
    public class PasswordHandler : IPasswordHandler
    {
        private readonly PasswordOptions _options;
        private readonly PasswordPolicy passwordPolicy;
        public PasswordHandler(PasswordOptions options)
        {
            _options = options;
            PasswordPolicy.Lower_Case_length = _options.LowerCaseLenght;
            PasswordPolicy.Upper_Case_length = _options.UpperCaseLenght;
            PasswordPolicy.Minimum_Length = _options.MinimumLenght;
            PasswordPolicy.NonAlpha_length = _options.SpcialCharacterLength;
            PasswordPolicy.Numeric_length = _options.NumericCount;
        }
        public bool IsValid(string password)
        {
            return PasswordPolicy.IsValid(password);
        }
    }
}
