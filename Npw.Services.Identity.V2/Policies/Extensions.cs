﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using N.Shared;

namespace Npw.Services.Identity.Policies
{
    public static class Extensions
    {
        private const string V = "password-policy";
        private static readonly string SectionName = V;

        public static void AddPasswordPolicy(this IServiceCollection services)
        {
            IConfiguration configuration;
            using (var serviceProvider = services.BuildServiceProvider())
            {
                configuration = serviceProvider.GetService<IConfiguration>();
            }
            var section = configuration.GetSection(SectionName);
            var options = configuration.GetOptions<PasswordOptions>(SectionName);
            services.Configure<PasswordOptions>(section);
            services.AddSingleton(options);
            services.AddSingleton<IPasswordHandler, PasswordHandler>();
        }
    }
}
