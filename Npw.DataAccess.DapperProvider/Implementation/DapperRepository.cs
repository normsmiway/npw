﻿using Dapper;
using N.Shared.DataAccess.Interface;
using N.Shared.DataAccess.Models;
using N.Shared.Types;
using NLog;
using Npw.DataAccess.ExpressionParser.ExpressionToSQLParser.SQLParser;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Npw.Services.Shared.DataAccess.DapperProvider.Implementation
{
    public class DapperRepository<TEntity> : IDapperRepository<TEntity> where TEntity : IIdentifiable
    {
        public IDbConnection ConnectionString { get; set; }

        public IDbTransaction Transaction { get; set; }

        public IEnumerable<TEntity> All { get; set; }



        public Logger logger { get; set; }

        public DapperRepository()
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public void Delete(TEntity entity)
        {

        }




        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {

        }

        public Task<IEnumerable<TEntity>> GetAsync(
      Expression<Func<TEntity, bool>> filter = null,
      Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, bool paginate = true, long pageindex = 0, long pagesize = 5,
      Dictionary<string, object> includes = null)
        {
            var task = new Task<IEnumerable<TEntity>>(() => 
            {
                return Get(filter, orderBy,paginate,pageindex,pagesize, includes);
            });
            task.Start();
            return task;
        }

        public int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, Dictionary<string, object> parameters = null)
        {
            int NoOfAffectedRows = 0;
            using (IDbConnection cn = ConnectionString)
            {
                cn.Open();
                try
                {
                    NoOfAffectedRows = cn.Execute(sql, null);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    throw;
                }
                cn.Close();
            }
            return NoOfAffectedRows;
        }

        public TEntity Get(object model)
        {
            Type ModelType = typeof(TEntity);
            IDbConnection Connection = ConnectionString;
            TEntity result;
            string SQLQuery = "";
            object parameters = null;
            if (model.GetType() == typeof(DapperQueryModel))
            {
                var dappermodel = (DapperQueryModel)model;
                SQLQuery = dappermodel.Query;
                if (dappermodel.Parameters != null)
                {
                    parameters = dappermodel.Parameters;
                }
            }
            else if (model.GetType() == typeof(int) || model.GetType() == typeof(long) || model.GetType() == typeof(Guid))
            {
                SQLQuery = "SELECT * FROM " + ModelType.Name + " WHERE Id = " + model;
            }
            else
            {
                throw new Exception("Unrecongnized Model Type");
            }
            using (IDbConnection cn = Connection)
            {
                cn.Open();
                result = cn.QuerySingleOrDefault<TEntity>(SQLQuery, parameters);
                cn.Close();
            }
            return result;
        }


        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, bool paginate = true, long pageindex = 0, long pagesize = 5, Dictionary<string, object> parameters = null)
        {
            IDbConnection Connection = ConnectionString;
            Type ModelType = typeof(TEntity);
            string SQLQuery = "SELECT * FROM " + ModelType.Name;
            var results = new List<TEntity>();
            using (IDbConnection cn = Connection)
            {
                cn.Open();
                if (filter != null)
                {
                    var expParser = new SQLParser<TEntity>();
                    SQLQuery += " WHERE " + expParser.FilterExpressionToSQL(filter);
                }
                if (orderBy != null)
                {

                }
                if (paginate)
                {
                    SQLQuery += " ORDER BY Id OFFSET " + pageindex + " ROWS FETCH NEXT " + pagesize + " ROWS ONLY";
                }
                object parameter = null;
                using (var multi = cn.QueryMultiple(SQLQuery, parameter))
                {
                    results = multi.Read<TEntity>().ToList();
                }
                cn.Close();
            }

            return results;
        }

        public Task<TEntity> GetAsync(object model)
        {
            var task = new Task<TEntity>(() =>
            {
                return Get(model);
            });
            task.Start();
            return task;
        }

        public object Insert(TEntity entity)
        {
            long Id = 0;
            Id = InsertEntity(entity);
            return Id;
        }

        private long InsertEntity(TEntity entity)
        {
            long Id = 0;
            Type ModelType = entity.GetType();
            string SQLQuery = "INSERT INTO " + ModelType.Name + "( ";
            IDbConnection cn = ConnectionString;
            cn.Open();
            var checkforidentitycolumquery = "SELECT NAME AS COLUMNNAME FROM SYS.IDENTITY_COLUMNS WHERE OBJECT_NAME(OBJECT_ID) = '" + ModelType.Name + "'";
            var identitycolumns = cn.Query<string>(checkforidentitycolumquery, null).ToList();
            foreach (var item in ModelType.GetProperties())
            {
                if (!identitycolumns.Contains(item.Name))
                {
                    SQLQuery += item.Name + ",";
                }

            }
            //Remove Last Comma
            SQLQuery = SQLQuery.Remove(SQLQuery.Length - 1) + "";
            SQLQuery += " )";
            SQLQuery += "OUTPUT Inserted.ID ";
            SQLQuery += "VALUES (";
            foreach (var item in ModelType.GetProperties())
            {
                if (!identitycolumns.Contains(item.Name))
                {
                    var value = item.GetValue(entity, null);
                    if (value != null)
                    {
                        SQLQuery += ReturnData(value.ToString()) + ",";
                    }
                    else
                    {
                        SQLQuery += "NULL,";
                    }
                    
                }

            }
            //Remove Last Comma
            SQLQuery = SQLQuery.Remove(SQLQuery.Length - 1) + "";
            SQLQuery += ")";
            Transaction = cn.BeginTransaction();
            try
            {
                Id = cn.ExecuteScalar<long>(SQLQuery, null, Transaction);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            //cn.Close();
            return Id;
        }

        private string ReturnData(string Data)
        {
            string data = "";
            bool isParseTrue = false;
            if (Data == null)
            {
                data = "NULL,";
                return data;
            }

            if (Data[0] == '0')
            {
                data = "'" + Data + "'";
                return data;
            }

            long result = 0;
            isParseTrue = long.TryParse(Data, out result);
            if (isParseTrue)
            {
                data = Data;
                return data;
            }
            DateTime dateresult = new DateTime();
            isParseTrue = DateTime.TryParse(Data, out dateresult);
            if (isParseTrue)
            {
                data = "'" + Data + "'";
                return data;
            }
            else
            {
                data = "'" + Data + "'";
            }
            return data;
        }
        public Task<object> InsertAsync(TEntity entity)
        {
            var task = new Task<object>(() => { return InsertEntity(entity); });
			 task.Start();
            return task;
        }

        public IEnumerable<TEntity> SqlQuery(string sql, Dictionary<string, object> parameters = null)
        {
            IEnumerable<TEntity> results = null;
            IDbConnection Connection = ConnectionString;
            using (IDbConnection cn = Connection)
            {
                cn.Open();
                object parameter = null;
                bool isKeyExist = false;
                if (parameters != null)
                {
                    isKeyExist = parameters.TryGetValue("dynamicParameters", out parameter);
                }
                if (isKeyExist)
                {
                    var dapperparams = parameter as Dictionary<string, object>;
                    DynamicParameters dbParams = new DynamicParameters();
                    dbParams.AddDynamicParams(dapperparams);
                    using (var multi = cn.QueryMultiple(sql, dbParams))
                    {
                        results = multi.Read<TEntity>().ToList();
                    }
                }
                else
                {
                    using (var multi = cn.QueryMultiple(sql, null))
                    {
                        results = multi.Read<TEntity>().ToList();
                    }
                }
                cn.Close();
            }
            return results;
        }


        

        public T ScalarSqlQuery<T>(string sql, Dictionary<string, object> parameters = null)
        {
            //Repo
            T obj;
            IDbConnection Connection = ConnectionString;
            using (IDbConnection cn = Connection)
            {
                cn.Open();
                object ParamObject = null;
                bool isKeyExist = false;
                if (parameters != null)
                {
                    isKeyExist = parameters.TryGetValue("dynamicParameters", out ParamObject);
                }
                if (isKeyExist)
                {
                    var dapperparams = ParamObject as Dictionary<string, object>;
                    DynamicParameters dbParams = new DynamicParameters();
                    dbParams.AddDynamicParams(dapperparams);
                    obj = cn.ExecuteScalar<T>(sql, dbParams);
                }
                else
                {
                    obj = cn.ExecuteScalar<T>(sql, null);
                }
                cn.Close();
            }
            return obj;
        }

        public IEnumerable<T> SqlQuery<T>(string sql, Dictionary<string, object> parameters = null)
        {
            IEnumerable<T> results = null;
            IDbConnection Connection = ConnectionString;
            using (IDbConnection cn = Connection)
            {
                cn.Open();
                object parameter = null;
                bool isKeyExist = false;
                if (parameters != null)
                {
                    isKeyExist = parameters.TryGetValue("dynamicParameters", out parameter);
                }
                if (isKeyExist)
                {
                    var dapperparams = parameter as Dictionary<string, object>;
                    DynamicParameters dbParams = new DynamicParameters();
                    dbParams.AddDynamicParams(dapperparams);
                    using (var multi = cn.QueryMultiple(sql, dbParams))
                    {
                        results = multi.Read<T>().ToList();
                    }
                }
                else
                {
                    using (var multi = cn.QueryMultiple(sql, null))
                    {
                        results = multi.Read<T>().ToList();
                    }
                }
                cn.Close();
            }
            return results;
        }

        public void Update(TEntity entity)
        {
            UpdateEntity(entity);
        }

        private void UpdateEntity(TEntity entity)
        {
            Type ModelType = entity.GetType();
            string SQLQuery = "UPDATE " + ModelType.Name + " SET ";
            IDbConnection cn = ConnectionString;
            cn.Open();
            var checkforidentitycolumquery = "SELECT NAME AS COLUMNNAME FROM SYS.IDENTITY_COLUMNS WHERE OBJECT_NAME(OBJECT_ID) = '" + ModelType.Name + "'";
            var identitycolumns = cn.Query<string>(checkforidentitycolumquery, null).ToList();
           
            foreach (var item in ModelType.GetProperties())
            {
                if (!identitycolumns.Contains(item.Name))
                {

                    var checkifcolumnexist = "  SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + ModelType.Name + "' AND COLUMN_NAME = '"+ item.Name + "'";
                    int columncount = cn.ExecuteScalar<int>(checkifcolumnexist, null);
                    if (columncount > 0)
                    {
                        var Value = item.GetValue(entity, null);
                        if (Value == null)
                        {
                            SQLQuery += item.Name + "= NULL,";
                        }
                        else
                        {
                            SQLQuery += item.Name + "=" + ReturnData(Value.ToString()) + ",";
                        }
                    }
                }

            }
            //Remove Last Comma
            SQLQuery = SQLQuery.Remove(SQLQuery.Length - 1) + "";
            SQLQuery += " WHERE Id = " + ModelType.GetProperty("Id").GetValue(entity) + "";
            Transaction = cn.BeginTransaction();
            try
            {
                cn.Query(SQLQuery, null, Transaction);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            //cn.Close();
        }
    }
}
