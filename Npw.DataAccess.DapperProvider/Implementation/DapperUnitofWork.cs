﻿using N.Shared.DataAccess.Interface;
using N.Shared.Types;
using NLog;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Npw.Services.Shared.DataAccess.DapperProvider.Implementation
{
    public class DapperUnitofWork : IDapperUnitofWork
    {

        public Logger logger { get; set; }

        public IDbConnection ConnectionString { get; set; }

        private bool _disposed;

        public IDbTransaction Transaction { get; set; }

        public DapperUnitofWork()
        {
            logger = LogManager.GetCurrentClassLogger();
        }


        private Hashtable _repositories;
        public void BeginTransaction()
        {
           
        }

        public int Commit()
        {
            using (ConnectionString)
            {
                using (Transaction)
                {
                    try
                    {
                        Transaction.Commit();
                        ConnectionString.Close();
                        return 0;
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                     Transaction.Rollback();
                        ConnectionString.Close();
                        throw;
                    }
                    
                }
                
            }
        }

        public Task<int> CommitAsync()
        {
            return null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if(!_disposed && disposing)
            {
                //_context.Dispose();

                if (_repositories != null && _repositories.Values != null && _repositories.Values.OfType<IDisposable>().Any())
                {
                    foreach (IDisposable repository in _repositories.Values)
                    {
                        repository.Dispose();// dispose all repositries
                    }
                }
            }
            _disposed = true;
            GC.SuppressFinalize(this);
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : IIdentifiable
        {
            if (_repositories == null)
            {
                _repositories = new Hashtable();
            }

            var type = typeof(TEntity).Name;
            if (_repositories.ContainsKey(type))
            {
                return (IDapperRepository<TEntity>)_repositories[type];
            }

            var repositoryType = typeof(DapperRepository<>);
            _repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity))));
            return (IDapperRepository<TEntity>)_repositories[type];
        }

        public void Rollback()
        {
            using (ConnectionString)
            {
                using (Transaction)
                {
                    try
                    {
                        Transaction.Rollback();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        public int SaveChanges()
        {
            return Commit();
        }

        public Task<int> SaveChangesAsync()
        {
            var task = new Task<int>(() => { return Commit(); });
            return task;
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
