﻿using N.Shared.DataAccess.Interface;
using N.Shared.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface
{
    public interface IDapperRepository<TEntity> : IRepository<TEntity> where TEntity : IIdentifiable
    {
         IDbConnection ConnectionString { get; set; }

         IDbTransaction Transaction { get; set; }

        int ExecuteSqlCommand(string sql, bool doNotEnsureTransaction = false, int? timeout = null, Dictionary<string, object> parameters = null);
        IEnumerable<TEntity> SqlQuery(string sql, Dictionary<string, object> parameters = null);
        T ScalarSqlQuery<T>(string sql, Dictionary<string, object> parameters = null);
        IEnumerable<T> SqlQuery<T>(string sql, Dictionary<string, object> parameters = null);

    }
}
