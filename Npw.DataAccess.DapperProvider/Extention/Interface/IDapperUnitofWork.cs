﻿using N.Shared.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface
{
    public interface IDapperUnitofWork : IUnitOfWork
    {
         IDbConnection ConnectionString { get; set; }


         IDbTransaction Transaction { get; set; }
    }
}
