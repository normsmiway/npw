﻿using Dapper;
using N.Shared.DataAccess.Interface;
using N.Shared.Types;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Npw.Services.Shared.DataAccess.DapperProvider.Extention.ProceduresImplementation
{
    public static class DbConnection
    {
        public static T ExecuteSqlInsertUpdateQuery<T,G>(this IDapperRepository<G> Repository, string storedProc, DynamicParameters parameter, string connectionString, string outParam) where G : IIdentifiable
        {

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                connection.Execute(storedProc, parameter, commandType: CommandType.StoredProcedure);
                T rowCount = parameter.Get<T>(outParam);
                connection.Close();
                return rowCount;
            }
        }
        public static async Task<IEnumerable<T>> ExecuteSqlSelectQuery<T>(this IDapperRepository<T> Repository, string storedProc, DynamicParameters parameter, string connectionString) where T : IIdentifiable
        {
            IEnumerable<T> result;

            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                result = await connection.QueryAsync<T>(storedProc, parameter, commandType: CommandType.StoredProcedure);
                connection.Close();
                return result;
            }
        }
    }
}
