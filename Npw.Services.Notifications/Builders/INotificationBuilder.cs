﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Notifications.Builders
{
    public interface INotificationBuilder
    {
        INotificationBuilder WithSender(string senderEmail);
        INotificationBuilder WithReceiver(string receiverEmail);
        INotificationBuilder WithSubject(string subject);
        INotificationBuilder WithSubject(string template, params object[] @params);
        INotificationBuilder WithBody(string body);
        INotificationBuilder WithBody(string template, params object[] @params);
        INotificationBuilder WithBodyAndAttachments(string bodyText, string attachmentsPaths);
        MimeMessage Build();
    }
}
