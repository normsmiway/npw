﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MimeKit;

namespace Npw.Services.Notifications.Builders
{
    public class NotificationBuilder : INotificationBuilder
    {

        private readonly MimeMessage _message;

        public NotificationBuilder()
        {
            _message = new MimeMessage();
        }

        public static INotificationBuilder Create()
            => new NotificationBuilder();

        INotificationBuilder INotificationBuilder.WithBodyAndAttachments(string bodyText, string attachmentsPaths)
        {
            // now create the multipart/mixed container to hold the message text and the
            // image attachment
            var multipart = new Multipart("mixed");

            var body = new TextPart("plain")
            {
                Text = bodyText
            };

            attachmentsPaths.Split(new char[] { ',', ':' }).ToList().ForEach(path =>
            {
                // create an image attachment for the file located at path
                var attachment = new MimePart(Path.GetFileNameWithoutExtension(path), Path.GetExtension(path))
                {
                    Content = new MimeContent(File.OpenRead(path), ContentEncoding.Default),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64,
                    FileName = Path.GetFileName(path)
                };

                multipart.Add(attachment);
            });

            multipart.Add(body);


            // now set the multipart/mixed as the message body
            _message.Body = multipart;

            return this;
        }

        MimeMessage INotificationBuilder.Build()
        {
            _message.Date = DateTime.UtcNow;
            return _message;
        }


        INotificationBuilder INotificationBuilder.WithBody(string body)
        {
            _message.Body = new TextPart("plain")
            {
                Text = body
            };

            return this;
        }

        INotificationBuilder INotificationBuilder.WithBody(string template, params object[] @params)
              => ((INotificationBuilder)this).WithBody(string.Format(template, @params));

        INotificationBuilder INotificationBuilder.WithReceiver(string receiverEmail)
        {
            _message.To.Add(new MailboxAddress(receiverEmail));
            return this;
        }

        INotificationBuilder INotificationBuilder.WithSender(string senderEmail)
        {
            _message.From.Add(new MailboxAddress(senderEmail));
            return this;
        }

        INotificationBuilder INotificationBuilder.WithSubject(string subject)
        {
            _message.Subject = subject;
            return this;
        }

        INotificationBuilder INotificationBuilder.WithSubject(string template, params object[] @params)
            => ((INotificationBuilder)this).WithSubject(string.Format(template, @params));
    }
}
