﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Consul;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using N.Shared;
using N.Shared.Consul;
using N.Shared.Dispatchers;
using N.Shared.Jaeger;
using N.Shared.MailKit;
using N.Shared.Mongo;
using N.Shared.Mvc;
using N.Shared.RabbitMq;
using N.Shared.Redis;
using N.Shared.Swagger;
using Npw.Services.Notifications.Messages.Commands;
using Npw.Services.Notifications.Messages.Events;
using Npw.Services.Notifications.Messages.Events.Identity;

namespace Npw.Services.Notifications
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer Container { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCustomMvc();
            services.AddSwaggerDocs();
            services.AddConsul();

            services.AddJaeger();
            services.AddOpenTracing();
            services.AddRedis();

            services.AddInitializers(typeof(IMongoDbInitializer));


            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly())
                .AsImplementedInterfaces();
            builder.Populate(services);


            builder.AddDispatchers();

            builder.AddRabbitMq();
            builder.AddMailKit();
            builder.AddMongo();
            //builder.AddMongoRepository<User>("Users");


            Container = builder.Build();

            return new AutofacServiceProvider(Container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
             IApplicationLifetime applicationLifetime, IConsulClient client,
             IStartupInitializer startupInitializer)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "local")
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAllForwardedHeaders();
            app.UseSwaggerDocs();
            app.UseErrorHandler();
            app.UseServiceId();
            app.UseMvc();
            app.UseRabbitMq().SubscribeCommand<SendEmailNotification>()
               .SubscribeEvent<SignedUp>(@namespace: "identity")
               .SubscribeEvent<PasswordGenerated>(@namespace: "identity"); 


            var consulServiceId = app.UseConsul();
            applicationLifetime.ApplicationStopped.Register(() =>
            {
                client.Agent.ServiceDeregister(consulServiceId);
                Container.Dispose();
            });

            startupInitializer.InitializeAsync();
        }
    }
}
