﻿using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.MailKit;
using N.Shared.RabbitMq;
using Npw.Services.Notifications.Builders;
using Npw.Services.Notifications.Messages.Events.Identity;
using Npw.Services.Notifications.Services;
using Npw.Services.Notifications.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Notifications.Handlers
{

    public class PasswordGeneratedHandler : IEventHandler<PasswordGenerated>
    {
        private readonly ILogger<PasswordGeneratedHandler> _logger;
        private readonly INotificationService _notificationService;
        private readonly MailKitOptions _options;

        public PasswordGeneratedHandler(INotificationService notificationService, ILogger<PasswordGeneratedHandler> logger, MailKitOptions options)
        {
            _notificationService = notificationService;
            _options = options;
            _logger = logger;
        }

        public async Task HandleAsync(PasswordGenerated @event, ICorrelationContext context)
        {
            _logger.LogInformation($"Sending signed up email message to: '{@event.Email}'.");

            var message = NotificationBuilder.Create()
                .WithSender(_options.Email).WithReceiver(@event.Email)
                .WithSubject(MessageTemplates.UserSignUpSubject, "NFW")
                .WithBody(MessageTemplates.UserSignedUpBody, @event.UserName, @event.Email, @event.UserName, @event.Password, @event.Role)
                .Build();

            await _notificationService.SendAsync(message);
        }
    }
}
