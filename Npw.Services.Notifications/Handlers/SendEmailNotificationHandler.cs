﻿using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.MailKit;
using N.Shared.RabbitMq;
using Npw.Services.Notifications.Builders;
using Npw.Services.Notifications.Messages.Commands;
using Npw.Services.Notifications.Services;
using System.Threading.Tasks;

namespace Npw.Services.Notifications.Handlers
{
    public class SendEmailNotificationHandler : ICommandHandler<SendEmailNotification>
    {
        private readonly ILogger<SendEmailNotificationHandler> _logger;
        private readonly INotificationService _notificationService;
        private readonly MailKitOptions _options;

        public SendEmailNotificationHandler(INotificationService notificationService,MailKitOptions options, ILogger<SendEmailNotificationHandler> logger)
        {
            _notificationService = notificationService;
            _options = options;
            _logger = logger;
        }

        public async Task HandleAsync(SendEmailNotification command, ICorrelationContext context)
        {
            try
            {

                _logger.LogInformation($"Sending an email message to: '{command.ToEmail}'.");

                // Publish: EmailSent
                // Publish: SendEmailNotificationRejected
                var message = NotificationBuilder.Create()
                    .WithSender(_options.Email).WithReceiver(command.ToEmail)
                    .WithSubject(command.Title).WithBody(command.Message)
                    .Build();

                await _notificationService.SendAsync(message);

            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}

