﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.Dispatchers;
using System.Threading.Tasks;

namespace Npw.Services.Users.Controllers
{
    public class NotificationsController : BaseController
    {
        public NotificationsController(IDispatcher dispatcher) : base(dispatcher)
        {
        }



        [HttpGet]
        public async Task<ActionResult> Get()
        => await Task.Run(()=>Content("Service is running..."));
    }


}
