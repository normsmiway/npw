﻿using N.Shared.Messages;
using Newtonsoft.Json;

namespace Npw.Services.Notifications.Messages.Commands
{
    public class SendEmailNotification : ICommand
    {
        public string FromEmail { get; }
        public string ToEmail { get; }
        public string Title { get; }
        public string Message { get; }

        [JsonConstructor]
        public SendEmailNotification(string fromEmail,string toEmail, string title, string message)
        {
            FromEmail = fromEmail;
            ToEmail = toEmail;
            Title = title;
            Message = message;
        }
    }
}
