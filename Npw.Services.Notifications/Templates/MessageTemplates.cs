﻿namespace Npw.Services.Notifications.Templates
{
    public class MessageTemplates
    {
        public static string UserSignUpSubject => "{0} New Membership";
        public static string UserSignedUpBody => @"Hi {0},
Your account has been created with the following details:
   -Email: {1},
   -UserName: {2},
   -Password: {3},
   -Role: {4}.

Please ensure to change your password to proceed.
      
 Best regards,
 Npw Team.";
    }
}
