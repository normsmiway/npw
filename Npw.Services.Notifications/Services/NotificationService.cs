﻿using MailKit.Net.Smtp;
using MimeKit;
using N.Shared.MailKit;
using System.Threading.Tasks;

namespace Npw.Services.Notifications.Services
{

    public class NotificationService : INotificationService
    {
        private readonly MailKitOptions _options;
        public NotificationService(MailKitOptions options)
        {
            _options = options;
        }

        public async Task SendAsync(MimeMessage message)
        {
            using(var client=new SmtpClient())
            {
                client.Connect(_options.SmtpHost, _options.Port, true);
                client.Authenticate(_options.Username, _options.Password);

                await client.SendAsync(message);
                client.Disconnect(true);
            }
        }
    }
}
