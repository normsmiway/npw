﻿using MimeKit;
using System.Threading.Tasks;

namespace Npw.Services.Notifications.Services
{
    public interface INotificationService
    {
        Task SendAsync(MimeMessage message);
    }
}
