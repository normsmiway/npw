#!/bin/bash
export ASPNETCORE_ENVIRONMENT=local
DOTNET_RUN=./scripts/dotnet-run.sh
PREFIX=Npw
SERVICE=$PREFIX.Services
REPOSITORIES=($PREFIX.Api.Gateway.Mobile  $PREFIX.Api.Gateway.WebClient $SERVICE.Access $SERVICE.Identity $SERVICE.Beneficiaries $SERVICE.Documents $SERVICE.Transactions $SERVICE.Users $SERVICE.Workflows)

for REPOSITORY in ${REPOSITORIES[*]}
do
	 echo ========================================================
	 echo Starting a service: $REPOSITORY
	 echo ========================================================
     cd $REPOSITORY
     $DOTNET_RUN &
     cd ..
done

cmd /k