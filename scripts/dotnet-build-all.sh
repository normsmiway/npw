#!/bin/bash
export ASPNETCORE_ENVIRONMENT=docker
BUILD=./scripts/dotnet-build.sh
PREFIX=Npw
SERVICE=$PREFIX.Services
REPOSITORIES=($PREFIX.Api.Gateway.Mobile  $PREFIX.Api.Gateway.WebClient $SERVICE.Access $SERVICE.Identity $SERVICE.Beneficiaries $SERVICE.Documents $SERVICE.Transactions $SERVICE.Users $SERVICE.Workflows)

for REPOSITORY in ${REPOSITORIES[*]}
do
	 echo ========================================================
	 echo Building a project: $REPOSITORY
	 echo ========================================================
     cd $REPOSITORY
     $BUILD
     cd ..
done