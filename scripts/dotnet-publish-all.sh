#!/bin/bash
export ASPNETCORE_ENVIRONMENT=local
PUBLISH=./scripts/dotnet-publish.sh
PREFIX=Npw
SERVICE=$PREFIX.Services
REPOSITORIES=($PREFIX.Api.Gateway.Mobile  $PREFIX.Api.Gateway.WebClient $SERVICE.Access $SERVICE.Identity $SERVICE.Beneficiaries $SERVICE.Documents $SERVICE.Transactions $SERVICE.Users $SERVICE.Workflows)

for REPOSITORY in ${REPOSITORIES[*]}
do
	 echo ========================================================
	 echo Publishing a project: $REPOSITORY
	 echo ========================================================
     cd $REPOSITORY
     $PUBLISH
     cd ..
done