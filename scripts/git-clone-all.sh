#!/bin/bash
REPOSITORIES=(Npw Npw.Api.Gateway.Mobile Npw.Api.Gateway.WebClient Npw.Services.Access Npw.Services.Identity Npw.Services.Beneficiaries Npw.Services.Documents Npw.Services.Transactions Npw.Services.Users Npw.Services.Workflows)
if [ "$1" = "-p" ]
  then
    echo ${REPOSITORIES[@]} | sed -E -e 's/[[:blank:]]+/\n/g' | xargs -I {} -n 1 -P 0 sh -c 'printf "========================================================\nCloning repository: {}\n========================================================\n"; git clone https://github.com/devmentors/{}.git'
  else
    for REPOSITORY in ${REPOSITORIES[*]}
    do
      echo ========================================================
      echo Cloning repository: $REPOSITORY
      echo ========================================================
      REPO_URL=https://github.com/devmentors/$REPOSITORY.git
      git clone $REPO_URL
    done
fi