﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Npw.DataAccess.ExpressionParser.ExpressionToSQLParser.SQLParser
{
    public class OperationsConfig
    {
        public IEnumerable<AllOperations> Operations { get; set; }
    }

    public class AllOperations
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
