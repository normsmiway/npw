﻿using N.Shared.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Npw.DataAccess.ExpressionParser.ExpressionToSQLParser.SQLParser
{
    public class SQLParser<T> where T: IIdentifiable
    {
        //x => x.Name == 'Tayo'
        //x => x.Name == 'Tayo' && x.Age == 12
        //x => x.Amount + 5 == 12 && x.Age == 12
        public string FilterExpressionToSQL(Expression<Func<T, bool>> filter)
        {
            string ResultingQuery = "";
            string SQLOperation = "";
            var lambdaExp = (LambdaExpression)filter;
            var subexpression = (BinaryExpression)lambdaExp.Body;
            SQLOperation = " " + ReturnSQLOperation(lambdaExp.Body.NodeType) + " ";
            ResultingQuery = CheckSubExpression(subexpression.Left) + SQLOperation + CheckSubExpression(subexpression.Right);
            return ResultingQuery;
        }

        //O(N + M)
        public string CheckSubExpression(Expression subex)
        {
           
            string ResultingQuery = "";
            string SQLOperation = "";
            if (subex.NodeType == ExpressionType.MemberAccess)
            {
                var newExpression = (Expression)subex;
                var newMemberExpression = (MemberExpression)newExpression;
                if (newMemberExpression.Member.ReflectedType == typeof(IIdentifiable) || newMemberExpression.Member.ReflectedType == typeof(T))
                {
                    PropertyInfo outerProp = (PropertyInfo)newMemberExpression.Member;
                    ResultingQuery += outerProp.Name;
                }
                else
                {
                    if (newMemberExpression.Member.GetType().BaseType == typeof(PropertyInfo))
                    {
                        var constexp = (ConstantExpression)newMemberExpression.Expression;
                        PropertyInfo prop = (PropertyInfo)newMemberExpression.Member;
                        var constexpobj = constexp.Value.GetType().GetProperty(prop.Name).GetValue(constexp.Value).ToString();
                        ResultingQuery += ReturnData(constexpobj);
                    }
                    else
                    {
                        //try
                        //{
                            var constexp = (ConstantExpression)newMemberExpression.Expression;
                            FieldInfo fieldprop = (FieldInfo)newMemberExpression.Member;
                            var constexpobj = constexp.Value.GetType().GetField(fieldprop.Name).GetValue(constexp.Value).ToString();
                            ResultingQuery += ReturnData(constexpobj);
                        //}
                        //catch (Exception)
                        //{
                        //    var fieldMemberExpression = (MemberExpression)newExpression;
                        //    FieldInfo fieldtype = (FieldInfo)fieldMemberExpression.Member;
                        //    var fieldobj =fieldtype.GetValue(fieldMemberExpression.GetType()).ToString();
                        //    ResultingQuery += ReturnData(fieldobj);
                        //}
                    }
                }
                
            }
            else if (subex.NodeType == ExpressionType.Constant)
            {
                string ConstantValue = "";
                var constantExpression = (Expression)subex;
                var newconstantExpression = (ConstantExpression)constantExpression;
                ConstantValue = ReturnData(newconstantExpression.Value.ToString());
                ResultingQuery += ConstantValue;
            }
            else if (subex.NodeType == ExpressionType.Convert)
            {
                var convertExpression = (UnaryExpression)subex;
                var newMemberExpression = (MemberExpression)convertExpression.Operand;
                if (newMemberExpression.Member.ReflectedType == typeof(IIdentifiable) || newMemberExpression.Member.ReflectedType == typeof(T))
                {
                    PropertyInfo outerProp = (PropertyInfo)newMemberExpression.Member;
                    ResultingQuery += outerProp.Name;
                }
            }
            else if (subex.NodeType == ExpressionType.Call)
            {
                var callExpression = (MethodCallExpression)subex;

                var modelobjectexp = callExpression.Object;
                var newMemberExpression = (MemberExpression)modelobjectexp;
                if (newMemberExpression.Member.ReflectedType == typeof(IIdentifiable) || newMemberExpression.Member.ReflectedType == typeof(T))
                {
                    PropertyInfo outerProp = (PropertyInfo)newMemberExpression.Member;
                    ResultingQuery += outerProp.Name;
                }

                var methodName = callExpression.Method.Name;
                var argument = (ConstantExpression)callExpression.Arguments[0];
                var argumentValue = argument.Value.ToString();
                if (methodName == "Contains")
                {
                    ResultingQuery += " LIKE '%" + argumentValue + "%'";
                }
            }
            else 
            {
                var subexp = (BinaryExpression)subex;
                SQLOperation = " " + ReturnSQLOperation(subex.NodeType) + " ";
                ResultingQuery += CheckSubExpression(subexp.Left) + SQLOperation + CheckSubExpression((subexp.Right));
            }
            return ResultingQuery;
        }

        public string ReturnSQLOperation(ExpressionType NodeType)
        {
            string SQLOperation = "";
            string ExpressionTypeName = Enum.GetName(typeof(ExpressionType), NodeType);
            string assemblyPath = Assembly.GetExecutingAssembly().Location;
            string assemblyDirectory = Path.GetDirectoryName(assemblyPath);
            string textPath = Path.Combine(assemblyDirectory, "operationconfig.json");
            Dictionary<string, string> config = JsonConvert.DeserializeObject<Dictionary<string,string>>(File.ReadAllText(textPath));
            bool isKeyExist = config.TryGetValue(ExpressionTypeName, out SQLOperation);
            if (!isKeyExist)
            {
                throw new Exception("Given Key Does Not Exist");
            }
            return SQLOperation;
        }

        private string ReturnData(string Data)
        {
            string data = "";
            bool isParseTrue = false;
            if (Data == null)
            {
                data = "NULL,";
                return data;
            }

            long result = 0;
            isParseTrue = long.TryParse(Data, out result);
            if (isParseTrue)
            {
                data = Data;
                return data;
            }
            DateTime dateresult = new DateTime();
            isParseTrue = DateTime.TryParse(Data, out dateresult);
            if (isParseTrue)
            {
                data = "'" + Data + "'";
                return data;
            }
            else
            {
                data = "'" + Data + "'";
            }
            return data;
        }
    }
}
