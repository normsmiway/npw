﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Npw.Services.Workflows.Migrations
{
    public partial class init_db : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DepartmentWorkFlow",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    DeptId = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false),
                    Task = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentWorkFlow", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkFlow",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkFlow", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkflowRequestSetup",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    WorkFlowId = table.Column<Guid>(nullable: false),
                    RequestId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkflowRequestSetup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkFlowSetup",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    WorkflowId = table.Column<Guid>(nullable: false),
                    DepartmentWorkflowId = table.Column<Guid>(nullable: false),
                    Order = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkFlowSetup", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentWorkFlow_Id",
                table: "DepartmentWorkFlow",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkFlow_Id",
                table: "WorkFlow",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkFlow_Name",
                table: "WorkFlow",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_WorkflowRequestSetup_Id",
                table: "WorkflowRequestSetup",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkFlowSetup_Id",
                table: "WorkFlowSetup",
                column: "Id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DepartmentWorkFlow");

            migrationBuilder.DropTable(
                name: "WorkFlow");

            migrationBuilder.DropTable(
                name: "WorkflowRequestSetup");

            migrationBuilder.DropTable(
                name: "WorkFlowSetup");
        }
    }
}
