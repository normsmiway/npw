﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Messages.Commands.WorkFlow
{
    //[MessageNamespace("workflow")]
    public class CreateDepartmentWorkFlowCommand : ICommand
    {
        public Guid Id { get; set; }
        public Guid DeptId { get; set; }
        public List<DepartmentWorkFlowModel> workflowlist { get; set; }

        [JsonConstructor]
        public CreateDepartmentWorkFlowCommand(Guid id, Guid deptId, List<DepartmentWorkFlowModel> Workflowlist)
        {
            Id = id;
            workflowlist = Workflowlist;
            DeptId = deptId;
        }
    }

    public class DepartmentWorkFlowModel
    {
        public Guid UserId { get; set; }
        public int Order { get; set; }
        public string Task { get; set; }
    }
}
