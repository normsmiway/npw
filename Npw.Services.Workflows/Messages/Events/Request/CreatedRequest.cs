﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Messages.Events.Request
{
    //public class CreatedRequest
    //{
    //}
    [MessageNamespace("request")]
    public class CreatedRequest : IEvent
    {
        public Guid RequestId { get; }

        public Guid WorkFlowId { get; }

        [JsonConstructor]
        public CreatedRequest(Guid requestId, Guid workFlowId)
        {
            RequestId = requestId;
            WorkFlowId = workFlowId;
        }
    }
}
