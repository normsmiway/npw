﻿using Microsoft.AspNetCore.Builder;
using Npw.Services.Workflows.Data;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Npw.Services.Workflows.Extensions
{

    public static class AppBuilderExtension
    {
        public static IApplicationBuilder SetUpAutoMigrations(this IApplicationBuilder app, IServiceProvider service)
        {
            try
            {
                var context = service.GetService<WorkflowDBContext>();
                context.Database.Migrate();

               //You can set up Seed Data here.
               
            }
            catch (Exception ex) { }

            return app;

        }
    }

}
