﻿using Microsoft.AspNetCore.Mvc;
using N.Shared.Dispatchers;
using Npw.Services.Workflows.Models;
using Npw.Services.Workflows.Query.WorkFlow;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Controllers
{
    //public class WorkflowController
    //{
    //}

    public class WorkflowController : BaseController
    {
        public WorkflowController(IDispatcher dispatcher) : base(dispatcher)
        {
        }

        [HttpGet("{userId:guid}/{departmentId:guid}")]
        public async Task<ActionResult<WorkFlowConfigDTO>> GetUserWorkFlowConfig([FromRoute]GetUserWorkFlowConfig query)
        {
            return Single(await QueryAsync(query));
        }


        [HttpGet]
        public async Task<ActionResult<WorkFlowListDTO>> GetWorkFlows([FromQuery] GetWorkFlows query)
        {
            return Single(await QueryAsync(query));
        }
    }

}
