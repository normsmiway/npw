﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Npw.Services.Workflows.Controllers
{
    [Route("")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
   

        // GET api/values/5
        [HttpGet("")]
        public ActionResult<string> Get(int id)
        {
            return "Workflow service running...";
        }

      
    }
}
