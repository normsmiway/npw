﻿using N.Shared.Types;
using Npw.Services.Workflows.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Query.WorkFlow
{
    public class GetUserWorkFlowConfig : IQuery<WorkFlowConfigDTO>
    {
        public Guid UserId { get; set; }
        public Guid DepartmentId { get; set; }
    }
}
