﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Workflows.Domain;
using Npw.Services.Workflows.Messages.Events.Request;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Handlers.Request
{
    public class CreateWorkFlowRequestSetupHandler : IEventHandler<CreatedRequest>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<WorkflowRequestSetup> wrkflwsetuprepo;
        private ILogger logger;
        private IConfiguration config;
        public CreateWorkFlowRequestSetupHandler(IDapperUnitofWork _unitofwork, ILogger<CreateWorkFlowRequestSetupHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            wrkflwsetuprepo = unitofwork.Repository<WorkflowRequestSetup>() as IDapperRepository<WorkflowRequestSetup>;
            logger = _logger;
            config = _config;
        }

        public async Task HandleAsync(CreatedRequest @event, ICorrelationContext context)
        {
            logger.LogInformation("Creating Transaction Handler...Building Model");
            try
            {
                logger.LogInformation("Command Model is " + JsonConvert.SerializeObject(@event));
                logger.LogInformation("Model Built...Getting Connevction String");
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                var wrkflw = new WorkflowRequestSetup();
                logger.LogInformation("Build Workflow Model");
                wrkflw.CompleteModel(@event.WorkFlowId, @event.RequestId);
                logger.LogInformation("Model Built...Inserting Workflow");
                var result = await wrkflwsetuprepo.InsertAsync(wrkflw);
                logger.LogInformation("Executing Unit of Work...");
                unitofwork.ConnectionString = new SqlConnection(ConnectionString);
                unitofwork.Transaction = wrkflwsetuprepo.Transaction;
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Unit of Work Executed");
            }
            catch (Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation("Error Is " + ex.Message);
                logger.LogInformation("Stack Trace is " + ex.StackTrace);
            }
        }
    }
}
