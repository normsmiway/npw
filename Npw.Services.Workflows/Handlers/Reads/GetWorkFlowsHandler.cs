﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.Types;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Workflows.Domain;
using Npw.Services.Workflows.Models;
using Npw.Services.Workflows.Query.WorkFlow;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Handlers.Reads
{
    public class GetWorkFlowsHandler : IQueryHandler<GetWorkFlows, WorkFlowListDTO>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<WorkFlow> workflowrepo;
        private ILogger logger;
        private IConfiguration config;
        public GetWorkFlowsHandler(IDapperUnitofWork _unitofwork, ILogger<GetUserWorkFlowConfigHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            workflowrepo = unitofwork.Repository<WorkFlow>() as IDapperRepository<WorkFlow>;
            logger = _logger;
            config = _config;
        }

        public async Task<WorkFlowListDTO> HandleAsync(GetWorkFlows query)
        {
            logger.LogInformation("Getting WorkFlow Config....");
            try
            {
                logger.LogInformation("Model is " + JsonConvert.SerializeObject(query));
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                workflowrepo.ConnectionString = new SqlConnection(ConnectionString);
                logger.LogInformation("Calling Database");
                var results = await workflowrepo.GetAsync(null,null,false);
                var departmetwrkflw = results.ToList();

                var workflowlist = departmetwrkflw.Select(x => new WorkFlowModel
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

                logger.LogInformation("WorkFLow List Gotten....");
                return new WorkFlowListDTO()
                {
                    workflows = workflowlist
                } ?? null;
            }
            catch (System.Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation(ex.Message);
                logger.LogInformation(ex.StackTrace);
                throw new NException("96", "An Error Occured While Fetching WorkFlow Configuration");
            }
        }
    }
}
