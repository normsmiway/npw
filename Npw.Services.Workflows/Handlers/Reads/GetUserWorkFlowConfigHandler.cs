﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.Types;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Workflows.Domain;
using Npw.Services.Workflows.Models;
using Npw.Services.Workflows.Query.WorkFlow;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Handlers.Reads
{
    public class GetUserWorkFlowConfigHandler : IQueryHandler<GetUserWorkFlowConfig, WorkFlowConfigDTO>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<WorkFlowSetup> workflowsetuprepo;
        private IDapperRepository<DepartmentWorkFlow> departmentworkflow;
        private ILogger logger;
        private IConfiguration config;
        public GetUserWorkFlowConfigHandler(IDapperUnitofWork _unitofwork, ILogger<GetUserWorkFlowConfigHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            workflowsetuprepo = unitofwork.Repository<WorkFlowSetup>() as IDapperRepository<WorkFlowSetup>;
            departmentworkflow = unitofwork.Repository<DepartmentWorkFlow>() as IDapperRepository<DepartmentWorkFlow>;
            logger = _logger;
            config = _config;
        }

        public async Task<WorkFlowConfigDTO> HandleAsync(GetUserWorkFlowConfig query)
        {
            logger.LogInformation("Getting WorkFlow Config....");
            try
            {
                logger.LogInformation("Model is " + JsonConvert.SerializeObject(query));
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                departmentworkflow.ConnectionString = new SqlConnection(ConnectionString);
                logger.LogInformation("Calling Database");
                Guid DepartmentId = query.DepartmentId;
                Guid UserId = query.UserId;
                var results = await departmentworkflow.GetAsync(x => x.DeptId == DepartmentId && x.UserId == UserId);

                var departmetwrkflw = results.SingleOrDefault();

                Guid DepartmentWorkFLowID = departmetwrkflw.Id;
                workflowsetuprepo.ConnectionString = new SqlConnection(ConnectionString);
                var result = await workflowsetuprepo.GetAsync(x => x.DepartmentWorkflowId == DepartmentWorkFLowID);
                var wrkflwsetup = result.SingleOrDefault();

                logger.LogInformation("WorkFLow COnfig Gotten....");
                return new WorkFlowConfigDTO()
                {
                    DepartMentWorkFlowId = departmetwrkflw.Id,
                    UserOrder = departmetwrkflw.Order,
                    DepartmentOrder = wrkflwsetup.Order,
                    WorkFlowId = wrkflwsetup.WorkflowId
                } ?? null;
            }
            catch (System.Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation(ex.Message);
                logger.LogInformation(ex.StackTrace);
                throw new NException("96", "An Error Occured While Fetching WorkFlow Configuration");
            }
        }
    }
}
