﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Workflows.Domain;
using Npw.Services.Workflows.Messages.Commands.WorkFlow;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Handlers.Writes
{
    public class CreateDepartmentWorkflowHandler : ICommandHandler<CreateDepartmentWorkFlowCommand>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<DepartmentWorkFlow> dptwrkflwrepo;
        private ILogger logger;
        private IConfiguration config;
        public CreateDepartmentWorkflowHandler(IDapperUnitofWork _unitofwork, ILogger<CreateDepartmentWorkflowHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            dptwrkflwrepo = unitofwork.Repository<DepartmentWorkFlow>() as IDapperRepository<DepartmentWorkFlow>;
            logger = _logger;
            config = _config;
        }
        public async Task HandleAsync(CreateDepartmentWorkFlowCommand command, ICorrelationContext context)
        {
            logger.LogInformation("Creating Transaction Handler...Building Model");
            try
            {
                logger.LogInformation("Command Model is " + JsonConvert.SerializeObject(command));
                logger.LogInformation("Model Built...Getting Connevction String");
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                dptwrkflwrepo.ConnectionString = new SqlConnection(ConnectionString);
                foreach (var item in command.workflowlist)
                {
                    var dptwrkflw = new DepartmentWorkFlow();
                    logger.LogInformation("Build Transaction Model");
                    dptwrkflw.CompleteModel(item.UserId,command.DeptId,item.Order,item.Task);
                    logger.LogInformation("Model Built...Inserting Transaction");
                    await dptwrkflwrepo.InsertAsync(dptwrkflw);
                    logger.LogInformation("Transaction Inserted");
                }
                logger.LogInformation("Executing Unit of Work...");
                unitofwork.ConnectionString = new SqlConnection(ConnectionString);
                unitofwork.Transaction = dptwrkflwrepo.Transaction;
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Unit of Work Executed");
            }
            catch (Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation("Error Is " + ex.Message);
                logger.LogInformation("Stack Trace is " + ex.StackTrace);
            }
        }


    }
}
