﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using N.Shared.Handlers;
using N.Shared.RabbitMq;
using Newtonsoft.Json;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Workflows.Domain;
using Npw.Services.Workflows.Messages.Commands.WorkFlow;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Handlers.Writes
{
    //public class CreateWorkFlowHandler
    //{
    //}


    public class CreateWorkFlowHandler : ICommandHandler<CreateWorkFlowCommand>
    {
        private IDapperUnitofWork unitofwork;
        private IDapperRepository<WorkFlow> wrkflwrepo;
        private IDapperRepository<WorkFlowSetup> wrkflwsetuprepo;
        private ILogger logger;
        private IConfiguration config;
        public CreateWorkFlowHandler(IDapperUnitofWork _unitofwork, ILogger<CreateWorkFlowHandler> _logger, IConfiguration _config)
        {
            unitofwork = _unitofwork;
            wrkflwrepo = unitofwork.Repository<WorkFlow>() as IDapperRepository<WorkFlow>;
            wrkflwsetuprepo = unitofwork.Repository<WorkFlowSetup>() as IDapperRepository<WorkFlowSetup>;
            logger = _logger;
            config = _config;
        }
        public async Task HandleAsync(CreateWorkFlowCommand command, ICorrelationContext context)
        {
            logger.LogInformation("Creating Transaction Handler...Building Model");
            try
            {
                logger.LogInformation("Command Model is " + JsonConvert.SerializeObject(command));
                logger.LogInformation("Model Built...Getting Connevction String");
                string ConnectionString = config.GetValue<string>("NpwConnectionString");
                logger.LogInformation("Connection String Gotten...Connection String Is ..." + ConnectionString);
                var wrkflw = new WorkFlow();
                logger.LogInformation("Build Workflow Model");
                wrkflw.CompleteModel(command.Name);
                logger.LogInformation("Model Built...Inserting Workflow");
                var result = await wrkflwrepo.InsertAsync(wrkflw);
                unitofwork.ConnectionString = new SqlConnection(ConnectionString);
                unitofwork.Transaction = wrkflwrepo.Transaction;
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Workflow Inserted");
                var wrkflwid = (Guid)result;
                wrkflwrepo.ConnectionString = new SqlConnection(ConnectionString);
                foreach (var item in command.workflowlist)
                {
                    var wrkflwsetup = new WorkFlowSetup();
                    logger.LogInformation("Build WorkFlow Setup Model");
                    wrkflwsetup.CompleteModel(wrkflwid,item.DepartmentWorkflowId,item.Order);
                    logger.LogInformation("Model Built...Inserting Workflow Setup");
                    await wrkflwsetuprepo.InsertAsync(wrkflwsetup);
                    logger.LogInformation("Workflow Setup Inserted");
                }
                logger.LogInformation("Executing Unit of Work...");
                unitofwork.ConnectionString = new SqlConnection(ConnectionString);
                unitofwork.Transaction = wrkflwsetuprepo.Transaction;
                await unitofwork.SaveChangesAsync();
                logger.LogInformation("Unit of Work Executed");
            }
            catch (Exception ex)
            {
                logger.LogInformation("An Error Occured");
                logger.LogInformation("Error Is " + ex.Message);
                logger.LogInformation("Stack Trace is " + ex.StackTrace);
            }
        }


    }
}
