﻿using Maverick.DataAccess;
using Maverick.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Npw.Services.Workflows.Data
{
    public class WorkflowDBContext : BaseDbContext
    {
        public WorkflowDBContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var assembly = Assembly.GetExecutingAssembly();
            modelBuilder.AddEntityConfigurations(assembly);
            base.OnModelCreating(modelBuilder);
        }

    }
}
