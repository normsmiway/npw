﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Consul;
using Maverick.DataAccess.Repository;
using Maverick.DataAccess.Repository.Abstract;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using N.Shared;
using N.Shared.Consul;
using N.Shared.Dispatchers;
using N.Shared.Jaeger;
using N.Shared.Mongo;
using N.Shared.Mvc;
using N.Shared.RabbitMq;
using N.Shared.Redis;
using N.Shared.Swagger;
using Npw.Services.Shared.DataAccess.DapperProvider.Extention.Interface;
using Npw.Services.Shared.DataAccess.DapperProvider.Implementation;

using Npw.Services.Workflows.Data;
using Npw.Services.Workflows.Messages.Commands.WorkFlow;
using Npw.Services.Workflows.Messages.Events.Request;
using System;
using Npw.Services.Workflows.Extensions;
using System.Reflection;




namespace Npw.Services.Workflows
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer Container { get; private set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {



            services.AddCustomMvc();
            services.AddSwaggerDocs();
            services.AddConsul();

            services.AddJaeger();
            services.AddOpenTracing();
            services.AddRedis();
            services.AddTransient<IDapperUnitofWork, DapperUnitofWork>();

            services.AddTransient<IDapperUnitofWork, DapperUnitofWork>();

            services.AddInitializers(typeof(IMongoDbInitializer));


            services.AddScoped(typeof(IUnitOfWork), c =>
            {
                var context = c.GetService<WorkflowDBContext>();
                return new UnitOfWork(context);
            });


            services.AddDbContext<WorkflowDBContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("WorkflowDbContext"));
            });
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly())
                .AsImplementedInterfaces();
            builder.Populate(services);


            builder.AddDispatchers();

            builder.AddRabbitMq();
            //builder.AddMongo();
            //builder.AddMongoRepository<User>("Users");

            Container = builder.Build();



            return new AutofacServiceProvider(Container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
                IApplicationLifetime applicationLifetime, IConsulClient client,
                IStartupInitializer startupInitializer, IServiceProvider service)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "local")
            {
                app.UseDeveloperExceptionPage();
            }

           // app.UseCors("CorsPolicy");
            app.UseAllForwardedHeaders();
            app.UseSwaggerDocs();
            app.UseErrorHandler();
            //app.UseAuthentication();
            //app.UseAccessTokenValidator();
            app.UseServiceId();
            app.UseMvc();
            //app.UseRabbitMq();
            app.UseRabbitMq()
              .SubscribeCommand<CreateDepartmentWorkFlowCommand>()
                .SubscribeCommand<CreateWorkFlowCommand>()
             .SubscribeEvent<CreatedRequest>(@namespace: "request");
            app.UseStaticFiles();

            //Set up auto migration for SQL DB
            app.SetUpAutoMigrations(service);

            var consulServiceId = app.UseConsul();
            applicationLifetime.ApplicationStopped.Register(() =>
            {
                client.Agent.ServiceDeregister(consulServiceId);
                Container.Dispose();
            });

            startupInitializer.InitializeAsync();

        }
    }
}
