
﻿using Maverick.DataAccess;
using N.Shared.Types;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Domain
{

    public class WorkFlow : BaseModel, IIdentifiable

    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public void CompleteModel(string name)
        {
            Name = name;
        }

    }
}
