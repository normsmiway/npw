
using Maverick.DataAccess;
using N.Shared.Types;
using System;

namespace Npw.Services.Workflows.Domain
{

    public class DepartmentWorkFlow : BaseModel, IIdentifiable
    {

        public Guid Id { get; private set; }
        public Guid UserId { get; private set; }
        public Guid DeptId { get; private set; }
        public int Order { get; private set; }
        public string Task { get; private set; }

        public void CompleteModel(Guid userId, Guid deptId, int order, string task)
        {
            UserId = userId;
            DeptId = deptId;
            Order = order;
            Task = task;

        }
    }
}
