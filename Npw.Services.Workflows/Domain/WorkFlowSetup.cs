
using Maverick.DataAccess;
using N.Shared.Types;
using System;

namespace Npw.Services.Workflows.Domain
{
    public class WorkFlowSetup : BaseModel, IIdentifiable
    {
        public Guid Id { get; private set; }
        //[ForeignKey("WorkFlow")]
        public Guid WorkflowId { get; private set; }
        public Guid DepartmentWorkflowId { get; private set; }
        public int Order { get; private set; }

        public void CompleteModel(Guid workflowId, Guid departmentworkflowid, int order)
        {
            WorkflowId = workflowId;
            DepartmentWorkflowId = departmentworkflowid;
            Order = order;
        }
    }
}
