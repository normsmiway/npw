
using Maverick.DataAccess;
using N.Shared.Types;
using System;

namespace Npw.Services.Workflows.Domain
{

    public class WorkflowRequestSetup : BaseModel, IIdentifiable

    {
        public Guid Id { get; private set; }
        public Guid WorkFlowId { get; private set; }
        public Guid RequestId { get; private set; }

        public void CompleteModel(Guid workflowId, Guid requestId)
        {
            WorkFlowId = workflowId;
            RequestId = requestId;
        }
    }
}
