﻿using Maverick.DataAccess;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Domain.Config
{
    public class WorkflowRequestSetupConfig : BaseModelConfiguration<WorkflowRequestSetup>
    {
        public override void Configure(EntityTypeBuilder<WorkflowRequestSetup> builder)
        {
            builder.HasIndex(c => c.Id).IsUnique();
            base.Configure(builder);
        }
    }
}
