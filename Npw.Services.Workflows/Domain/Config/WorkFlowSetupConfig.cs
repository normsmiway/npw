﻿using Maverick.DataAccess;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Domain.Config
{
    public class WorkFlowSetupConfig: BaseModelConfiguration<WorkFlowSetup>
    {
        public override void Configure(EntityTypeBuilder<WorkFlowSetup> builder)
        {
            builder.HasIndex(c => c.Id).IsUnique();
            base.Configure(builder);
        }
    }

}
