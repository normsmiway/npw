﻿using Maverick.DataAccess;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Workflows.Domain.Config
{
    public class WorkflowConfig : BaseModelConfiguration<WorkFlow>
    {
        public override void Configure(EntityTypeBuilder<WorkFlow> builder)
        {
            builder.HasIndex(c => c.Id).IsUnique();
            builder.HasIndex(c => c.Name).IsUnique();
            base.Configure(builder);
        }
    }


   
}
