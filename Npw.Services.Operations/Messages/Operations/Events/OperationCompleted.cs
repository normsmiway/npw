﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Operations.Messages.Operations.Events
{
    public class OperationCompleted : IEvent
    {
        public Guid Id { get; }
        public Guid UserId { get; }
        public string Name { get; }
        public string Resource { get; }
        public string ChatId { get; }

        [JsonConstructor]
        public OperationCompleted(Guid id,
            Guid userId, string name, string resource, string chatId)
        {
            Id = id;
            UserId = userId;
            Name = name;
            Resource = resource;
            ChatId = chatId;
        }
    }
}
