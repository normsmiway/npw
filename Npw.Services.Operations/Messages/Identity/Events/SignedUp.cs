﻿using N.Shared.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Operations.Messages.Identity.Events
{

    [MessageNamespace("identity")]
    public class SignedUp : IEvent
    {
        public Guid UserId { get; }
        public string Email { get; }
        public string UserName { get; }
        public string Role { get; }
        public string Password { get; }
        [JsonConstructor]
        public SignedUp(Guid userId, string email, string userName, string role, string password)
        {
            UserId = userId;
            Email = email;
            UserName = userName;
            Role = role;
            Password = password;
        }

    }
}
