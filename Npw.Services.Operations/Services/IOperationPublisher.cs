﻿using N.Shared.RabbitMq;
using System.Threading.Tasks;

namespace Npw.Services.Operations.Services
{
    public interface IOperationPublisher
    {

        Task PendingAsync(ICorrelationContext context);
        Task CompleteAsync(ICorrelationContext context,string extraValue);
        Task RejectAsync(ICorrelationContext context, string code, string message);
    }
}
