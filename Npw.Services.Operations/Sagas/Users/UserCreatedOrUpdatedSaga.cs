﻿using Chronicle;
using Npw.Services.Operations.Messages.Users.Events;
using System;
using System.Threading.Tasks;

namespace Npw.Services.Operations.Sagas
{
    public class ProfileCreatedSagaState
    {
        public DateTime ProofileCreatedAt;
    }


    public class UserCreatedOrUpdatedSaga : Saga<ProfileCreatedSagaState>, ISagaStartAction<UserCreatedOrUpdated>,ISagaStartAction<CreateOrUpdateUserRejected>
    {
        public UserCreatedOrUpdatedSaga()
        {

        }

        public override Guid ResolveId(object message, ISagaContext context)
        {
            switch (message)
            {
                case UserCreatedOrUpdated pc: return pc.Id;
                default: return base.ResolveId(message, context);
            }
        }
        public async Task CompensateAsync(UserCreatedOrUpdated message, ISagaContext context)
        {
            await Task.CompletedTask;
        }

        public async Task HandleAsync(UserCreatedOrUpdated message, ISagaContext context)
        {
            Data.ProofileCreatedAt = DateTime.Now;
            Complete();
            await Task.CompletedTask;
        }

        public async Task HandleAsync(CreateOrUpdateUserRejected message, ISagaContext context)
        {
            Reject();
            await Task.CompletedTask;
        }

        public async Task CompensateAsync(CreateOrUpdateUserRejected message, ISagaContext context)
        {
            await Task.CompletedTask;
        }
    }
}
