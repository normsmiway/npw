﻿using Chronicle;
using N.Shared.Handlers;
using N.Shared.Messages;
using N.Shared.RabbitMq;
using Npw.Services.Operations.Sagas;
using Npw.Services.Operations.Services;
using System.Threading.Tasks;
using SagaContext = Npw.Services.Operations.Sagas.SagaContext;

namespace Npw.Services.Operations.Handlers
{
    public class GenericCommandHandler<T> : ICommandHandler<T> where T : class, ICommand
    {
        private readonly ISagaCoordinator _sagaCoordinator;
        private readonly IOperationPublisher _operationPublisher;
        private readonly IOperationsStorage _operationsStorage;

        public GenericCommandHandler(ISagaCoordinator sagaCoordinator,
           IOperationPublisher operationPublisher,
           IOperationsStorage operationsStorage)
        {
            _sagaCoordinator = sagaCoordinator;
            _operationPublisher = operationPublisher;
            _operationsStorage = operationsStorage;
        }

        public async Task HandleAsync(T command, ICorrelationContext context)
        {
            if (!command.BelongsToSaga())
            {
                return;
            }

            var sagaContext = SagaContext.FromCorrelationContext(context);
            await _sagaCoordinator.ProcessAsync(command, sagaContext);
        }
    }
}
