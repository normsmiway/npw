﻿using Chronicle;
using N.Shared.Handlers;
using N.Shared.Messages;
using N.Shared.RabbitMq;
using Npw.Services.Operations.Sagas;
using Npw.Services.Operations.Services;
using System.Threading.Tasks;
using SagaContext = Npw.Services.Operations.Sagas.SagaContext;

namespace Npw.Services.Operations.Handlers
{
    public class GenericEventHandler<T> : IEventHandler<T> where T : class, IEvent
    {
        private readonly ISagaCoordinator _sagaCoordinator;
        private readonly IOperationPublisher _operationPublisher;
        private readonly IOperationsStorage _operationsStorage;


        public GenericEventHandler(ISagaCoordinator sagaCoordinator, IOperationPublisher operationPublisher, IOperationsStorage operationsStorage)
        {
            _sagaCoordinator = sagaCoordinator;
            _operationPublisher = operationPublisher;
            _operationsStorage = operationsStorage;
        }
        public async Task HandleAsync(T @event, ICorrelationContext context)
        {
            if (@event.BelongsToSaga())
            {

                if (@event.BelongsToSaga())
                {
                    var sagaContext = SagaContext.FromCorrelationContext(context);
                    await _sagaCoordinator.ProcessAsync(@event, sagaContext);
                }

                switch (@event)
                {
                    case IRejectedEvent rejectedEvent:
                        await _operationsStorage.SetAsync(context.Id, context.UserId,
                            context.Name, OperationState.Rejected, context.Resource,
                            rejectedEvent.Code, rejectedEvent.Reason);
                        await _operationPublisher.RejectAsync(context,
                            rejectedEvent.Code, rejectedEvent.Reason);
                        return;
                    case IEvent @e:
                        await _operationsStorage.SetAsync(context.Id, context.UserId,
                            context.Name, OperationState.Completed, context.Resource,e.ToString());

                        await _operationPublisher.CompleteAsync(context,e.ToString());
                        return;
                }
            }
        }
    }
}
