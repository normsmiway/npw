﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Npw.Services.Operations
{
    public enum OperationState
    {
        Pending,
        Completed,
        Rejected
    }
}
