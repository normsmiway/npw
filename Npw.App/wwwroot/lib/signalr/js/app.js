'use strict';
(function () {
    const $jwt = document.getElementById("jwt");
    const $connect = document.getElementById("connect");
    const $messages = document.getElementById("messages");
    const connection = new signalR.HubConnectionBuilder()
       // .withUrl('http://23.254.229.176/notifications-service/pinz')
        .withUrl('http://localhost:5010/npw')
        .configureLogging(signalR.LogLevel.Information).build();

    $connect.onclick = function () {
        const jwt = $jwt.value;
        if (!jwt || /\s/g.test(jwt)) {
            alert('Invalid JWT.')
            return;
        }

        appendMessage("Connecting to Pinz Hub...");
        connection.start()
            .then(() => {
               connection.invoke('initializeAsync', $jwt.value);
              
            })
            .catch(err => appendMessage(err));
    }

    connection.on('connected', _ => {
        appendMessage("Connected.", "primary");
    });

    connection.on('disconnected', _ => {
        appendMessage("Disconnected, invalid token.", "danger");
    });


    connection.on('operation_pending', (usr) => {
        appendMessage('Operation Pending', "success", usr);
    });


    connection.on('operation_completed', (usr) => {
        appendMessage('Operation Completed', "success", usr);
    });

    connection.on('operation_rejected', (data) => {
        appendMessage("Operation Rejected.", "danger", data);
    });




    function appendMessage(message, type, data) {
        var dataInfo = "";
        if (data) {
            dataInfo += "<div>" + JSON.stringify(data) + "</div>";
        }
        $messages.innerHTML += `<li class="list-group-item list-group-item-${type}">${message} ${dataInfo}</li>`;
    }
})();